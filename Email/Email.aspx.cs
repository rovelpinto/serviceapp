﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Email : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ans = "";
        try
        {
            string title = Request.QueryString["Title"];
            string email = Request.QueryString["EmailId"];
            string sub = Request.QueryString["Sub"];
            string msg = Request.QueryString["Msg"];
            string attachment = Request.QueryString["Att"];
            string names = Request.QueryString["AttNames"];

            if (title != null)
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("servicenzapp@gmail.com", title);

                string[] recipient = email.Split(',');
                for (int i = 0; i < recipient.Length; i++)
                {
                    mail.To.Add(recipient[i]);
                }
                mail.Subject = sub;
                mail.Body = msg;

                if (attachment != null)
                {
                    string[] images = attachment.Split(',');
                    string[] imgnames = names.Split(',');
                    for (int i = 0; i < images.Length; i++)
                    {
                        //string img = images[i].Replace('*', '/');
                        byte[] byteBuffer = Convert.FromBase64String(images[i]);
                        MemoryStream m = new MemoryStream(byteBuffer);
                        mail.Attachments.Add(new Attachment(m, imgnames[i]));
                    }
                }

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("servicenzapp@gmail.com", "serviceNZ@12345");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

                ans = "true";
            }
            else
            {
                ans = "title is empty";
            }
        }
        catch(Exception ex)
        {
            ans = ex.Message;
        }

        string s= "{\"status\": \""+ans+"\"}";
        Response.Write(s);
    }
}