﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using static webapi_demo.Models.JSONMaker;

namespace webapi_demo.Models
{
    public static class Helper
    {
        public static string REST_BASE_PATH = "http://serviceappdemo.azurewebsites.net/";
        //"http://serviceappdemo.iotgecko.com/";
        //"http://servicenz.iotgecko.com/";

        public static string VENDOR_BASE_PATH = "Images/Vendorphoto/";
        public static string VENDORIMAGES_BASE_PATH = "Images/VendorImages/";
        public static string USER_BASE_PATH = "Images/Userphoto/";
        public static string CATEGORY_BASE_PATH = "Images/Category/";
        public static string SUBCAT_BASE_PATH = "Images/SubCategory/";
        public static string SERVICE_BASE_PATH = "Images/Services/";
        public static string REQUEST_BASE_PATH = "Images/Request/";

        public static string EMAIL_PHOTOLINK = REST_BASE_PATH + "images/Taskmaster_logo.png";
        public static string EMAIL_PHOTOCLICK = REST_BASE_PATH + "homepage.aspx";
        public static string EMAIL_ADDRESS = "<p>Task Master</p> <p> New Zealand</p>";
        public static string EMAIL_SIGNATURE = "Task Master";

        public static string EMAIL_TEXT = "<span><b><font color='#ff8b00'>{0}:</font></b> {1}</span><br/>";
        public static string EMAIL_LINE = "<hr style=\"border: none; border - top:1px solid #ffffff\"/>";
        public static string EMAIL_STATUS = "<h2 style=\"background:{0};margin: 0 auto;width: max-content;padding: 0 20px;color: #fff;border-radius: 10px;\">{1}</h2>";
        public static string EMAIL_STATUSCHANGE_TITLE = "<p>Status for Request No <b>{0}</b> has been Changed by {1}</p>";
        public static string EMAIL_OFFER_TITLE = "<p>Offer is <b>{0}</b> for Request No <b>{1}</b> by {2}</p>";
        public static string EMAIL_PAYMENT_DONE = "<p>Payment of <b>{0}</b> is <b>{1}</b> for Request No <b>{2}</b> by {3}</p>";
        public static string EMAIL_PAYMENT_DONE_USER = "<p>Payment of <b>{0}</b> is <b>{1}</b> for Request No <b>{2}</b>";
        public static string EMAIL_PAYMENT_RECIEPT = "<a href=\"{0}\" <span><b><font color='#1a73e7'>View Payment Reciept</font><br/></span><a/>";

        public static string PAYMENT_LINK = REST_BASE_PATH + "Customer/ReviewPayment.aspx";
        public static string PAYMENT_LINK_QRid = "?QRid=";
        public static string PAYMENT_LINK_Amount = "&Amount=";
        public static string PAYMENT_LINK_Sname = "&Sname=";
        public static string PAYMENT_LINK_Title = "&Title=";
        public static string PAYMENT_LINK_Type = "&Type=";
        public static string PAYMENT_LINK_Oid = "&Oid=";

        public static string FULL_PAYMENT_LINK_DIRECT = PAYMENT_LINK
            + PAYMENT_LINK_QRid + "{0}" + PAYMENT_LINK_Amount + "{1}" + PAYMENT_LINK_Sname + "{2}" + PAYMENT_LINK_Title + "{3}" + PAYMENT_LINK_Type + "{4}";

        public static string FULL_PAYMENT_LINK_QUOTE = PAYMENT_LINK
            + PAYMENT_LINK_QRid + "{0}" + PAYMENT_LINK_Amount + "{1}" + PAYMENT_LINK_Sname + "{2}" + PAYMENT_LINK_Title + "{3}" + PAYMENT_LINK_Type + "{4}" + PAYMENT_LINK_Oid + "{5}";


        public static string getRestbaseURL()
        {
            return REST_BASE_PATH;
        }

        public static string getvendorURL()
        {
            return REST_BASE_PATH+VENDOR_BASE_PATH;
        }

        public static string getvendorimagesURL()
        {
            return REST_BASE_PATH + VENDORIMAGES_BASE_PATH;
        }

        public static string getuserURL()
        {
            return REST_BASE_PATH + USER_BASE_PATH;
        }

        public static string getcategoryURL()
        {
            return REST_BASE_PATH + CATEGORY_BASE_PATH;
        }

        public static string getsubcatURL()
        {
            return REST_BASE_PATH + SUBCAT_BASE_PATH;
        }

        public static string getserviceURL()
        {
            return REST_BASE_PATH + SERVICE_BASE_PATH;
        }

        public static string getrequestURL()
        {
            return REST_BASE_PATH + REQUEST_BASE_PATH;
        }

        public static string getOTP()
        {   
            string ans = "";

            Random r = new Random();
            ans += r.Next(9) + "";
            ans += r.Next(9) + "";
            ans += r.Next(9) + "";
            ans += r.Next(9) + "";
            ans += r.Next(9) + "";

            return ans; 
        }

        public static string getcolor(string status)
        {
            if(status== "Accepted" || status== "Completed")
            {
                return "#0ed86a";
            }
            else
            {
                return "#8b1214";
            }
        }

        public static string Addtransaction(string qrid,string uid,string vid,string amount,string datetime,string status,string s_id,string s_res,string s_receipturl)
        {
            string ans = "";
            SqlConnection con = Database.getSADB();
            try
            {
                SqlCommand cmd = new SqlCommand("insert into [Transactions](QR_Id,U_Id,V_Id,Amount,Datetime,Status,Stripe_TransId,Stripe_Response,Stripe_Receipturl) values(@qrid,@uid,@vid,@amount,@dt,@status,@sid,@sres,@receipt)", con);
                cmd.Parameters.AddWithValue("@qrid", qrid);
                cmd.Parameters.AddWithValue("@uid", uid);
                cmd.Parameters.AddWithValue("@vid", vid);
                cmd.Parameters.AddWithValue("@amount", amount);
                cmd.Parameters.AddWithValue("@dt", datetime);
                cmd.Parameters.AddWithValue("@status", status);
                cmd.Parameters.AddWithValue("@sid", s_id);
                cmd.Parameters.AddWithValue("@sres", s_res);
                cmd.Parameters.AddWithValue("@receipt", s_receipturl);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                ans = "true";
            }
            catch(Exception e)
            {
                ans = e.Message;
            }
            return ans;
        }

        //btype - Direct/Quote
        //sendby - User/Vendor
        //mtype - Message/DirectRequest/QuoteRequest/Offer/Cancel
        //typeid - NA/DSB_Id/QSB_Id/O_Id/NA
        public static string AddtoChat(string btype,string bid,string senderid,string recid,string sendby,string mtype,string typeid,string message,string datetime,string uname,string task)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlCommand cmd = new SqlCommand("insert into Chat(BType,BId,Sen_Id,Rec_Id,Sendby,M_Type,M_TypeId,M_Message,M_Datetime) values(@btype,@bid,@sid,@rid,@sendby,@mtype,@mtypeid,@mesg,@dt)", con);
                cmd.Parameters.AddWithValue("@btype",btype);
                cmd.Parameters.AddWithValue("@bid", bid);
                cmd.Parameters.AddWithValue("@sid", senderid);
                cmd.Parameters.AddWithValue("@rid", recid);
                cmd.Parameters.AddWithValue("@sendby", sendby);
                cmd.Parameters.AddWithValue("@mtype",mtype);
                cmd.Parameters.AddWithValue("@mtypeid", typeid);
                cmd.Parameters.AddWithValue("@mesg", message);
                cmd.Parameters.AddWithValue("@dt", datetime);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                string Ntitle = "";
                string Nmesg = "";
                string recievedby = sendby == "User" ? "Vendor" : "User";
                if (mtype=="Message")
                {
                    Ntitle = "Message from " + uname;
                    Nmesg = message;
                }
                else if(mtype== "Offer")
                {

                    string osdt = getResult("CONCAT(O_Deliverydate,' ',O_DeliveryTime)", "Offer", "O_Id", typeid);
                    string odt = datetime;

                    DateTime DT_os = DateTime.ParseExact(osdt, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                    DateTime DT_o = DateTime.ParseExact(odt, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                    DateTime DT_48 = DT_o.AddHours(48);

                    string Expirydt = DT_48.ToString("ddd dd MMM yy, HH:mm");
                    if (DT_os < DT_48)
                    {
                        Expirydt = DT_os.ToString("ddd dd MMM yy, HH: mm");
                    }

                    Ntitle = "Offer for " + task;
                    Nmesg = "Offer is given by " + uname + ", Offer will expire on "+Expirydt;
                }
                else if(mtype=="Cancel")
                {
                    Ntitle = "Cancelled - " + task;
                    Nmesg = uname + " Cancelled the Service";
                }
                else
                {
                    Ntitle = "Request for " + task;
                    Nmesg = "Request is placed by "+uname;
                }

                Addnotification(Ntitle, Nmesg, datetime, mtype, recid, recievedby, bid);

                ans = "true";
            }
            catch(Exception e)
            {
                ans = e.Message;
            }
            return ans;
        }

        public static string Addnotification(string title,string message,string datetime,string type,string uid,string utype,string QR_Id)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlCommand cmd = new SqlCommand("insert into [Notification](N_Title,N_Message,N_DateTime,N_Type,N_Uid,N_Utype,N_Visible) values(@title,@mesg,@dt,@type,@uid,@utype,@visible)", con);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@mesg", message);
                cmd.Parameters.AddWithValue("@dt", datetime);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@uid", uid);
                cmd.Parameters.AddWithValue("@utype", utype);
                cmd.Parameters.AddWithValue("@visible","Yes");
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                ans = "true";

                SqlDataAdapter da = new SqlDataAdapter("select Token from FirebaseTokens where Userid=@uid AND Usertype=@utype AND Flag=@flag", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", uid);
                da.SelectCommand.Parameters.AddWithValue("@utype", utype);
                da.SelectCommand.Parameters.AddWithValue("@flag", "True");
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if(count>0)
                {
                    title = title + " (Request No:" + QR_Id + ")";
                    for (int i=0;i<count;i++)
                    {
                        string token = ds.Tables[0].Rows[i][0].ToString();
                        if (utype=="User")
                        {
                            string res=User_PushNotification(title, message, token,QR_Id,type);
                        }
                        else
                        {
                            string res = Vendor_PushNotification(title, message, token, QR_Id, type);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = e.Message;
            }
            return ans;
        }

        //QRid
        //Type
        //Chat - Message/Offer
        //Bookings - DirectRequest/QuoteRequest
        public static string User_PushNotification(string Title,string Mesg,string Token,string Qrid, string type)
        {
            if (Token == "NA" || Token == "")
            {
                return "no";
            }

            string ans = "";
            try
            {
                string applicationID = "AAAA_dUmZBk:APA91bEtlRIDDVvTdGh5TusliUhxl-TXaUKQxfGLOXunjD5aZ0G_9zvzszKWUm5TmvTQrv9x3xWjEEq1-yxPmLcmVqBMFky3-FQVcYkGT5C_xLCnglp7xGk72ffM9Y2_glTJLzrenfCF";


                string senderId = "1090202788889";

                string deviceId = Token;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";

                var data = new

                {
                    collapseKey = getRandomAlpha(),

                    to = deviceId,

                    notification = new

                    {

                        body = Mesg,

                        title = Title,

                        icon = "myicon"

                    }
                    ,
                    data = new
                    {
                        QRid = Qrid,
                        Type = type
                    }
                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                ans = sResponseFromServer;
                                //{"status": "{"multicast_id":5015833947968800616,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1605869368217740%bb933c01bb933c01"}]}"}

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                ans = ex.Message;
            }

            return ans;

        }

        public static string Vendor_PushNotification(string Title, string Mesg, string Token, string Qrid, string type)
        {
            if (Token == "NA" || Token == "")
            {
                return "";
            }

            string ans = "";
            try
            {
                string applicationID = "AAAA61zxF4E:APA91bHDKYaI3akf1kQXtmayVno9uEmL2U3EZXk-5V3AoYeCBJ3dwcVbJRUXP8JALeTdbtqZMkOtPrkEv9XhbA3bx-UOte5d8QA4e2gGAHAATEAeX4aWRDc00Rh_93DylowKtujB8oPu";


                string senderId = "1010876618625";

                string deviceId = Token;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";

                var data = new

                {
                    collapseKey = getRandomAlpha(),

                    to = deviceId,

                    notification = new

                    {

                        body = Mesg,

                        title = Title,

                        icon = "myicon"

                    }
                    ,
                    data = new
                    {
                        QRid = Qrid,
                        Type = type
                    }
                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                ans = sResponseFromServer;
                                //{"status": "{"multicast_id":5015833947968800616,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1605869368217740%bb933c01bb933c01"}]}"}

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                ans = ex.Message;
            }

            return ans;

        }

        public static HttpResponseMessage getResponse(String s)
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent(s);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return response;
        }

        public static string[] decodeJson(string s, string src)
        {
            string[] ans = null;
            MultipleData c2 = JsonConvert.DeserializeObject<MultipleData>(s);
            int c = c2.Data.Count;
            ans = new string[c];
            for (int i = 0; i < ans.Length; i++)
            {
                ans[i] = c2.Data[i].value;
            }

            return ans;
        }

        public static string getdatetime(int src)
        {
            TimeZoneInfo SL_ZONE = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, SL_ZONE);
            string date = indianTime.ToString("yyyy-MM-dd");
            string time = indianTime.ToString("HH:mm");
            string day = indianTime.ToString("dddd");
            string dayofmonth = indianTime.ToString("dd");
            if (src == 1)
            {
                return date;
            }
            else if (src == 2)
            {
                return time;
            }
            else if (src == 3)
            {
                return date + " " + time;
            }
            else if (src == 4)
            {
                return day;
            }
            else if (src == 5)
            {
                return dayofmonth;
            }
            return date + " " + time;
        }

        public static double getmin(DateTime d1, DateTime d2)
        {
            TimeSpan span = d1.Subtract(d2);

            return span.TotalHours;
        }

        public static string getExtra(int s)
        {
            string s1 = s + "";
            return s1.Length == 1 ? "0" + s1 : s1;
        }

        public static string getStringforIN(string[] s)
        {
            string ans = "";
            for (int i = 0; i < s.Length; i++)
            {
                ans += "'%" + s[i] + "%',";
            }
            ans = ans.Remove(ans.Length - 1);
            ans = "(" + ans + ")";
            return ans;
        }

        public static string getINQuery(List<string> s)
        {
            string ans = "";
            for (int i = 0; i < s.Count; i++)
            {
                ans += "'" + s[i] + "',";
            }
            ans = ans.Remove(ans.Length - 1);
            ans = "(" + ans + ")";
            return ans;
        }

        public static string getStringforLIKE(string[] s,string column)
        {
            string ans = "";
            for (int i = 0; i < s.Length; i++)
            {
                ans += " "+column+" LIKE '%" + s[i] + "%' OR";
            }
            ans = ans.Remove(ans.Length - 2);
            ans = "(" + ans + ")";
            return ans;
        }

        public static string getStringforLIKE_2cols(string[] s,string[] s1,string col1,string col2)
        {
            string ans = "";
            for (int i = 0; i < s.Length; i++)
            {
                ans += " ("+col1+"= '"+s[i]+"' AND " + col2 + " LIKE '%" + s1[i] + "%') OR";
            }
            ans = ans.Remove(ans.Length - 2);
            ans = "(" + ans + ")";
            return ans;
        }

        public static string getStringforCASE(string col,string[] condition,string[] result,string def)
        {
            string ans = " CASE ";
            for(int i=0;i<condition.Length;i++)
            {
                ans += " WHEN " + col + "=" + condition[i] + " THEN " + result[i];
            }
            ans+= " ELSE "+def;
            ans+= " END ";
            return ans;
        }

        public static bool checkforSQLInjections(string atchecker)
        {
            if (atchecker.Contains("convert") || atchecker.Contains("char(") || atchecker.Contains("SQL_") || atchecker.Contains("get_host_address") || atchecker.Contains("select") || atchecker.Contains("update") || atchecker.Contains("detete") || atchecker.Contains("dbname") || atchecker.Contains("version") || atchecker.Contains("1=1") || atchecker.Contains("'") || atchecker.Contains("%27"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool checkforSQLInjections_ignoreInverted(string atchecker)
        {
            if (atchecker.Contains("convert") || atchecker.Contains("char(") || atchecker.Contains("SQL_") || atchecker.Contains("get_host_address") || atchecker.Contains("select") || atchecker.Contains("update") || atchecker.Contains("detete") || atchecker.Contains("dbname") || atchecker.Contains("version") || atchecker.Contains("1=1") || atchecker.Contains("%27"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string getResult(string col, string table, string wherecol, string wherevalue)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";

            string str = "Select " + col + " from " + table + " where " + wherecol + "='" + wherevalue + "'";
            SqlDataAdapter da = new SqlDataAdapter(str, con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                ans = "no";
            }
            else
            {
                ans = ds.Tables[0].Rows[0][0].ToString();
            }
            return ans;
        }

        public static string getRandomAlpha()
        {
            string ans = "";

            string[] alphabets = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            string[] digits = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

            Random r = new Random();
            ans = alphabets[r.Next(alphabets.Length)];

            int a = r.Next(2);
            ans += a == 1 ? alphabets[r.Next(alphabets.Length)] : digits[r.Next(digits.Length)];

            a = r.Next(2);
            ans += a == 1 ? alphabets[r.Next(alphabets.Length)] : digits[r.Next(digits.Length)];

            a = r.Next(2);
            ans += a == 1 ? alphabets[r.Next(alphabets.Length)] : digits[r.Next(digits.Length)];

            a = r.Next(2);
            ans += a == 1 ? alphabets[r.Next(alphabets.Length)] : digits[r.Next(digits.Length)];

            a = r.Next(2);
            ans += a == 1 ? alphabets[r.Next(alphabets.Length)] : digits[r.Next(digits.Length)];

            return ans;
        }

        public static string Email(string email, string name, string username, string password)
        {
            try
            {
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress("info@taskmaster.co.nz", "Task Master");
                mm.Subject = "One Step from Account Verification";
                mm.To.Add(email);

                string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/registration_otp.html");
                string content = System.IO.File.ReadAllText(path);

                content = content.Replace("@photoclick??",EMAIL_PHOTOCLICK);
                content = content.Replace("@photolink??", EMAIL_PHOTOLINK);
                content = content.Replace("@address??", EMAIL_ADDRESS);
                content = content.Replace("@name??", name);
                content = content.Replace("@signature??", EMAIL_SIGNATURE);
                
                mm.Body = content;


                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp-relay.sendinblue.com";
                //smtpc.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "taskmaster.co.nz@gmail.com";
                NetworkCred.Password = "3XmyvKa418hUb6BV";
                //smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);

                return "true";
            }
            catch (Exception e)
            {
                string ans = e.Message;
                return ans;
            }
        }

        public static string sendEmail(string[] email, string subject, string body)
        {
            try
            {
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress("info@taskmaster.co.nz", "Task Master");
                mm.Subject = subject;
                for(int i=0;i<email.Length;i++)
                {
                    mm.To.Add(email[i]);
                }

                mm.Body = body;


                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp-relay.sendinblue.com";
                //smtpc.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "taskmaster.co.nz@gmail.com";
                NetworkCred.Password = "3XmyvKa418hUb6BV";
                //smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);

                return "true";
            }
            catch (Exception e)
            {
                string ans = e.Message;
                return ans;
            }
        }

    }
}