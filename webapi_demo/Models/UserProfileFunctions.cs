﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
namespace webapi_demo.Models
{
    public class UserProfileFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();

        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public string registration(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from User_Register where U_EmailID=@email", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //email already exists
                    return jm.Singlevalue("Email");
                }

                da = new SqlDataAdapter("select * from User_Register where U_MobileNo=@mobile", con);
                da.SelectCommand.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //contact already exists
                    return jm.Singlevalue("Mobile");
                }

                if (ans == "")
                {
                    string url = Helper.getuserURL() + u.Photo;

                    cmd = new SqlCommand("insert into User_Register(U_FirstName,U_LastName,U_EmailID,U_Password,U_MobileNo,U_City,U_Country,U_Profilepicture) values (@fname,@lname,@email,@pass,@mobile,@city,@country,@photo)", con);
                    cmd.Parameters.AddWithValue("@fname", u.Fname);
                    cmd.Parameters.AddWithValue("@lname", u.Lname);
                    cmd.Parameters.AddWithValue("@email", u.Email);
                    cmd.Parameters.AddWithValue("@pass", u.Pass);
                    cmd.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                    cmd.Parameters.AddWithValue("@city", u.City);
                    cmd.Parameters.AddWithValue("@country", u.Country);
                    cmd.Parameters.AddWithValue("@photo", url);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    string name = u.Fname + " " + u.Lname;
                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/account_created.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", name);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                    string subject = "Welcome to Task Master! Let’s get started.";

                    Helper.sendEmail(new string[] { u.Email }, subject, content);

                    //account added
                    ans = jm.Singlevalue("true");
                }
            }
            catch(Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Checkregistration(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from User_Register where U_EmailID=@email", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //email already exists
                    return jm.Singlevalue("Email");
                }

                da = new SqlDataAdapter("select * from User_Register where U_MobileNo=@mobile", con);
                da.SelectCommand.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //contact already exists
                    return jm.Singlevalue("Mobile");
                }

                if (ans == "")
                {
                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/registration_otp.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", u.Fname + " " + u.Lname);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);
                    content = content.Replace("@otp??", u.OTP);

                    string subject = "One Step from Account Verification";

                    Helper.sendEmail(new string[] { u.Email }, subject, content);

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public class UserD
        {
            public string Uid { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            public string Email { get; set; }
            public string Pass { get; set; }
            public string Mobile { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Photo { get; set; }
            public string OldPass { get; set; }
            public string NewPass { get; set; }
            public string OTP { get; set; }
            public string Aid { get; set; }
            public string Atype { get; set; }
            public string Address { get; set; }
            public string Token { get; set; }
            public string Utype { get; set; }
            public string Flag { get; set; }
        }

        public class Cards
        {
            public string Uid { get; set; }
            public string Cid { get; set; }
            public string Ctype { get; set; }
            public string Cno { get; set; }
            public string Expiryyear { get; set; }
            public string Expirymonth { get; set; }
            public string Cvv { get; set; }
            public string Cardname { get; set; }
            public string Bankname { get; set; }
        }

        public string login(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select U_Id,U_FirstName,U_LastName,U_Password,U_Profilepicture from User_Register where (U_EmailID=@email OR U_MobileNo=@contact)", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                da.SelectCommand.Parameters.AddWithValue("@contact", "+64"+u.Mobile);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string uid = ds.Tables[0].Rows[0][0].ToString();
                    string name = ds.Tables[0].Rows[0][1].ToString()+" "+ ds.Tables[0].Rows[0][2].ToString();
                    string password = ds.Tables[0].Rows[0][3].ToString();
                    string photo = ds.Tables[0].Rows[0][4].ToString();
                    if (password == u.Pass)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Uid");
                        dt.Columns.Add("Name");
                        dt.Columns.Add("Photo");
                        dt.Rows.Add(uid, name,photo);

                        //status ok
                        //data - uid,fullname,photo
                        ans = jm.Maker1(dt);
                    }
                    else
                    {
                        //wrong password
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //email/mobile doesnt exists
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string loginwithToken(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select U_Id,U_FirstName,U_LastName,U_Password,U_Profilepicture from User_Register where (U_EmailID=@email OR U_MobileNo=@contact)", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                da.SelectCommand.Parameters.AddWithValue("@contact", "+64" + u.Mobile);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string uid = ds.Tables[0].Rows[0][0].ToString();
                    string name = ds.Tables[0].Rows[0][1].ToString() + " " + ds.Tables[0].Rows[0][2].ToString();
                    string password = ds.Tables[0].Rows[0][3].ToString();
                    string photo = ds.Tables[0].Rows[0][4].ToString();
                    if (password == u.Pass)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Uid");
                        dt.Columns.Add("Name");
                        dt.Columns.Add("Photo");
                        dt.Rows.Add(uid, name, photo);

                        u.Uid = uid;
                        u.Utype = "User";
                        updateToken(u);

                        //status ok
                        //data - uid,fullname,photo
                        ans = jm.Maker1(dt);
                    }
                    else
                    {
                        //wrong password
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //email/mobile doesnt exists
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Logout(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("delete from FirebaseTokens where Userid=@vid AND Token=@token AND Usertype='User'", con);
                cmd.Parameters.AddWithValue("@vid", u.Uid);
                cmd.Parameters.AddWithValue("@token", u.Token);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getProfile(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select U_FirstName,U_LastName,U_EmailID,U_MobileNo,U_City,U_Country,U_Profilepicture from User_Register where U_Id=@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - fname,lname,email,mobile,city,country,photo
                    return jm.Maker(ds);
                }
                else
                {
                    //user doesnt exists
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string updateprofile(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from User_Register where U_EmailID=@email AND U_Id<>@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //email already exists
                    return jm.Singlevalue("Email");
                }

                da = new SqlDataAdapter("select * from User_Register where U_MobileNo=@mobile AND U_Id<>@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //contact already exists
                    return jm.Singlevalue("Mobile");
                }

                if (ans == "")
                {
                    string url = Helper.getuserURL() + u.Photo;

                    cmd = new SqlCommand("update User_Register set U_FirstName=@fname,U_LastName=@lname,U_EmailID=@email,U_MobileNo=@mobile,U_City=@city,U_Country=@country,U_Profilepicture=@photo where U_Id=@uid", con);
                    cmd.Parameters.AddWithValue("@fname", u.Fname);
                    cmd.Parameters.AddWithValue("@lname", u.Lname);
                    cmd.Parameters.AddWithValue("@email", u.Email);
                    cmd.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                    cmd.Parameters.AddWithValue("@city", u.City);
                    cmd.Parameters.AddWithValue("@country", u.Country);
                    cmd.Parameters.AddWithValue("@photo", url);
                    cmd.Parameters.AddWithValue("@uid", u.Uid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //account added
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string changepassword(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from User_Register where U_Password=@pass AND U_Id=@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@pass", u.OldPass);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    if (u.OldPass == u.NewPass)
                    {
                        //no
                        //Your new password cannot be same as old
                        ans = jm.Singlevalue("no");
                    }
                    else
                    {
                        cmd = new SqlCommand("update User_Register set U_Password=@pass where U_Id=@uid", con);
                        cmd.Parameters.AddWithValue("@pass", u.NewPass);
                        cmd.Parameters.AddWithValue("@uid", u.Uid);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        //true
                        ans = jm.Singlevalue("true");
                    }
                }
                else
                {
                    //old password is not correct
                    ans = jm.Singlevalue("false");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string forgetpassword(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select U_Id,CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_EmailID=@email", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string name = ds.Tables[0].Rows[0][1].ToString();
                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/forgotpass_otp.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", name);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);
                    content = content.Replace("@otp??", u.OTP);

                    string subject = "Password Reset";

                    Helper.sendEmail(new string[] { u.Email }, subject, content);

                    //Uid
                    ans = jm.Maker(ds);
                }
                else
                {
                    //email doesnt exists
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string resetpassword(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("update User_Register set U_Password=@pass where U_Id=@uid", con);
                cmd.Parameters.AddWithValue("@pass", u.Pass);
                cmd.Parameters.AddWithValue("@uid", u.Uid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //Password changed
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string addAddress(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from [Address] where u_Id=@uid AND A_Type=@type AND A_BillingAddress=@add", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                da.SelectCommand.Parameters.AddWithValue("@type", u.Atype);
                da.SelectCommand.Parameters.AddWithValue("@add", u.Address);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //already
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("insert into [Address](U_Id,A_Type,A_BillingAddress) values(@uid,@type,@add)", con);
                    cmd.Parameters.AddWithValue("@type", u.Atype);
                    cmd.Parameters.AddWithValue("@add", u.Address);
                    cmd.Parameters.AddWithValue("@uid", u.Uid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //address added
                    ans = jm.Singlevalue("true");
                }
                
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string updateAddress(UserD u)
        {
            string uid = Helper.getResult("U_Id", "[Address]", "A_Id", u.Aid);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from [Address] where u_Id=@uid AND A_Type=@type AND A_BillingAddress=@add AND A_Id<>@aid", con);
                da.SelectCommand.Parameters.AddWithValue("@aid", u.Aid);
                da.SelectCommand.Parameters.AddWithValue("@uid", uid);
                da.SelectCommand.Parameters.AddWithValue("@type", u.Atype);
                da.SelectCommand.Parameters.AddWithValue("@add", u.Address);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //already
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("update [Address] set A_Type=@type,A_BillingAddress=@add where A_Id=@aid", con);
                    cmd.Parameters.AddWithValue("@type", u.Atype);
                    cmd.Parameters.AddWithValue("@add", u.Address);
                    cmd.Parameters.AddWithValue("@aid", u.Aid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //address updated
                    ans = jm.Singlevalue("true");
                }

            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string deleteAddress(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("delete from [Address] where A_Id=@aid", con);
                cmd.Parameters.AddWithValue("@aid", u.Aid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //address updated
                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getAddress(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select A_Id,A_Type,A_BillingAddress from [Address] where U_Id=@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - aid,type,address
                    return jm.Maker(ds);
                }
                else
                {
                    //no address added
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getAddressbyId(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select A_Id,A_Type,A_BillingAddress from [Address] where U_Id=@uid AND A_Id=@aid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                da.SelectCommand.Parameters.AddWithValue("@aid", u.Aid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - aid,type,address
                    return jm.Maker(ds);
                }
                else
                {
                    //no address added
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string addCard(Cards c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from [Card] where U_Id=@uid AND C_No=@cno", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", c.Uid);
                da.SelectCommand.Parameters.AddWithValue("@cno", c.Cno);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //already
                    //Card already Exists
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("insert into [Card](U_Id,C_Type,C_No,C_Expiryyear,C_ExpiryMonth,C_CVV,C_CardName,C_BankName) values(@uid,@type,@cno,@year,@month,@cvv,@cname,@bname)", con);
                    cmd.Parameters.AddWithValue("@uid", c.Uid);
                    cmd.Parameters.AddWithValue("@type", c.Ctype);
                    cmd.Parameters.AddWithValue("@cno", c.Cno);
                    cmd.Parameters.AddWithValue("@year", c.Expiryyear);
                    cmd.Parameters.AddWithValue("@month", c.Expirymonth);
                    cmd.Parameters.AddWithValue("@cvv", c.Cvv);
                    cmd.Parameters.AddWithValue("@cname", c.Cardname);
                    cmd.Parameters.AddWithValue("@bname", c.Bankname);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //Card added
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string deleteCard(Cards c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                cmd = new SqlCommand("delete from [Card] where C_Id=@cid", con);
                cmd.Parameters.AddWithValue("@cid", c.Cid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //Card removed
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getCards(Cards c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from Card where U_Id=@uid order by C_Id DESC", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", c.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - cid,uid,ctype,cno,year,month,cvv,cardname,bankname
                    return jm.Maker(ds);
                }
                else
                {
                    //no cards added
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string updateToken(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            string dt = Helper.getdatetime(3);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select Userid,Usertype from FirebaseTokens where Token=@token", con);
                da.SelectCommand.Parameters.AddWithValue("@token", u.Token);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count=ds.Tables[0].Rows.Count;
                if(count>0)
                {
                    string userid = ds.Tables[0].Rows[0][0].ToString();
                    string usertype = ds.Tables[0].Rows[0][1].ToString();

                    if(userid==u.Uid && usertype==u.Utype)
                    {
                        ans = jm.Singlevalue("true");
                    }
                    else
                    {
                        cmd = new SqlCommand("update FirebaseTokens set Userid=@uid,Usertype=@type,Lastupdated=@dt where Token=@token", con);
                        cmd.Parameters.AddWithValue("@uid", u.Uid);
                        cmd.Parameters.AddWithValue("@type",u.Utype);
                        cmd.Parameters.AddWithValue("@dt", dt);
                        cmd.Parameters.AddWithValue("@token", u.Token);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        //true
                        ans = jm.Singlevalue("true");
                    }
                }
                else
                {
                    cmd = new SqlCommand("insert into FirebaseTokens(Userid,Usertype,Token,Flag,Lastupdated) values(@uid,@type,@token,@flag,@dt)", con);
                    cmd.Parameters.AddWithValue("@uid", u.Uid);
                    cmd.Parameters.AddWithValue("@type", u.Utype);
                    cmd.Parameters.AddWithValue("@dt", dt);
                    cmd.Parameters.AddWithValue("@token", u.Token);
                    cmd.Parameters.AddWithValue("@flag", "True");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }

            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string updateNotifications(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            string dt = Helper.getdatetime(3);

            try
            {
                cmd = new SqlCommand("update FirebaseTokens set Flag=@flag,Lastupdated=@dt where Userid=@uid AND Usertype=@utype AND Token=@token", con);
                cmd.Parameters.AddWithValue("@flag", u.Flag);
                cmd.Parameters.AddWithValue("@dt", dt);
                cmd.Parameters.AddWithValue("@uid", u.Uid);
                cmd.Parameters.AddWithValue("@utype","User");
                cmd.Parameters.AddWithValue("@token", u.Token);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch(Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getNotificationStatus(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select Flag from FirebaseTokens where Userid=@uid AND Usertype=@utype AND Token=@token", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Uid);
                da.SelectCommand.Parameters.AddWithValue("@utype", "User");
                da.SelectCommand.Parameters.AddWithValue("@token", u.Token);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - flag
                    return jm.Maker(ds);
                }
                else
                {
                    //no data found
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

    }
}