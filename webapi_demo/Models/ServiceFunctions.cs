﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace webapi_demo.Models
{
    public class ServiceFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();

        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public class Vendorservices
        {
            public string Uid { get; set; }
            public string VNId { get; set; }
            public string VSId { get; set; }
            public string Vid { get; set; }
            public string VNType { get; set; }
            public string VNName { get; set; }
            public string VNDesc { get; set; }
            public string VNPrice { get; set; }
            public string VNTime { get; set; }
            public string VNCat { get; set; }
            public string VNSubcat { get; set; }
            public string VNStatus { get; set; }
            public JObject Data { get; set; }
            public string City { get; set; }
            public string SName { get; set; }
            public string Name { get; set; }
            public string Cid { get; set; }
        }

        public class VendorImages
        {
            public string SIId { get; set; }
            public string VNId { get; set; }
            public string Image1 { get; set; }
            public string Image2 { get; set; }
            public string Image3 { get; set; }
            public string Image4 { get; set; }
            public string Image5 { get; set; }
        }

        public class Variant
        {
            public string VSId { get; set; }
            public string VNId { get; set; }
            public string VS1Name { get; set; }
            public string VS1Desc { get; set; }
            public string VS1Amount { get; set; }
            public string VS1Image { get; set; }
            public string VS1Time { get; set; }
            public string VS2Name { get; set; }
            public string VS2Desc { get; set; }
            public string VS2Amount { get; set; }
            public string VS2Image { get; set; }
            public string VS2Time { get; set; }
            public string VS3Name { get; set; }
            public string VS3Desc { get; set; }
            public string VS3Amount { get; set; }
            public string VS3Image { get; set; }
            public string VS3Time { get; set; }
            public string VS4Name { get; set; }
            public string VS4Desc { get; set; }
            public string VS4Amount { get; set; }
            public string VS4Image { get; set; }
            public string VS4Time { get; set; }
            public string VS5Name { get; set; }
            public string VS5Desc { get; set; }
            public string VS5Amount { get; set; }
            public string VS5Image { get; set; }
            public string VS5Time { get; set; }
        }

        public class Suburbs
        {
            public JObject Data { get; set; }
            public string City { get; set; }
            public string SName { get; set; }
        }

        public string AddServices(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select VN_Id from Vendor_Services where V_Id=@vid AND VN_ServiceName=@name AND VN_Category=@cat AND VN_SubCategory=@subcat", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", s.Vid);
                da.SelectCommand.Parameters.AddWithValue("@name", s.VNName);
                da.SelectCommand.Parameters.AddWithValue("@cat", s.VNCat);
                da.SelectCommand.Parameters.AddWithValue("@subcat", s.VNSubcat);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Service already exists
                    return jm.Singlevalue("already");
                }

                if (ans == "")
                {

                    cmd = new SqlCommand("insert into Vendor_Services(V_Id,V_Type,VN_ServiceName,VN_ServiceDescr,VN_Price,VN_Time,VN_Category,VN_Subcategory,VN_Status) values (@vid,@type,@name,@desc,@price,@time,@cat,@subcat,@status)", con);
                    cmd.Parameters.AddWithValue("@vid",s.Vid);
                    cmd.Parameters.AddWithValue("@type", s.VNType);
                    cmd.Parameters.AddWithValue("@name", s.VNName);
                    cmd.Parameters.AddWithValue("@desc", s.VNDesc);
                    cmd.Parameters.AddWithValue("@price", s.VNPrice);
                    cmd.Parameters.AddWithValue("@time", s.VNTime);
                    cmd.Parameters.AddWithValue("@cat", s.VNCat);
                    cmd.Parameters.AddWithValue("@subcat", s.VNSubcat);
                    cmd.Parameters.AddWithValue("@status", s.VNStatus);
                    con.Open();
                    cmd.ExecuteNonQuery();

                    SqlDataAdapter da2 = new SqlDataAdapter("select Top 1 @@IDENTITY from Vendor_Services order by VN_Id DESC", con);
                    DataSet ds2 = new DataSet();
                    da2.Fill(ds2);
                    string VNId = ds2.Tables[0].Rows[0][0].ToString();

                    con.Close();

                    DataTable dt = new DataTable();
                    dt.Columns.Add("VNId");
                    dt.Rows.Add(VNId);

                    //Status - Ok
                    //Data - VNId
                    ans = jm.Maker1(dt);
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string AddServicesImages(VendorImages i)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string url1 = i.Image1 == "NA" ? i.Image1 : Helper.getserviceURL() + i.Image1;
                string url2 = i.Image2 == "NA" ? i.Image2 : Helper.getserviceURL() + i.Image2;
                string url3 = i.Image3 == "NA" ? i.Image3 : Helper.getserviceURL() + i.Image3;
                string url4 = i.Image4 == "NA" ? i.Image4 : Helper.getserviceURL() + i.Image4;
                string url5 = i.Image5 == "NA" ? i.Image5 : Helper.getserviceURL() + i.Image5;

                cmd = new SqlCommand("insert into Service_Image(VN_Id,SI_Image1,SI_Image2,SI_Image3,SI_Image4,SI_Image5) values (@vnid,@img1,@img2,@img3,@img4,@img5)", con);
                cmd.Parameters.AddWithValue("@vnid", i.VNId);
                cmd.Parameters.AddWithValue("@img1", url1);
                cmd.Parameters.AddWithValue("@img2", url2);
                cmd.Parameters.AddWithValue("@img3", url3);
                cmd.Parameters.AddWithValue("@img4", url4);
                cmd.Parameters.AddWithValue("@img5", url5);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string UpdateServices(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select VN_Id from Vendor_Services where V_Id=@vid AND VN_ServiceName=@name AND VN_Category=@cat AND VN_SubCategory=@subcat AND VN_Id<>@vnid", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", s.Vid);
                da.SelectCommand.Parameters.AddWithValue("@name", s.VNName);
                da.SelectCommand.Parameters.AddWithValue("@cat", s.VNCat);
                da.SelectCommand.Parameters.AddWithValue("@subcat", s.VNSubcat);
                da.SelectCommand.Parameters.AddWithValue("@vnid", s.VNId);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Service name already exists
                    return jm.Singlevalue("already");
                }

                if (ans == "")
                {
                    cmd = new SqlCommand("update Vendor_Services set V_Type=@type,VN_ServiceName=@name, VN_ServiceDescr=@desc,VN_Price=@price,VN_Time=@time,VN_Category=@cat,VN_Subcategory=@subcat,VN_Status=@status where VN_Id=@vnid", con);
                    cmd.Parameters.AddWithValue("@type", s.VNType);
                    cmd.Parameters.AddWithValue("@name", s.VNName);
                    cmd.Parameters.AddWithValue("@desc", s.VNDesc);
                    cmd.Parameters.AddWithValue("@price", s.VNPrice);
                    cmd.Parameters.AddWithValue("@time", s.VNTime);
                    cmd.Parameters.AddWithValue("@cat", s.VNCat);
                    cmd.Parameters.AddWithValue("@subcat", s.VNSubcat);
                    cmd.Parameters.AddWithValue("@status", s.VNStatus);
                    cmd.Parameters.AddWithValue("@vnid", s.VNId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string UpdateServicesImages(VendorImages i)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string url1 = i.Image1 == "NA" ? i.Image1 : Helper.getserviceURL() + i.Image1;
                string url2 = i.Image2 == "NA" ? i.Image2 : Helper.getserviceURL() + i.Image2;
                string url3 = i.Image3 == "NA" ? i.Image3 : Helper.getserviceURL() + i.Image3;
                string url4 = i.Image4 == "NA" ? i.Image4 : Helper.getserviceURL() + i.Image4;
                string url5 = i.Image5 == "NA" ? i.Image5 : Helper.getserviceURL() + i.Image5;

                cmd = new SqlCommand("update Service_Image set SI_Image1=@img1,SI_Image2=@img2,SI_Image3=@img3,SI_Image4=@img4,SI_Image5=@img5 where VN_Id=@vnid", con);
                cmd.Parameters.AddWithValue("@vnid", i.VNId);
                cmd.Parameters.AddWithValue("@img1", url1);
                cmd.Parameters.AddWithValue("@img2", url2);
                cmd.Parameters.AddWithValue("@img3", url3);
                cmd.Parameters.AddWithValue("@img4", url4);
                cmd.Parameters.AddWithValue("@img5", url5);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string DeleteService(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                cmd = new SqlCommand("update Vendor_Services set VN_Status=@status where VN_Id=@vnid", con);
                cmd.Parameters.AddWithValue("@status", s.VNStatus);
                cmd.Parameters.AddWithValue("@vnid", s.VNId);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string AddServiceVariant(Variant v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("insert into Variant (VS1_ServiceName,VS1_Description,VS1_Amount,VS1_Image,VS1_DeliveryTime, VS2_ServiceName,VS2_Description,VS2_Amount,VS2_Image,VS2_DeliveryTime, VS3_ServiceName,VS3_Description,VS3_Amount,VS3_Image,VS3_DeliveryTime, VS4_ServiceName,VS4_Description,VS4_Amount,VS4_Image,VS4_DeliveryTime, VS5_ServiceName,VS5_Description,VS5_Amount,VS5_Image,VS5_DeliveryTime) values (@name1,@desc1,@price1,@img1,@time1,@name2,@desc2,@price2,@img2,@time2,      @name3,@desc3,@price3,@img3,@time3,@name4,@desc4,@price4,@img4,@time4, @name5,@desc5,@price5,@img5,@time5)", con);
                cmd.Parameters.AddWithValue("@name1", v.VS1Name);
                cmd.Parameters.AddWithValue("@desc1", v.VS1Desc);
                cmd.Parameters.AddWithValue("@price1", v.VS1Amount);
                cmd.Parameters.AddWithValue("@img1", v.VS1Image);
                cmd.Parameters.AddWithValue("@time1", v.VS1Time);
                cmd.Parameters.AddWithValue("@name2", v.VS2Name);
                cmd.Parameters.AddWithValue("@desc2", v.VS2Desc);
                cmd.Parameters.AddWithValue("@price2", v.VS2Amount);
                cmd.Parameters.AddWithValue("@img2", v.VS2Image);
                cmd.Parameters.AddWithValue("@time2", v.VS2Time);
                cmd.Parameters.AddWithValue("@name3", v.VS3Name);
                cmd.Parameters.AddWithValue("@desc3", v.VS3Desc);
                cmd.Parameters.AddWithValue("@price3", v.VS3Amount);
                cmd.Parameters.AddWithValue("@img3", v.VS3Image);
                cmd.Parameters.AddWithValue("@time3", v.VS3Time);
                cmd.Parameters.AddWithValue("@name4", v.VS4Name);
                cmd.Parameters.AddWithValue("@desc4", v.VS4Desc);
                cmd.Parameters.AddWithValue("@price4", v.VS4Amount);
                cmd.Parameters.AddWithValue("@img4", v.VS4Image);
                cmd.Parameters.AddWithValue("@time4", v.VS4Time);
                cmd.Parameters.AddWithValue("@name5", v.VS5Name);
                cmd.Parameters.AddWithValue("@desc5", v.VS5Desc);
                cmd.Parameters.AddWithValue("@price5", v.VS5Amount);
                cmd.Parameters.AddWithValue("@img5", v.VS5Image);
                cmd.Parameters.AddWithValue("@time5", v.VS5Time);
                con.Open();
                cmd.ExecuteNonQuery();

                SqlDataAdapter da2 = new SqlDataAdapter("select Top 1 @@IDENTITY from Variant order by VS_Id DESC", con);
                DataSet ds2 = new DataSet();
                da2.Fill(ds2);
                string VSId = ds2.Tables[0].Rows[0][0].ToString();

                con.Close();

                cmd = new SqlCommand("Update Vendor_Services set VS_Id=@vsid where VN_Id=@vnid", con);
                cmd.Parameters.AddWithValue("@vsid", VSId);
                cmd.Parameters.AddWithValue("@vnid", v.VNId);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string UpdateServiceVariant(Variant v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("update Variant set VS1_ServiceName=@name1,VS1_Description=@desc1,VS1_Amount=@price1,VS1_Image=@img1,VS1_DeliveryTime=@time1,VS2_ServiceName=@name2,VS2_Description=@desc2,VS2_Amount=@price2,VS2_Image=@img2,VS2_DeliveryTime=@time2,VS3_ServiceName=@name3,VS3_Description=@desc3,VS3_Amount=@price3,VS3_Image=@img3,VS3_DeliveryTime=@time3,VS4_ServiceName=@name4,VS4_Description=@desc4,VS4_Amount=@price4,VS4_Image=@img4,VS4_DeliveryTime=@time4,VS5_ServiceName=@name5,VS5_Description=@desc5,VS5_Amount=@price5,VS5_Image=@img5,VS5_DeliveryTime=@time5  where VS_Id=@vsid", con);
                cmd.Parameters.AddWithValue("@vsid", v.VSId);
                cmd.Parameters.AddWithValue("@name1", v.VS1Name);
                cmd.Parameters.AddWithValue("@desc1", v.VS1Desc);
                cmd.Parameters.AddWithValue("@price1", v.VS1Amount);
                cmd.Parameters.AddWithValue("@img1", v.VS1Image);
                cmd.Parameters.AddWithValue("@time1", v.VS1Time);
                cmd.Parameters.AddWithValue("@name2", v.VS2Name);
                cmd.Parameters.AddWithValue("@desc2", v.VS2Desc);
                cmd.Parameters.AddWithValue("@price2", v.VS2Amount);
                cmd.Parameters.AddWithValue("@img2", v.VS2Image);
                cmd.Parameters.AddWithValue("@time2", v.VS2Time);
                cmd.Parameters.AddWithValue("@name3", v.VS3Name);
                cmd.Parameters.AddWithValue("@desc3", v.VS3Desc);
                cmd.Parameters.AddWithValue("@price3", v.VS3Amount);
                cmd.Parameters.AddWithValue("@img3", v.VS3Image);
                cmd.Parameters.AddWithValue("@time3", v.VS3Time);
                cmd.Parameters.AddWithValue("@name4", v.VS4Name);
                cmd.Parameters.AddWithValue("@desc4", v.VS4Desc);
                cmd.Parameters.AddWithValue("@price4", v.VS4Amount);
                cmd.Parameters.AddWithValue("@img4", v.VS4Image);
                cmd.Parameters.AddWithValue("@time4", v.VS4Time);
                cmd.Parameters.AddWithValue("@name5", v.VS5Name);
                cmd.Parameters.AddWithValue("@desc5", v.VS5Desc);
                cmd.Parameters.AddWithValue("@price5", v.VS5Amount);
                cmd.Parameters.AddWithValue("@img5", v.VS5Image);
                cmd.Parameters.AddWithValue("@time5", v.VS5Time);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string DeleteServiceVariant(Variant v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("delete from Variant where VS_Id=@vsid", con);
                cmd.Parameters.AddWithValue("@vsid", v.VSId);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getServicesforVendor(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string where = "";
                if(s.VNType.Length>0)
                {
                    where += s.VNType=="Any"?"" : " AND s.V_Type=@type";
                }

                if (s.VNName.Length > 0)
                {
                    where += " AND s.VN_ServiceName LIKE '%' + @name + '%'";
                }

                if (s.VNStatus.Length > 0)
                {
                    where += " AND s.VN_Status=@status";
                }



                string cat = "ISNULL((select CG_Name from Categories where CG_Id=s.VN_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=s.VN_SubCategory),'Unknown')";

                string query = "select s.VN_Id,s.VS_Id,s.V_Type,s.VN_ServiceName,s.VN_ServiceDescr,s.VN_Price,s.VN_Time,s.VN_Category," + cat + ",s.VN_SubCategory," + subcat + ",s.VN_Status,i.SI_Image1,i.SI_Image2,i.SI_Image3,i.SI_Image4,i.SI_Image5 from Vendor_Services s,Service_Image i where s.V_Id=@vid " + where + " AND s.VN_Id=i.VN_Id order by s.VN_Id DESC";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@vid", s.Vid);
                da.SelectCommand.Parameters.AddWithValue("@type", s.VNType);
                da.SelectCommand.Parameters.AddWithValue("@name", s.VNName);
                da.SelectCommand.Parameters.AddWithValue("@status", s.VNStatus);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if(count>0)
                {
                    //Vnid,Vsid,type,name,desc,price,time,catid,catname,subcatid,subcatname,status,img1,img2,img3,img4,img5
                    //Vsid can be empty
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no
                    ans = jm.Singlevalue("no");
                }
            }
            catch(Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getVariant(Variant v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from Variant where VS_Id=@vsid", con);
                da.SelectCommand.Parameters.AddWithValue("@vsid", v.VSId);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if(count>0)
                {
                    //vsid,name1,desc1,amt1,img1,time1,name2,desc2,amt2,img2,time2,name3,desc3,amt3,img3,time3,
                    //name4,desc4,amt4,img4,time4,name5,desc5,amt5,img5,time5
                    ans = jm.Maker(ds);
                }
                else
                {
                    ans = jm.Singlevalue("no");
                }
            }
            catch(Exception e)
            {
                ans = jm.Singlevalue(e.Message);
            }
            return ans;
        }

        //---------------------------------USER APP---------------------------------------------------------------

        public string getUserSuburbs(Vendorservices s)
        {

            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string[] names = new string[] { "City" };
                string[] values = new string[names.Length];

                SqlDataAdapter da = new SqlDataAdapter("select DISTINCT City from Suburbs", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    ans = "{ \"status\" : \"ok\",";
                    ans += "\"Data\" :[ ";

                    for (int i = 0; i < count; i++)
                    {

                        values = new string[names.Length];
                        string city = ds.Tables[0].Rows[i][0].ToString();
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = city;
                        }

                        SqlDataAdapter da1 = null;
                        da1 = new SqlDataAdapter("select s.SName,ISNULL((select 'Yes' from User_Suburb where SName=s.SName AND City=s.City AND U_Id=@uid),'No') from Suburbs s where City=@city order by SName", con);

                        da1.SelectCommand.Parameters.AddWithValue("@uid", s.Uid);
                        da1.SelectCommand.Parameters.AddWithValue("@city", city);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);

                        ans += jm.getArray(names, values, ds1);
                        ans += ",";

                    }

                    ans = ans.Remove(ans.Length - 1);
                    ans += "] }";

                    //status ok
                    //data - Sname
                }
                else
                {
                    //no suburbs
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }

            return ans;
        }

        public string SubmitSuburb(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("delete from User_Suburb where U_Id=@uid", con);
                cmd.Parameters.AddWithValue("@uid", s.Uid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                cmd = new SqlCommand("insert into User_Suburb(U_Id,City,SName) values(@uid,@city,@sname)", con);
                cmd.Parameters.AddWithValue("@uid", s.Uid);
                cmd.Parameters.AddWithValue("@city", s.City);
                cmd.Parameters.AddWithValue("@sname", s.SName);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getCategories(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string where = s.Name == "All" ? "" : " AND CG_Name LIKE '%' + @name + '%'";
                SqlDataAdapter da = new SqlDataAdapter("select * from Categories where CG_Id in (select VN_Category from Vendor_Services where V_Id in (select V_Id from Vendor_Suburb where City=@city AND SName=@sname)) "+where+ " order by CG_Name", con);
                da.SelectCommand.Parameters.AddWithValue("@name", s.Name);
                da.SelectCommand.Parameters.AddWithValue("@city", s.City);
                da.SelectCommand.Parameters.AddWithValue("@sname", s.SName);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - cid,name,image,description
                    return jm.Maker(ds);
                }
                else
                {
                    //no data
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getSubCategories(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string where = s.Name == "All" ? "" : " AND  SCG_Name LIKE '%' + @name + '%'";
                SqlDataAdapter da = new SqlDataAdapter("select * from SubCategories where CG_Id=@cid AND SCG_Id in (select VN_SubCategory from Vendor_Services where V_Id in (select V_Id from Vendor_Suburb where City=@city AND SName=@sname)) "+where+ " order by SCG_Name", con);
                da.SelectCommand.Parameters.AddWithValue("@name", s.Name);
                da.SelectCommand.Parameters.AddWithValue("@cid", s.Cid);
                da.SelectCommand.Parameters.AddWithValue("@city", s.City);
                da.SelectCommand.Parameters.AddWithValue("@sname", s.SName);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - subcatid,catid,name,image,description
                    return jm.Maker(ds);
                }
                else
                {
                    //no data
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getServicesforUser(Vendorservices s)
        {
            //string checksuburb = Helper.getResult("Sid", "User_Suburb", "U_Id", s.Uid);
            SqlConnection con = Database.getSADB();
            List<string> suburbs = new List<string>();
            string ans = "";
            try
            {

               
                //  "Data": {
                //    "Suburbs": [
                //      {
                //        "SName": "Andheri"
                //      },
                //      {
                //        "SName": "Borivali"
                //      }
                //    ]
                //  }

                //cmd = new SqlCommand("delete from User_Suburb where U_Id=@uid", con);
                //cmd.Parameters.AddWithValue("@uid",s.Uid);
                //con.Open();
                //cmd.ExecuteNonQuery();
                //con.Close();

                //JObject data = s.Data;
                //IList<JToken> results = data["Suburbs"].Children().ToList();
                //foreach (JToken result in results)
                //{
                //    Suburbs searchResult = result.ToObject<Suburbs>();
                //    suburbs.Add(searchResult.SName);

                //    cmd = new SqlCommand("insert into User_Suburb(U_Id,SName) values(@uid,@sname)", con);
                //    cmd.Parameters.AddWithValue("@uid", s.Uid);
                //    cmd.Parameters.AddWithValue("@sname", searchResult.SName);
                //    con.Open();
                //    cmd.ExecuteNonQuery();
                //    con.Close();
                //}
                //string inquery = Helper.getINQuery(suburbs);

                string where = "";
                if (s.VNType != "All")
                {
                    where += " AND s.V_Type=@type";
                }

                string bname = "ISNULL((select V_BusinessName from Vendor_Register where V_Id=s.V_Id),'Unknown')";
                string vratings = "ISNULL((select CAST(AVG(CAST(R_Rating AS Float)) as decimal(2,1)) from Rating where V_Id=s.V_Id AND VN_Id='0' AND R_Status='Yes'),'0')";

                string cat = "ISNULL((select CG_Name from Categories where CG_Id=s.VN_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=s.VN_SubCategory),'Unknown')";

                string vidlist = "(select V_Id from Vendor_Suburb where City=@city AND SName=@sname)";

                string query = "select s.VN_Id,s.VS_Id,V_Id," + bname + "," + vratings + ",s.V_Type,s.VN_ServiceName, s.VN_ServiceDescr,s.VN_Price,s.VN_Time,s.VN_Category," + cat + ",s.VN_SubCategory," + subcat + ",s.VN_Status,i.SI_Image1,i.SI_Image2,i.SI_Image3,i.SI_Image4,i.SI_Image5 from Vendor_Services s,Service_Image i where s.VN_Id=i.VN_Id AND s.VN_Category=@cat AND s.VN_SubCategory=@subcat AND s.VN_Status='Enable' AND s.V_Id in " + vidlist + " " + where + " order by s.VN_ServiceName";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@cat", s.VNCat);
                da.SelectCommand.Parameters.AddWithValue("@subcat", s.VNSubcat);
                da.SelectCommand.Parameters.AddWithValue("@city", s.City);
                da.SelectCommand.Parameters.AddWithValue("@sname", s.SName);

                if (s.VNType != "NA")
                {
                    da.SelectCommand.Parameters.AddWithValue("@type", s.VNType);
                }

                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Vnid,Vsid,vid,vname,vrating,type,name,desc,price,time,catid,catname,subcatid,subcatname,status,img1,img2,img3,img4,img5
                    //Vsid can be empty
                    //if rating is 0 i.e there are no ratings
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no
                    ans = jm.Singlevalue("no");
                }
                
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getServicedetails(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string vname = "ISNULL((select CONCAT(V_FirstName,' ',V_LastName) from Vendor_Register where V_Id=s.V_Id),'Unknown')";
                string vratings = "ISNULL((select AVG(CAST(R_Rating AS Float)) from Rating where VN_Id=s.VN_Id AND R_Status='Yes'),'0')";

                string cat = "ISNULL((select CG_Name from Categories where CG_Id=s.VN_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=s.VN_SubCategory),'Unknown')";

                string query = "select s.VN_Id,s.VS_Id,V_Id," + vname + "," + vratings + ",s.V_Type,s.VN_ServiceName, s.VN_ServiceDescr,s.VN_Price,s.VN_Time,s.VN_Category," + cat + ",s.VN_SubCategory," + subcat + ",s.VN_Status,i.SI_Image1,i.SI_Image2,i.SI_Image3,i.SI_Image4,i.SI_Image5 from Vendor_Services s,Service_Image i where s.VN_Id=@vnid AND s.VN_Id=i.VN_Id order by s.VN_ServiceName";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@vnid", s.VNId);

                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Vnid,Vsid,vid,vname,servicerating,type,name,desc,price,time,catid,catname,subcatid,subcatname,status,img1,img2,img3,img4,img5
                    //Vsid can be empty
                    //if srating is 0 i.e there are no ratings
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        //api/Service/getVariant to get Variant

        public string getServiceReviews(Vendorservices v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string uphoto = "ISNULL((select U_Profilepicture from User_Register where U_Id=r.U_Id),'Unknown')";
                string uname = "ISNULL((select CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_Id=r.U_Id),'Unknown')";

                SqlDataAdapter da = new SqlDataAdapter("select r.U_Id,"+uname+","+uphoto+ ",r.R_Rating, r.R_Description,r.R_Datetime from Rating r where r.VN_Id=@vnid AND r.R_Status='Yes'", con);
                da.SelectCommand.Parameters.AddWithValue("@vnid", v.VNId);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //uid,uname,uphoto,rating,review,datetime
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no ratings/reviews given
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Singlevalue(e.Message);
            }
            return ans;
        }

        public string getVendor_Services(Vendorservices s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string sratings = "ISNULL((select AVG(CAST(R_Rating AS Float)) from Rating where VN_Id=s.VN_Id AND R_Status='Yes'),'0')";

                string cat = "ISNULL((select CG_Name from Categories where CG_Id=s.VN_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=s.VN_SubCategory),'Unknown')";

                string query = "select s.VN_Id,s.V_Type,s.VN_ServiceName,s.VN_ServiceDescr,s.VN_Price," + cat + "," + subcat + ","+sratings+" from Vendor_Services s where s.V_Id=@vid AND s.VN_Status='Enable' AND s.V_Type=@type order by s.VN_ServiceName";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@vid", s.Vid);
                da.SelectCommand.Parameters.AddWithValue("@type", s.VNType);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //VNId,type,sname,sdesc,price,catname,subcatname,ratings
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getVendor_Reviews(Vendorservices v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string uphoto = "ISNULL((select U_Profilepicture from User_Register where U_Id=r.U_Id),'Unknown')";
                string uname = "ISNULL((select CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_Id=r.U_Id),'Unknown')";

                SqlDataAdapter da = new SqlDataAdapter("select r.U_Id," + uname + "," + uphoto + ",r.R_Rating, r.R_Description,r.R_Datetime from Rating r where r.V_Id=@vid AND VN_Id='0' AND r.R_Status='Yes'", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", v.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //uid,uname,uphoto,rating,review,datetime
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no ratings/reviews given
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Singlevalue(e.Message);
            }
            return ans;
        }

        public string getVendor_Aboutus(Vendorservices v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select V_FirstName,V_LastName,V_EmailID,V_MobileNo,V_BusinessName,V_Profilepicture,V_Aboutus from Vendor_Register  where V_Id=@vid", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", v.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //fname,lname,email,contact,businessname,vphoto,aboutus
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no vendor found
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Singlevalue(e.Message);
            }
            return ans;
        }

        public string getVendor_Vendordetails(Vendorservices v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string img1 = "ISNULL((select Image1 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img2 = "ISNULL((select Image2 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img3 = "ISNULL((select Image3 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img4 = "ISNULL((select Image4 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img5 = "ISNULL((select Image5 from Vendor_Image where V_Id=r.V_Id),'NA')";

                SqlDataAdapter da = new SqlDataAdapter("select CONCAT(r.V_FirstName,' ',r.V_LastName),"+img1+ "," + img2 + "," + img3 + "," + img4 + "," + img5 + " from Vendor_Register r where  r.V_Id=@vid", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", v.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //name,img1,img2,img3,img4,img5
                    //img can be NA
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no vendor
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }

            return ans;
        }

        public string getServiceReviewsforVendors(Vendorservices v)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string[] names = new string[] { "Ratings" };
                string[] values = new string[names.Length];

                SqlDataAdapter da = new SqlDataAdapter("select * from Rating where VN_Id =@vnid AND R_Status = 'Yes'", con);
                da.SelectCommand.Parameters.AddWithValue("@vnid", v.VNId);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    da = new SqlDataAdapter("select CAST(AVG(CAST(R_Rating AS Float)) as decimal(2,1)) from Rating where VN_Id =@vnid AND R_Status = 'Yes'", con);
                    da.SelectCommand.Parameters.AddWithValue("@vnid", v.VNId);
                    ds = new DataSet();
                    da.Fill(ds);
                    count = ds.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        ans = "{ \"status\" : \"ok\",";
                        ans += "\"Data\" :[ ";

                        values = new string[names.Length];
                        string avgratings = ds.Tables[0].Rows[0][0].ToString();
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = avgratings;
                        }


                        string uphoto = "ISNULL((select U_Profilepicture from User_Register where U_Id=r.U_Id),'Unknown')";
                        string uname = "ISNULL((select CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_Id=r.U_Id),'Unknown')";

                        SqlDataAdapter da1 = new SqlDataAdapter("select r.U_Id," + uname + "," + uphoto + ",r.R_Rating, r.R_Description,r.R_Datetime from Rating r where VN_Id=@vnid AND r.R_Status='Yes'", con);
                        da1.SelectCommand.Parameters.AddWithValue("@vnid", v.VNId);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);

                        //status ok
                        // "Ratings"
                        //data - uid,uname,uphoto,rating,review,datetime
                        ans += jm.getArray(names, values, ds1);
                        ans += "] }";

                    }
                    else
                    {
                        //No ratings or Reviews
                        ans = jm.Singlevalue("no");
                    }
                }
                else
                {
                    //No ratings or Reviews
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Singlevalue(e.Message);
            }
            return ans;
        }

    }

}