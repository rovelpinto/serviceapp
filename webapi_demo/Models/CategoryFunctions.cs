﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
namespace webapi_demo.Models
{
    public class CategoryFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();

        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public class Category
        {
            public string Cid { get; set; }
            public string Name { get; set; }
            public string Image { get; set; }
            public string Desciption { get; set; }

        }

        public class Subcategory
        {
            public string SCid { get; set; }
            public string Cid { get; set; }
            public string Name { get; set; }
            public string Image { get; set; }
            public string Desciption { get; set; }
        }

        public string AddCategory(Category c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select CG_Id from Categories where CG_Name=@name", con);
                da.SelectCommand.Parameters.AddWithValue("@name",c.Name);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Name already exists
                    return jm.Singlevalue("already");
                }

                if (ans == "")
                {
                    string url = Helper.getcategoryURL() + c.Image;

                    cmd = new SqlCommand("insert into Categories(CG_Name,CG_Image,CG_Description) values (@name,@image,@desc)", con);
                    cmd.Parameters.AddWithValue("@name", c.Name);
                    cmd.Parameters.AddWithValue("@image", url);
                    cmd.Parameters.AddWithValue("@desc", c.Desciption);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string UpdateCategory(Category c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select CG_Id from Categories where CG_Name=@name AND CG_Id<>@cid", con);
                da.SelectCommand.Parameters.AddWithValue("@name", c.Name);
                da.SelectCommand.Parameters.AddWithValue("@cid", c.Cid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Name already exists
                    return jm.Singlevalue("already");
                }

                if (ans == "")
                {
                    string url = Helper.getcategoryURL() + c.Image;

                    cmd = new SqlCommand("Update Categories set CG_Name=@name,CG_Image=@image,CG_Description=@desc where CG_Id=@cid", con);
                    cmd.Parameters.AddWithValue("@name", c.Name);
                    cmd.Parameters.AddWithValue("@image", url);
                    cmd.Parameters.AddWithValue("@desc", c.Desciption);
                    cmd.Parameters.AddWithValue("@cid", c.Cid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string DeleteCategory(Category c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select VN_Id from vendor_Services where VN_Category=@cid", con);
                da.SelectCommand.Parameters.AddWithValue("@cid", c.Cid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //false
                    //cannot delete there are services in this category
                    //delete the service or change the category in those services
                    return jm.Singlevalue("false");
                }

                if (ans == "")
                {

                    cmd = new SqlCommand("delete from Categories where CG_Id=@cid", con);
                    cmd.Parameters.AddWithValue("@cid", c.Cid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getCategories(Category c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string where = c.Name == "All" ? "" : " where CG_Name LIKE '%' + @name + '%'";
                SqlDataAdapter da = new SqlDataAdapter("select * from Categories "+where+" order by CG_Name", con);
                da.SelectCommand.Parameters.AddWithValue("@name", c.Name);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - cid,name,image,description
                    return jm.Maker(ds);
                }
                else
                {
                    //no data
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string AddSubcategory(Subcategory c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select CG_Id from SubCategories where SCG_Name=@name AND CG_Id=@cid", con);
                da.SelectCommand.Parameters.AddWithValue("@name", c.Name);
                da.SelectCommand.Parameters.AddWithValue("@cid", c.Cid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Name already exists
                    return jm.Singlevalue("already");
                }

                if (ans == "")
                {
                    string url = Helper.getsubcatURL() + c.Image;

                    cmd = new SqlCommand("insert into SubCategories(CG_Id,SCG_Name,SCG_Image,SCG_Description) values (@cid,@name,@image,@desc)", con);
                    cmd.Parameters.AddWithValue("@cid", c.Cid);
                    cmd.Parameters.AddWithValue("@name", c.Name);
                    cmd.Parameters.AddWithValue("@image", url);
                    cmd.Parameters.AddWithValue("@desc", c.Desciption);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string UpdateSubcategory(Subcategory c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select CG_Id from SubCategories where SCG_Name=@name AND CG_Id=@cid AND SCG_Id<>@scid", con);
                da.SelectCommand.Parameters.AddWithValue("@name", c.Name);
                da.SelectCommand.Parameters.AddWithValue("@cid", c.Cid);
                da.SelectCommand.Parameters.AddWithValue("@scid", c.SCid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //Name already exists
                    return jm.Singlevalue("already");
                }

                if (ans == "")
                {
                    string url = Helper.getsubcatURL() + c.Image;

                    cmd = new SqlCommand("Update SubCategories set SCG_Name=@name,SCG_Image=@image,SCG_Description=@desc where SCG_Id=@scid", con);
                    cmd.Parameters.AddWithValue("@name", c.Name);
                    cmd.Parameters.AddWithValue("@image", url);
                    cmd.Parameters.AddWithValue("@desc", c.Desciption);
                    cmd.Parameters.AddWithValue("@scid", c.SCid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string DeleteSubcategory(Subcategory c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select VN_Id from vendor_Services where VN_SubCategory=@scid", con);
                da.SelectCommand.Parameters.AddWithValue("@scid", c.SCid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //false
                    //cannot delete there are services in this category
                    //delete the service or change the category in those services
                    return jm.Singlevalue("false");
                }

                if (ans == "")
                {

                    cmd = new SqlCommand("delete from Subcategories where SCG_Id=@scid", con);
                    cmd.Parameters.AddWithValue("@scid", c.SCid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getSubcategories(Subcategory c)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string where = c.Name== "All" ? "" : " AND SCG_Name LIKE '%' + @name + '%'";
                SqlDataAdapter da = new SqlDataAdapter("select * from Subcategories where CG_Id=@cid " + where + " order by SCG_Name", con);
                da.SelectCommand.Parameters.AddWithValue("@name", c.Name);
                da.SelectCommand.Parameters.AddWithValue("@cid", c.Cid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - subcatid,cid,name,image,description
                    return jm.Maker(ds);
                }
                else
                {
                    //no data
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

    }

}