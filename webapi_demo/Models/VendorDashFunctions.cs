﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace webapi_demo.Models
{
    public class VendorDashFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();

        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public class VendorD
        {
            public string Vid { get; set; }
            public string Type { get; set; }
        }

        public string getVendordashboad(VendorD u)
        {
            SqlConnection con = Database.getSADB();

            string[] names = new string[] { "Received", "Sent","Accepted","Completed", "Earnings" };
            string[] values = new string[names.Length];

            int tot;
            string ans = "";
            try
            {
                DateTime NZcurtime = DateTime.ParseExact(Helper.getdatetime(3), "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                string lastdate = DateTime.DaysInMonth(NZcurtime.Year, NZcurtime.Month)+"";
                string sdt = "";
                string edt = "";
                if (u.Type == "Yearly")
                {
                    tot = 12;
                    DateTime year = NZcurtime.AddYears(-1);
                    sdt = year.Year + "-" + year.Month + "-01 00:00";
                    edt = NZcurtime.Year + "-" + NZcurtime.Month +"-"+ lastdate + " 23:59";
                }
                else if (u.Type == "Half Yearly")
                {
                    tot = 6;
                    DateTime halfyear = NZcurtime.AddMonths(-6);
                    sdt = halfyear.Year + "-" + Helper.getExtra(halfyear.Month) + "-01 00:00";
                    edt = NZcurtime.Year + "-" + Helper.getExtra(NZcurtime.Month) + "-" + lastdate + " 23:59";
                }
                else
                {
                    tot = Convert.ToInt32(Helper.getdatetime(5));
                    sdt = NZcurtime.Year + "-" + Helper.getExtra(NZcurtime.Month) + "-01 00:00";
                    edt = NZcurtime.Year + "-" + Helper.getExtra(NZcurtime.Month) + "-" + Helper.getExtra(tot) + " 23:59";
                }


                SqlDataAdapter da = new SqlDataAdapter("select V_Id from Vendor_Register where V_Id=@vid", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", u.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    List<string> mainvalues = getData(u.Vid, sdt, edt);

                    ans = "{ \"status\" : \"ok\",";
                    ans += "\"Data\" :[ ";

                    values = new string[names.Length];

                    for(int i=0;i<mainvalues.Count;i++)
                    {
                        values[i] = mainvalues[i];
                    }

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Month");
                    dt.Columns.Add(names[4]);

                    DateTime newdt = NZcurtime;
                    for (int i=0;i<tot;i++)
                    {
                        if (u.Type == "Monthly")
                        {
                            int DD = (tot - i);

                            sdt = newdt.Year + "-" + Helper.getExtra(newdt.Month) + "-"+Helper.getExtra(DD)+" 00:00";
                            edt = newdt.Year + "-" + Helper.getExtra(newdt.Month) + "-"+ Helper.getExtra(DD) + " 23:59";

                            List<string> newvalues = getData(u.Vid, sdt, edt);

                            dt.Rows.Add(Helper.getExtra(DD)+" "+newdt.ToString("MMM"), newvalues[4]);
                        }
                        else
                        {

                            if (i != 0)
                            {
                                newdt = newdt.AddMonths(-1);
                            }
                            lastdate = DateTime.DaysInMonth(newdt.Year, newdt.Month) + "";

                            sdt = newdt.Year + "-" + Helper.getExtra(newdt.Month) + "-01 00:00";
                            edt = newdt.Year + "-" + Helper.getExtra(newdt.Month) + "-" + lastdate + " 23:59";
                            List<string> newvalues = getData(u.Vid, sdt, edt);

                            dt.Rows.Add(newdt.ToString("MMM yy"), newvalues[4]);
                        }
                    }


                    //status ok
                    // Data - "Received", "Sent","Accepted","Completed", "Earnings"
                    // datastatus - yes/no
                    // Data - data0 - month year
                    //      - data1 - earnings
                    DataTable dt1 = dt.AsEnumerable().Reverse().CopyToDataTable();
                    ans+= jm.getArray1(names,values,dt1);
                    ans += "] }";

                }
                else
                {
                    //user doesnt exists
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public List<string> getData(string vid,string sdt,string edt)
        {
            SqlConnection con = Database.getSADB();

            List<string> ans = new List<string>();

            string between = " BETWEEN '" + sdt + "' AND '" + edt + "'";

            string requestrecieved = "(select COUNT(V_Id) from Quote_Request where V_Id=v.V_Id AND QR_DateTime " + between + ")";
            string offerssent = "(select COUNT(V_ID) from Offer where V_ID=v.V_Id AND O_DateTime " + between + ")";
            string acceptedoffers = "(select COUNT(V_ID) from Offer where V_ID=v.V_Id AND O_Status='Accepted' AND O_DateTime " + between + ")";
            string completedoffers = "(select COUNT(QSB_Id) from Quote_ServiceBooking where v_Id=v.V_Id AND QSB_Status='Completed' AND QSB_DateTime " + between + ")";


            string Directbooking = "ISNULL((select SUM(CAST(DSB_Amount AS float)) from Direct_ServiceBooking where V_ID=v.V_Id AND DSB_Status='Completed' AND DSB_DateTime " + between + "),'0')";

            string Quoteoffer = "ISNULL((select SUM(CAST(QSB_Amount AS float)) from Quote_ServiceBooking where V_ID=v.V_Id AND QSB_Status='Completed' AND QSB_DateTime " + between + "),0)";

            string query = "select " + requestrecieved + "," + offerssent + "," + acceptedoffers + "," + completedoffers + "," + Directbooking + "," + Quoteoffer + " from Vendor_Register v where v.V_Id=@vid";

            SqlDataAdapter da = new SqlDataAdapter(query, con);
            da.SelectCommand.Parameters.AddWithValue("@vid", vid);
            DataSet ds = new DataSet();
            da.Fill(ds);
            int count = ds.Tables[0].Rows.Count;
            if (count > 0)
            {
                string rr = ds.Tables[0].Rows[0][0].ToString();
                string os = ds.Tables[0].Rows[0][1].ToString();
                string ao = ds.Tables[0].Rows[0][2].ToString();
                string co = ds.Tables[0].Rows[0][3].ToString();
                string db = ds.Tables[0].Rows[0][4].ToString();
                string qb = ds.Tables[0].Rows[0][5].ToString();

                double total = double.Parse(db) + double.Parse(qb);

                ans.Add(rr);
                ans.Add(os);
                ans.Add(ao);
                ans.Add(co);
                ans.Add(total+"");
            }

            return ans;
        }

    }
}