﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace webapi_demo.Models
{
    public class BookingsFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();


        //status Service Booking
        // Pending, Accepted, Rejected, Completed, Cancelled


        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public class QuoteRequest
        {
            public string RequestType { get; set; }
            public string QRid { get; set; }
            public string Uid { get; set; }
            public string Vid { get; set; }
            public string VNCat { get; set; }
            public string VNSubcat { get; set; }
            public string Address { get; set; }
            public string QTitle { get; set; }
            public string QDesc { get; set; }
            public string QDatetime { get; set; }
            public string QJobtype { get; set; }
            public string QJobDate { get; set; }
            public string QJobTime { get; set; }
            public string QJobPreference { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string Img5 { get; set; }
            public JObject Data { get; set; }
            public string Datetime { get; set; }
            public string Status { get; set; }
            public string Message { get; set; }
            public string Amount { get; set; }
            public string StripeId { get; set; }
            public string StripeResponse { get; set; }
            public string StripeReceipt { get; set; }
        }

        public class Vendors
        {
            public string Vid { get; set; }
            public string VNId { get; set; }
            public string VNName { get; set; }
            public string VNPrice { get; set; }
        }

        public class BillingAddress
        {
            public string Aid { set; get; }
            public string Uid { set; get; }
            public string Atype { set; get; }
            public string Address { set; get; }
        }

        public class Ratings
        {
            public string Uid { set; get; }
            public string QRid { set; get; }
            public string Rating { set; get; }
            public string Review { set; get; }
        }

        //-----------------------------------------USER APP---------------------------------------------

        public string getAddress(BillingAddress a)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select A_Id,A_Type,A_BillingAddress from [Address] where U_Id=@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", a.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - aid,type,address
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no address found
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string AddAddress(BillingAddress a)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from [Address] where u_Id=@uid AND A_Type=@type AND A_BillingAddress=@add", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", a.Uid);
                da.SelectCommand.Parameters.AddWithValue("@type", a.Atype);
                da.SelectCommand.Parameters.AddWithValue("@add", a.Address);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if(count>0)
                {
                    //already
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("insert into [Address](U_Id,A_Type,A_BillingAddress) values(@uid,@type,@add)", con);
                    cmd.Parameters.AddWithValue("@uid", a.Uid);
                    cmd.Parameters.AddWithValue("@type", a.Atype);
                    cmd.Parameters.AddWithValue("@add", a.Address);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");

                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string UpdateAddress(BillingAddress a)
        {
            string uid = Helper.getResult("U_Id", "[Address]", "A_Id", a.Aid);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from [Address] where u_Id=@uid AND A_Type=@type AND A_BillingAddress=@add AND A_Id<>@aid", con);
                da.SelectCommand.Parameters.AddWithValue("@aid", a.Aid);
                da.SelectCommand.Parameters.AddWithValue("@uid", uid);
                da.SelectCommand.Parameters.AddWithValue("@type", a.Atype);
                da.SelectCommand.Parameters.AddWithValue("@add", a.Address);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //already
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("update [Address] set A_Type=@type,A_BillingAddress=@add where A_Id=@aid", con);
                    cmd.Parameters.AddWithValue("@aid", a.Aid);
                    cmd.Parameters.AddWithValue("@type", a.Atype);
                    cmd.Parameters.AddWithValue("@add", a.Address);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string DeleteAddress(BillingAddress a)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("delete from [Address] where A_Id=@aid", con);
                cmd.Parameters.AddWithValue("@aid", a.Aid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string RequestBooking(QuoteRequest q)
        {
            string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "user_Register", "U_Id", q.Uid);
            string uemail = Helper.getResult("U_EmailID", "user_Register", "U_Id", q.Uid);

            

            StringBuilder useremailbody = new StringBuilder();
            StringBuilder vendoremailbody = new StringBuilder();

            SqlConnection con = Database.getSADB();
            List<Vendors> vlist = new List<Vendors>();
            string ans = "";
            string dt = Helper.getdatetime(3);
            try
            {
                // add to qouterequest & Images
                // add to Direct or Qoute servicebooking
                // add to Chat
                // add to notification

                //parse vendor array 
                JObject data = q.Data;
                IList<JToken> results = data["Vendors"].Children().ToList();
                foreach (JToken result in results)
                {
                    Vendors searchResult = result.ToObject<Vendors>();
                    vlist.Add(searchResult);
                }


                for(int i=0;i<vlist.Count;i++)
                {
                    Vendors v = vlist[i];

                    //qoute Request
                    cmd = new SqlCommand("insert into Quote_Request(V_Id,U_Id,VN_Id,VN_ServiceName,VN_Price,QR_Category,QR_SubCategory,QR_Address,QR_Title,QR_Description,QR_DateTime,QR_Jobtype,QR_JobDate,QR_JobTime,QR_JobPreference,QR_Bookingtype) values(@vid,@uid,@vnid,@sname,@price,@cat,@subcat,@add,@title,@dec,@dt,@type,@date,@time,@preference,@btype)", con);
                    cmd.Parameters.AddWithValue("@vid", v.Vid);
                    cmd.Parameters.AddWithValue("@uid", q.Uid);
                    cmd.Parameters.AddWithValue("@vnid", v.VNId);
                    cmd.Parameters.AddWithValue("@sname", v.VNName);
                    cmd.Parameters.AddWithValue("@price", v.VNPrice);
                    cmd.Parameters.AddWithValue("@cat", q.VNCat);
                    cmd.Parameters.AddWithValue("@subcat", q.VNSubcat);
                    cmd.Parameters.AddWithValue("@add", q.Address);
                    cmd.Parameters.AddWithValue("@title", q.QTitle);
                    cmd.Parameters.AddWithValue("@dec", q.QDesc);
                    cmd.Parameters.AddWithValue("@dt", dt);
                    cmd.Parameters.AddWithValue("@type", q.QJobtype);
                    cmd.Parameters.AddWithValue("@date", q.QJobDate);
                    cmd.Parameters.AddWithValue("@time", q.QJobTime);
                    cmd.Parameters.AddWithValue("@preference", q.QJobPreference);
                    cmd.Parameters.AddWithValue("@btype", q.RequestType);
                    con.Open();
                    cmd.ExecuteNonQuery();

                    //getqouteid
                    SqlDataAdapter da2 = new SqlDataAdapter("select Top 1 @@IDENTITY from Quote_Request order by QR_Id DESC", con);
                    DataSet ds2 = new DataSet();
                    da2.Fill(ds2);
                    string QRId = ds2.Tables[0].Rows[0][0].ToString();
                    con.Close();

                    string vendorname = Helper.getResult("V_BusinessName", "Vendor_Register", "V_Id", v.Vid);
                    string vendoremail = Helper.getResult("V_EmailID", "Vendor_Register", "V_Id", v.Vid);

                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", QRId }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", v.VNName }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Vendor", vendorname }));
                    useremailbody.Append(string.Format(Helper.EMAIL_LINE));

                    string url1 = q.Img1 == "NA" ? q.Img1 : Helper.getrequestURL() + q.Img1;
                    string url2 = q.Img2 == "NA" ? q.Img2 : Helper.getrequestURL() + q.Img2;
                    string url3 = q.Img3 == "NA" ? q.Img3 : Helper.getrequestURL() + q.Img3;
                    string url4 = q.Img4 == "NA" ? q.Img4 : Helper.getrequestURL() + q.Img4;
                    string url5 = q.Img5 == "NA" ? q.Img5 : Helper.getrequestURL() + q.Img5;

                    //add qoute image
                    cmd = new SqlCommand("insert into Quote_Image(QR_Id,QI_Image1,QI_Image2,QI_Image3,QI_Image4,QI_Image5) values(@qrid,@img1,@img2,@img3,@img4,@img5)", con);
                    cmd.Parameters.AddWithValue("qrid", QRId);
                    cmd.Parameters.AddWithValue("img1", url1);
                    cmd.Parameters.AddWithValue("img2", url2);
                    cmd.Parameters.AddWithValue("img3", url3);
                    cmd.Parameters.AddWithValue("img4", url4);
                    cmd.Parameters.AddWithValue("img5", url5);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    if(q.RequestType== "Direct")
                    {
                        //add to Direct Service Booking
                        cmd = new SqlCommand("insert into Direct_ServiceBooking(QR_Id,VN_Id,U_Id,V_Id,DSB_DateTime,DSB_Amount,DSB_Status,DSB_PaymentStatus) values(@qrid,@vnid,@uid,@vid,@dt,@amount,@status,@paystatus)", con);
                        cmd.Parameters.AddWithValue("@qrid", QRId);
                        cmd.Parameters.AddWithValue("@vnid", v.VNId);
                        cmd.Parameters.AddWithValue("@uid", q.Uid);
                        cmd.Parameters.AddWithValue("@vid", v.Vid);
                        cmd.Parameters.AddWithValue("@dt", dt);
                        cmd.Parameters.AddWithValue("@amount",v.VNPrice);
                        cmd.Parameters.AddWithValue("@status","Pending");
                        cmd.Parameters.AddWithValue("@paystatus", "Unpaid");
                        con.Open();
                        cmd.ExecuteNonQuery();

                        SqlDataAdapter da3 = new SqlDataAdapter("select Top 1 @@IDENTITY from Direct_ServiceBooking order by DSB_Id DESC", con);
                        DataSet ds3 = new DataSet();
                        da2.Fill(ds3);
                        string DSBId = ds3.Tables[0].Rows[0][0].ToString();
                        con.Close();

                        //string res=Helper.AddtoChat("Direct", QRId, q.Uid, v.Vid, "User", "DirectRequest", DSBId, "New Booking", q.QDatetime,uname, v.VNName);

                        Helper.Addnotification("Request for " + v.VNName, "Request is placed by " + uname, dt, "DirectRequest", v.Vid, "Vendor", QRId);
                    }
                    else
                    {
                        //add to Quote Service Booking
                        cmd = new SqlCommand("insert into Quote_ServiceBooking(QR_Id,VN_Id,U_Id,V_Id,QSB_DateTime,QSB_Amount,QSB_Status,QSB_PaymentStatus) values(@qrid,@vnid,@uid,@vid,@dt,@amount,@status,@paystatus)", con);
                        cmd.Parameters.AddWithValue("@qrid", QRId);
                        cmd.Parameters.AddWithValue("@vnid", v.VNId);
                        cmd.Parameters.AddWithValue("@uid", q.Uid);
                        cmd.Parameters.AddWithValue("@vid", v.Vid);
                        cmd.Parameters.AddWithValue("@dt", dt);
                        cmd.Parameters.AddWithValue("@amount", v.VNPrice);
                        cmd.Parameters.AddWithValue("@status", "Pending");
                        cmd.Parameters.AddWithValue("@paystatus", "Unpaid");
                        con.Open();
                        cmd.ExecuteNonQuery();

                        SqlDataAdapter da3 = new SqlDataAdapter("select Top 1 @@IDENTITY from Quote_ServiceBooking order by QSB_Id DESC", con);
                        DataSet ds3 = new DataSet();
                        da2.Fill(ds3);
                        string QSBId = ds3.Tables[0].Rows[0][0].ToString();
                        con.Close();

                        //string res = Helper.AddtoChat("Quote", QRId, q.Uid, v.Vid, "User", "QuoteRequest", QSBId, "New Booking", q.QDatetime, uname, v.VNName);

                        Helper.Addnotification("Request for " + v.VNName, "Request is placed by " + uname, dt, "QuoteRequest", v.Vid, "Vendor", QRId);
                    }

                    vendoremailbody = new StringBuilder();

                    vendoremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", QRId }));
                    vendoremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", v.VNName }));
                    vendoremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Customer Name", uname }));
                    vendoremailbody.Append(string.Format(Helper.EMAIL_LINE));
                    vendoremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Booking Date", dt }));
                    string vpref = q.QJobPreference == "Specific Date Time" ? "Specific Date Time" + "(" + q.QJobDate + " " + q.QJobTime + ")" : q.QJobPreference;
                    vendoremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Prefered Date", vpref }));
                    vendoremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Booking Type", q.RequestType }));

                    string vpath = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/vendor_requestbooking.html");
                    string vcontent = System.IO.File.ReadAllText(vpath);

                    vcontent = vcontent.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    vcontent = vcontent.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    vcontent = vcontent.Replace("@address??", Helper.EMAIL_ADDRESS);
                    vcontent = vcontent.Replace("@name??", vendorname);
                    vcontent = vcontent.Replace("@signature??", Helper.EMAIL_SIGNATURE);
                    vcontent = vcontent.Replace("@bookingdetails??", vendoremailbody.ToString());

                    string vsubject = "New Service Request";

                    Helper.sendEmail(new string[] { vendoremail }, vsubject, vcontent);

                }

                string pref = q.QJobPreference == "Specific Date Time" ? "Specific Date Time" + "(" + q.QJobDate + " " + q.QJobTime + ")" : q.QJobPreference;

                useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Booking Date", dt }));
                useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Prefered Date", pref }));
                useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Booking Type", q.RequestType }));

                string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/user_requestbooking.html");
                string content = System.IO.File.ReadAllText(path);

                content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                content = content.Replace("@name??", uname);
                content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);
                content = content.Replace("@bookingdetails??", useremailbody.ToString());

                string subject = "Your Service Request is Submitted.";

                Helper.sendEmail(new string[] { uemail }, subject, content);

                //true
                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_Bookings(BillingAddress a)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string bname = "ISNULL((select V_BusinessName from vendor_Register where V_Id=q.V_Id),'Unknown')";
                string cat = "ISNULL((select CG_Name from Categories where CG_Id=q.QR_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=q.QR_SubCategory),'Unknown')";
                string status = "(CASE WHEN q.QR_Bookingtype='Direct' THEN ISNULL((select DSB_Status from Direct_ServiceBooking where QR_Id=q.QR_Id),'Unknown') ELSE ISNULL((select QSB_Status from Quote_ServiceBooking where QR_Id=q.QR_Id),'Unknown') END)";
                string canviewchat = "(CASE WHEN q.QR_Bookingtype='Direct' THEN (select(CASE WHEN COUNT(M_Id) > 0 THEN 'Yes' Else 'No' END) from Chat where BType = 'Direct' AND Bid = q.QR_Id) ELSE(select(CASE WHEN COUNT(M_Id) > 0 THEN 'Yes' Else 'No' END) from Chat where BType = 'Quote' AND Bid = q.QR_Id) END)";

                string isservicerated = "ISNULL((select TOP 1 'Yes' from Rating where QR_Id=q.QR_Id AND U_Id=q.U_id),'No')";
                string isvendorrated = "ISNULL((select TOP 1 'Yes' from Rating where V_id=q.V_Id AND U_Id=q.U_id AND QR_Id='0'),'No')";

                string paymentstatus = "(select SB_PaymentStatus from vw_quotedetails where QR_Id=q.QR_Id)";

                string canpay = "(select ( CASE WHEN SB_PaymentStatus='Unpaid' AND SB_Status='Accepted' AND QR_Bookingtype='Direct' AND QR_JobDate<>'NA' AND QR_JobTime<>'NA' THEN 'Yes' ELSE 'No' END) from vw_quotedetails where QR_Id=q.QR_Id) as canpay";

                string price = "(CASE WHEN q.QR_Bookingtype = 'Direct' THEN q.VN_Price ELSE(CASE WHEN(select COUNT(O_Amount) from vw_quoteoffers where QR_Id = q.QR_Id AND O_Status = 'Accepted') > 0 THEN(select TOP 1 O_Amount from vw_quoteoffers where QR_Id = q.QR_Id AND O_Status = 'Accepted') ELSE q.VN_Price END) END)";

                string pricesource= "(CASE WHEN q.QR_Bookingtype = 'Direct' THEN 'Original Price' ELSE (CASE WHEN(select COUNT(O_Amount) from vw_quoteoffers where QR_Id = q.QR_Id AND O_Status = 'Accepted') > 0 THEN 'Negotiated Price' ELSE 'Original Price' END) END)";

                string query = "select q.QR_Id,q.V_Id," + bname + ",q.VN_Id,q.VN_ServiceName,q.VN_Price," + cat + "," + subcat + ",q.QR_Datetime,q.QR_JobDate,q.QR_JobTime," + status + "," + canviewchat + "," + isservicerated + "," + isvendorrated + ","+ paymentstatus + ","+canpay+","+ price + ","+pricesource+" from Quote_Request q where q.U_Id=@uid order by q.QR_Id DESC";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@uid", a.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - qid,vid,bname,vnid,servicename,price,cat,subcat,dt,jdate,jtime,status,chatstatus,isservicerated,isvendorrated,payementstatus,canpay,price,pricesource
                    //chatstatus - Yes/No (Yes chat is enabled/No is disabled)
                    //isservicerated - Yes/No (Yes dont show button/No show button)
                    //isvendorrated - Yes/No (Yes dont show button/No show button)
                    //canpay - Yes/No (Yes show button/dont show button)

                    ans = jm.Maker(ds);
                }
                else
                {
                    //no bookings
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;

        }

        public string User_BookingDetails(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string bname = "ISNULL((select V_BusinessName from vendor_Register where V_Id=q.V_Id),'Unknown')";
                string vname = "ISNULL((select CONCAT(V_FirstName,' ',V_LastName) from vendor_Register where V_Id=q.V_Id),'Unknown')";
                string cat = "ISNULL((select CG_Name from Categories where CG_Id=q.QR_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=q.QR_SubCategory),'Unknown')";
                string status = "(CASE WHEN q.QR_Bookingtype='Direct' THEN ISNULL((select DSB_Status from Direct_ServiceBooking where QR_Id=q.QR_Id),'Unknown') ELSE ISNULL((select QSB_Status from Quote_ServiceBooking where QR_Id=q.QR_Id),'Unknown') END)";
                string payment = "(select SB_PaymentStatus from vw_quotedetails where QR_Id=q.QR_Id)";

                SqlDataAdapter da = new SqlDataAdapter("select q.QR_Id,q.V_Id," + bname + ","+vname+",q.VN_Id,q.VN_ServiceName,q.VN_Price," + cat + "," + subcat + ",q.QR_Address,q.QR_Title,q.QR_Description,q.QR_Datetime,q.QR_JobDate,q.QR_JobTime,q.QR_JobPreference,"+payment+"," + status + ",q.QR_Jobtype,i.QI_Image1,i.QI_Image2,i.QI_Image3,i.QI_Image4,i.QI_Image5 from Quote_Request q,Quote_Image i where q.QR_Id=@qid AND q.QR_Id=i.QR_Id order by q.QR_Id DESC", con);
                da.SelectCommand.Parameters.AddWithValue("@qid", q.QRid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - qid,vi,bname,vname,vnid,sname,price,catname,subcatname,address,title,description,dt,jobdate,jobtime, jobpreference,paymenttype,status,jobtype,image1,image2,image3,image4,image5
                    //if images are not added, they will return NA
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no booking found
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_CancelBooking(QuoteRequest q)
        {
            string type = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", q.QRid);
            string vid = Helper.getResult("V_Id", "Quote_Request", "QR_Id", q.QRid);
            string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "user_Register", "U_Id", q.Uid);
            string task = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", q.QRid);
            string vendorname = Helper.getResult("V_BusinessName", "Vendor_Register", "V_Id", vid);
            string vendoremail = Helper.getResult("V_EmailID", "Vendor_Register", "V_Id", vid);
            string dt = Helper.getdatetime(3);

            string QRstatus = "";
            
            if(type=="Direct")
            {
                string dsbid = Helper.getResult("DSB_Id", "Direct_ServiceBooking", "QR_Id", q.QRid);
                if (dsbid != "no")
                {
                    QRstatus = Helper.getResult("DSB_Status", "Direct_ServiceBooking", "DSB_Id", dsbid);
                }
            }
            else
            {
                string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", q.QRid);
                if (qsbid != "no")
                {
                    QRstatus = Helper.getResult("QSB_Status", "Quote_ServiceBooking", "QSB_Id", qsbid);
                }
            }
            
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                if (type!="no")
                {
                    if (QRstatus == "Pending")
                    {
                        if (type == "Direct")
                        {
                            cmd = new SqlCommand("update Direct_ServiceBooking set DSB_Status=@status where QR_Id=@qid", con);
                            cmd.Parameters.AddWithValue("@status", "Cancelled");
                            cmd.Parameters.AddWithValue("@qid", q.QRid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();

                            //Helper.AddtoChat("Direct", q.QRid, q.Uid, vid, "User", "Cancel", "NA", "NA", q.QDatetime, uname, task);

                            Helper.Addnotification("Booking Cancelled - " + task, "Booking Cancelled by " + uname, dt, "DirectRequest", vid, "Vendor", q.QRid);

                            //true
                            ans = jm.Singlevalue("true");
                        }
                        else //Multitple
                        {
                            cmd = new SqlCommand("update Quote_ServiceBooking set QSB_Status=@status where QR_Id=@qid", con);
                            cmd.Parameters.AddWithValue("@status", "Cancelled");
                            cmd.Parameters.AddWithValue("@qid", q.QRid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();

                            //Helper.AddtoChat("Quote", q.QRid, q.Uid, vid, "User", "Cancel", "NA", "NA", q.QDatetime, uname, task);

                            Helper.Addnotification("Booking Cancelled - " + task, "Booking Cancelled by " + uname, dt, "QuoteRequest", vid, "Vendor", q.QRid);

                            //true
                            ans = jm.Singlevalue("true");
                        }

                        StringBuilder emailbody = new StringBuilder();

                        string statustitle = string.Format(Helper.EMAIL_STATUSCHANGE_TITLE, new string[] { q.QRid, uname });

                        emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", q.QRid }));
                        emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", task }));
                        emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Customer", uname }));

                        string status = string.Format(Helper.EMAIL_STATUS, new string[] { Helper.getcolor("Cancelled"), "Cancelled" });

                        string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/statuschange.html");
                        string content = System.IO.File.ReadAllText(path);

                        content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                        content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                        content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                        content = content.Replace("@name??", vendorname);
                        content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                        content = content.Replace("@intro??", statustitle);
                        content = content.Replace("@bookingdetails??", emailbody.ToString());
                        content = content.Replace("@status??", status);

                        string subject = "Service Booking is Cancelled";

                        Helper.sendEmail(new string[] { vendoremail }, subject, content);
                    }
                    else
                    {
                        //false
                        //Request cannot be cancelled, Contact Vendor
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //no booking found
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_SubmitServiceRatings(Ratings r)
        {
            string ans = "";
            string type = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", r.QRid);
            string Vnid = Helper.getResult("VN_Id", "Quote_Request", "QR_Id", r.QRid);
            string Vid = Helper.getResult("V_Id", "Quote_Request", "QR_Id", r.QRid);
            string dsbid = Helper.getResult("DSB_Id", "Direct_ServiceBooking", "QR_Id", r.QRid);
            string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", r.QRid);
            dsbid = dsbid == "no" ? "0" : dsbid;
            qsbid = qsbid == "no" ? "0" : qsbid;
            string dt = Helper.getdatetime(3);
            
            SqlConnection con = Database.getSADB();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from Rating where U_Id=@uid AND V_Id=@vid AND VN_Id=@vnid AND QR_Id=@qrid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", r.Uid);
                da.SelectCommand.Parameters.AddWithValue("@vid", Vid);
                da.SelectCommand.Parameters.AddWithValue("@vnid", Vnid);
                da.SelectCommand.Parameters.AddWithValue("@qrid", r.QRid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count=ds.Tables[0].Rows.Count;
                if(count>0)
                {
                    //already
                    //You have already given your feedback for this Order
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("insert into Rating(U_Id,V_Id,VN_Id,QR_Id,DSB_Id,QSB_Id,R_Rating,R_Description,R_DateTime,R_Status) values(@uid,@vid,@vnid,@qrid,@dsbid,@qsbid,@rating,@review,@dt,@status)", con);
                    cmd.Parameters.AddWithValue("@uid", r.Uid);
                    cmd.Parameters.AddWithValue("@vid", Vid);
                    cmd.Parameters.AddWithValue("@vnid", Vnid);
                    cmd.Parameters.AddWithValue("@qrid", r.QRid);
                    cmd.Parameters.AddWithValue("@dsbid", dsbid);
                    cmd.Parameters.AddWithValue("@qsbid", qsbid);
                    cmd.Parameters.AddWithValue("@rating", r.Rating);
                    cmd.Parameters.AddWithValue("@review", r.Review);
                    cmd.Parameters.AddWithValue("@dt",dt);
                    cmd.Parameters.AddWithValue("@status","Yes");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch(Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_SubmitVendorRatings(Ratings r)
        {
            string ans = "";
            string Vnid = "0";
            string Vid = Helper.getResult("V_Id", "Quote_Request", "QR_Id", r.QRid);
            string dsbid = "0";
            string qsbid = "0";
            string dt = Helper.getdatetime(3);

            SqlConnection con = Database.getSADB();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from Rating where U_Id=@uid AND V_Id=@vid AND VN_Id=@vnid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", r.Uid);
                da.SelectCommand.Parameters.AddWithValue("@vid", Vid);
                da.SelectCommand.Parameters.AddWithValue("@vnid", Vnid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //already
                    //You have already given your feedback for this Vendor
                    ans = jm.Singlevalue("already");
                }
                else
                {
                    cmd = new SqlCommand("insert into Rating(U_Id,V_Id,VN_Id,QR_Id,DSB_Id,QSB_Id,R_Rating,R_Description,R_DateTime,R_Status) values(@uid,@vid,@vnid,@qrid,@dsbid,@qsbid,@rating,@review,@dt,@status)", con);
                    cmd.Parameters.AddWithValue("@uid", r.Uid);
                    cmd.Parameters.AddWithValue("@vid", Vid);
                    cmd.Parameters.AddWithValue("@vnid", Vnid);
                    cmd.Parameters.AddWithValue("@qrid", "0");
                    cmd.Parameters.AddWithValue("@dsbid", dsbid);
                    cmd.Parameters.AddWithValue("@qsbid", qsbid);
                    cmd.Parameters.AddWithValue("@rating", r.Rating);
                    cmd.Parameters.AddWithValue("@review", r.Review);
                    cmd.Parameters.AddWithValue("@dt", dt);
                    cmd.Parameters.AddWithValue("@status", "Yes");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_CheckBookingPaid(QuoteRequest q)
        {
            string type = Helper.getResult("QR_Bookingtype", "vw_quotedetails", "QR_Id", q.QRid);
            string QRstatus = Helper.getResult("SB_Status", "vw_quotedetails", "QR_Id", q.QRid);
            string paystatus = Helper.getResult("SB_PaymentStatus", "vw_quotedetails", "QR_Id", q.QRid);

            string VN_Price = Helper.getResult("VN_Price", "vw_quotedetails", "QR_Id", q.QRid);
            string VN_ServiceName = Helper.getResult("VN_ServiceName", "vw_quotedetails", "QR_Id", q.QRid);
            string QR_Title = Helper.getResult("QR_Title", "vw_quotedetails", "QR_Id", q.QRid);

            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                if (type != "no")
                {
                    if (QRstatus == "Accepted")
                    {
                        if (type == "Direct")
                        {
                            if(paystatus=="Paid")
                            {
                                //already
                                //You have already paid for this Service Booking
                                ans = jm.Singlevalue("already");
                                return ans;
                            }


                            DataTable dt = new DataTable();
                            dt.Columns.Add("link");

                            DataRow drow = dt.NewRow();

                            string paylink = string.Format(Helper.FULL_PAYMENT_LINK_DIRECT, new string[] { q.QRid, VN_Price, VN_ServiceName, QR_Title, "Direct" });

                            dt.Rows.Add(paylink);

                            //ok
                            //data0 - link
                            //you can proceed with the payment
                            ans = jm.Maker1(dt);
                        }
                        else 
                        {
                            //false
                            //Cannot make payments for this Booking
                            ans = jm.Singlevalue("false");
                        }

                    }
                    else
                    {
                        //false
                        //Cannot make payments for this Booking
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //no
                    //no booking found
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_BookingPaid(QuoteRequest q)
        {
            string Stripestatus = Helper.getResult("Status", "[Transactions]", "Stripe_TransId", q.StripeId);
            string type = Helper.getResult("QR_Bookingtype", "vw_quotedetails", "QR_Id", q.QRid);
            string amount = Helper.getResult("SB_Amount", "vw_quotedetails", "QR_Id", q.QRid);
            string vid = Helper.getResult("V_Id", "vw_quotedetails", "QR_Id", q.QRid);
            string uid = Helper.getResult("U_Id", "vw_quotedetails", "QR_Id", q.QRid);
            string date = Helper.getResult("QR_JobDate", "vw_quotedetails", "QR_Id", q.QRid);
            string time = Helper.getResult("QR_JobTime", "vw_quotedetails", "QR_Id", q.QRid);
            string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "user_Register", "U_Id", uid);
            string uemail = Helper.getResult("U_EmailID", "user_Register", "U_Id", uid);
            string task = Helper.getResult("VN_ServiceName", "vw_quotedetails", "QR_Id", q.QRid);
            string vendorname = Helper.getResult("V_BusinessName", "Vendor_Register", "V_Id", vid);
            string vendoremail = Helper.getResult("V_EmailID", "Vendor_Register", "V_Id", vid);
            string QRstatus = Helper.getResult("SB_Status", "vw_quotedetails", "QR_Id", q.QRid);
            string paystatus = Helper.getResult("SB_PaymentStatus", "vw_quotedetails", "QR_Id", q.QRid);
            string dt = Helper.getdatetime(3);

            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                if (q.Status == "succeeded")
                //if (Stripestatus.ToLower() == "paid")   
                {
                    cmd = new SqlCommand("update Direct_ServiceBooking set DSB_PaymentStatus=@status where QR_Id=@qid", con);
                    cmd.Parameters.AddWithValue("@status", "Paid");
                    cmd.Parameters.AddWithValue("@qid", q.QRid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    Helper.Addnotification("Payment Complete - " + task, "Payment is made by " + uname, dt, "DirectRequest", vid, "Vendor", q.QRid);

                    Helper.Addnotification("Payment Complete - " + task, "Your Payment is Recieved", dt, "DirectRequest", uid, "User", q.QRid);

                    Helper.Addtransaction(q.QRid, uid, vid, q.Amount, dt, q.Status, q.StripeId, q.StripeResponse, q.StripeReceipt);
                    //User email
                    StringBuilder useremailbody = new StringBuilder();

                    string ustatustitle = string.Format(Helper.EMAIL_PAYMENT_DONE_USER, new string[] { amount, "Successfull", q.QRid });

                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", q.QRid }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", task }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Vendor", vendorname }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Offer Amount", amount }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Date", date }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Time", time }));
                    //useremailbody.Append(string.Format(Helper.EMAIL_PAYMENT_RECIEPT, new string[] {q.StripeReceipt}));

                    string ue_status = string.Format(Helper.EMAIL_STATUS, new string[] { Helper.getcolor("Accepted"), "Payment Successfull" });

                    string upath = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/offeraccepted.html");
                    string ucontent = System.IO.File.ReadAllText(upath);

                    ucontent = ucontent.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    ucontent = ucontent.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    ucontent = ucontent.Replace("@address??", Helper.EMAIL_ADDRESS);
                    ucontent = ucontent.Replace("@name??", uname);
                    ucontent = ucontent.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                    ucontent = ucontent.Replace("@intro??", ustatustitle);
                    ucontent = ucontent.Replace("@bookingdetails??", useremailbody.ToString());
                    ucontent = ucontent.Replace("@status??", ue_status);

                    string usubject = "Payment Successfull - " + task;

                    Helper.sendEmail(new string[] { uemail }, usubject, ucontent);


                    //vendor email
                    StringBuilder emailbody = new StringBuilder();

                    string statustitle = string.Format(Helper.EMAIL_PAYMENT_DONE, new string[] { amount, "Successfull", q.QRid, uname });

                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", q.QRid }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", task }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Customer", uname }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Offer Amount", amount }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Date", date }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Time", time }));
                    //emailbody.Append(string.Format(Helper.EMAIL_PAYMENT_RECIEPT, new string[] { q.StripeReceipt }));

                    string e_status = string.Format(Helper.EMAIL_STATUS, new string[] { Helper.getcolor("Accepted"), "Payment Successfull" });

                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/offeraccepted.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", vendorname);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                    content = content.Replace("@intro??", statustitle);
                    content = content.Replace("@bookingdetails??", emailbody.ToString());
                    content = content.Replace("@status??", e_status);

                    string subject = "Payment Successfull - " + task;

                    Helper.sendEmail(new string[] { vendoremail }, subject, content);

                    //true
                    //Thank you! Payment was Successfull
                    ans = jm.Singlevalue("true");
                }
                else
                {
                    Helper.Addtransaction(q.QRid, uid, vid, q.Amount, dt, q.Status, q.StripeId, q.StripeResponse, q.StripeReceipt);

                    //false
                    //payment failed
                    ans = jm.Singlevalue("false");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        //-------------------------------VENDOR--------------------------------------------------

        public string Vendor_Bookings(Vendors a)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string uname = "ISNULL((select CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_Id=q.U_Id),'Unknown')";
                string cat = "ISNULL((select CG_Name from Categories where CG_Id=q.QR_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=q.QR_SubCategory),'Unknown')";
                string status = "(CASE WHEN q.QR_Bookingtype='Direct' THEN ISNULL((select DSB_Status from Direct_ServiceBooking where QR_Id=q.QR_Id),'Unknown') ELSE ISNULL((select QSB_Status from Quote_ServiceBooking where QR_Id=q.QR_Id),'Unknown') END)";
                string paymentstatus = "(select SB_PaymentStatus from vw_quotedetails where QR_Id=q.QR_Id)";

                string price = "(CASE WHEN q.QR_Bookingtype = 'Direct' THEN q.VN_Price ELSE(CASE WHEN(select COUNT(O_Amount) from vw_quoteoffers where QR_Id = q.QR_Id AND O_Status = 'Accepted') > 0 THEN(select TOP 1 O_Amount from vw_quoteoffers where QR_Id = q.QR_Id AND O_Status = 'Accepted') ELSE q.VN_Price END) END)";

                string pricesource = "(CASE WHEN q.QR_Bookingtype = 'Direct' THEN 'Original Price' ELSE (CASE WHEN(select COUNT(O_Amount) from vw_quoteoffers where QR_Id = q.QR_Id AND O_Status = 'Accepted') > 0 THEN 'Negotiated Price' ELSE 'Original Price' END) END)";

                SqlDataAdapter da = new SqlDataAdapter("select q.QR_Id,q.U_Id," + uname + ",q.VN_Id,q.VN_ServiceName,q.VN_Price," + cat + "," + subcat + ",q.QR_Datetime,q.QR_JobDate,q.QR_JobTime," + status + ","+ paymentstatus + ","+price+","+pricesource+" from Quote_Request q where q.V_Id=@vid order by q.QR_Id DESC", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", a.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - qid,uid,uname,vnid,servicename,price,cat,subcat,dt,jdate,jtime,status,paymentstatus,price, pricesource
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no bookings
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;

        }

        public string Vendor_BookingDetails(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string uname = "ISNULL((select CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_Id=q.U_Id),'Unknown')";
                string cat = "ISNULL((select CG_Name from Categories where CG_Id=q.QR_Category),'Unknown')";
                string subcat = "ISNULL((select SCG_Name from SubCategories where SCG_Id=q.QR_SubCategory),'Unknown')";
                string status = "(CASE WHEN q.QR_Bookingtype='Direct' THEN ISNULL((select DSB_Status from Direct_ServiceBooking where QR_Id=q.QR_Id),'Unknown') ELSE ISNULL((select QSB_Status from Quote_ServiceBooking where QR_Id=q.QR_Id),'Unknown') END)";
                string payment = "(select SB_PaymentStatus from vw_quotedetails where QR_Id=q.QR_Id)";

                SqlDataAdapter da = new SqlDataAdapter("select q.QR_Id,q.U_Id," + uname + ",q.VN_Id,q.VN_ServiceName,q.VN_Price," + cat + "," + subcat + ",q.QR_Address,q.QR_Title,q.QR_Description,q.QR_Datetime,q.QR_JobDate,q.QR_JobTime,q.QR_JobPreference," + payment + "," + status + ",q.QR_Jobtype, i.QI_Image1,i.QI_Image2,i.QI_Image3,i.QI_Image4,i.QI_Image5 from Quote_Request q,Quote_Image i where q.QR_Id=@qid AND q.QR_Id=i.QR_Id order by q.QR_Id DESC", con);
                da.SelectCommand.Parameters.AddWithValue("@qid", q.QRid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - qid,uid,uname,vnid,sname,price,catname,subcatname,address,title,description,dt,jobdate,jobtime, jobpreference,paymenttype,status,jobtype,image1,image2,image3,image4,image5
                    //if images are not added, they will return NA
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no booking found
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_UpdateBooking(QuoteRequest q)
        {
            string type = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", q.QRid);
            string uid = Helper.getResult("U_Id", "Quote_Request", "QR_Id", q.QRid);
            string bname = Helper.getResult("V_BusinessName", "Vendor_Register", "V_Id", q.Vid);
            string task = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", q.QRid);
            string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "user_Register", "U_Id", uid);
            string uemail = Helper.getResult("U_EmailID", "user_Register", "U_Id", uid);
            string dt = Helper.getdatetime(3);
            bool canupdate = false;
            string QRstatus = "";

            if (type == "Direct")
            {
                string dsbid = Helper.getResult("DSB_Id", "Direct_ServiceBooking", "QR_Id", q.QRid);
                if (dsbid != "no")
                {
                    QRstatus = Helper.getResult("DSB_Status", "Direct_ServiceBooking", "DSB_Id", dsbid);
                }
            }
            else
            {
                string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", q.QRid);
                if (qsbid != "no")
                {
                    QRstatus = Helper.getResult("QSB_Status", "Quote_ServiceBooking", "QSB_Id", qsbid);
                }
            }

            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                if (type != "no")
                {

                    if((q.Status=="Accepted" || q.Status=="Rejected") && QRstatus=="Pending")
                    {
                        canupdate = true;
                    }
                    else if ((q.Status == "Completed" || q.Status == "Cancelled") && QRstatus == "Accepted")
                    {
                        canupdate = true;
                    }

                    if (canupdate)
                    {

                        if (type == "Direct")
                        {
                            cmd = new SqlCommand("update Direct_ServiceBooking set DSB_Status=@status where QR_Id=@qid", con);
                            cmd.Parameters.AddWithValue("@status", q.Status);
                            cmd.Parameters.AddWithValue("@qid", q.QRid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();

                            Helper.Addnotification("Booking " + q.Status + " - " + task, "Booking " + q.Status + " by " + bname, dt, "DirectRequest", uid, "User", q.QRid);

                            //true
                            ans = jm.Singlevalue("true");
                        }
                        else //Multitple
                        {
                            cmd = new SqlCommand("update Quote_ServiceBooking set QSB_Status=@status where QR_Id=@qid", con);
                            cmd.Parameters.AddWithValue("@status", q.Status);
                            cmd.Parameters.AddWithValue("@qid", q.QRid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();

                            Helper.Addnotification("Booking " + q.Status + " - " + task, "Booking " + q.Status + " by " + bname, dt, "QuoteRequest", uid, "User", q.QRid);

                            //true
                            ans = jm.Singlevalue("true");
                        }

                        StringBuilder emailbody = new StringBuilder();

                        string statustitle = string.Format(Helper.EMAIL_STATUSCHANGE_TITLE, new string[] { q.QRid, bname });

                        emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", q.QRid }));
                        emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", task }));
                        emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Vendor", bname }));

                        string status= string.Format(Helper.EMAIL_STATUS, new string[] { Helper.getcolor(q.Status), q.Status });

                        string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/statuschange.html");
                        string content = System.IO.File.ReadAllText(path);

                        content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                        content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                        content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                        content = content.Replace("@name??", uname);
                        content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                        content = content.Replace("@intro??", statustitle);
                        content = content.Replace("@bookingdetails??", emailbody.ToString());
                        content = content.Replace("@status??", status);

                        string subject = "Service Booking is "+q.Status;

                        Helper.sendEmail(new string[] { uemail }, subject, content);
                    }
                    else
                    {
                        //false
                        //You are not allowed to Update the Request.
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //no booking found
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

    }
}