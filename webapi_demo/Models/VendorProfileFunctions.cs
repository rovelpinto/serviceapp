﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace webapi_demo.Models
{
    public class VendorProfileFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();

        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public string registration(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from Vendor_Register where V_EmailID=@email", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //email already exists
                    return jm.Singlevalue("Email");
                }

                da = new SqlDataAdapter("select * from Vendor_Register where V_MobileNo=@mobile", con);
                da.SelectCommand.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //contact already exists
                    return jm.Singlevalue("Mobile");
                }

                da = new SqlDataAdapter("select * from Vendor_Register where V_GSTINNO=@gst", con);
                da.SelectCommand.Parameters.AddWithValue("@gst", u.GST);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //GSTno already exists
                    return jm.Singlevalue("GST");
                }

                if (ans == "")
                {
                    string url = Helper.getvendorURL()+ u.Photo;

                    cmd = new SqlCommand("insert into Vendor_Register(V_FirstName,V_LastName,V_EmailID,V_Password,V_MobileNo,V_BusinessName,V_GSTINNO,V_City,V_Country,V_AverageRating,V_Profilepicture,V_Aboutus) values (@fname,@lname,@email,@pass,@mobile,@bname,@gst,@city,@country,@rating,@photo,@aboutus)", con);
                    cmd.Parameters.AddWithValue("@fname", u.Fname);
                    cmd.Parameters.AddWithValue("@lname", u.Lname);
                    cmd.Parameters.AddWithValue("@email", u.Email);
                    cmd.Parameters.AddWithValue("@pass", u.Pass);
                    cmd.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                    cmd.Parameters.AddWithValue("@bname", u.Bname);
                    cmd.Parameters.AddWithValue("@gst", u.GST);
                    cmd.Parameters.AddWithValue("@city", u.City);
                    cmd.Parameters.AddWithValue("@country", u.Country);
                    cmd.Parameters.AddWithValue("@rating", "NA");
                    cmd.Parameters.AddWithValue("@photo", url);
                    cmd.Parameters.AddWithValue("@aboutus", u.Aboutus);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    string name = u.Fname + " " + u.Lname;
                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/account_created.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", name);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                    string subject = "Welcome to Task Master! Let’s get started.";

                    Helper.sendEmail(new string[] { u.Email }, subject, content);


                    //account added
                    ans = jm.Singlevalue("true");
                }
            }
            catch(Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Checkregistration(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from Vendor_Register where V_EmailID=@email", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //email already exists
                    return jm.Singlevalue("Email");
                }

                da = new SqlDataAdapter("select * from Vendor_Register where V_MobileNo=@mobile", con);
                da.SelectCommand.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //contact already exists
                    return jm.Singlevalue("Mobile");
                }

                da = new SqlDataAdapter("select * from Vendor_Register where V_GSTINNO=@gst", con);
                da.SelectCommand.Parameters.AddWithValue("@gst", u.GST);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //GSTno already exists
                    return jm.Singlevalue("GST");
                }

                if (ans == "")
                {

                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/registration_otp.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", u.Fname+" "+u.Lname);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);
                    content = content.Replace("@otp??", u.OTP);

                    string subject = "One Step from Account Verification";

                    Helper.sendEmail(new string[] { u.Email }, subject, content);
                    //true
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public class VendorD
        {
            public string Vid { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            public string Email { get; set; }
            public string Pass { get; set; }
            public string Mobile { get; set; }
            public string Bname { get; set; }
            public string GST { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string Photo { get; set; }
            public string Aboutus { get; set; }
            public string OldPass { get; set; }
            public string NewPass { get; set; }
            public string Image1 { get; set; }
            public string Image2 { get; set; }
            public string Image3 { get; set; }
            public string Image4 { get; set; }
            public string Image5 { get; set; }
            public string Token { get; set; }
            public string Flag { get; set; }
            public string OTP { get; set; }
        }

        public class Suburbs
        {
            public string Vid { get; set; }
            public string City { get; set; }
            public string SName { get; set; }
            public JObject Data { get; set; }
        }

        public string login(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select V_Id,V_FirstName,V_LastName,V_Password,V_Profilepicture from Vendor_Register where (V_EmailID=@email OR V_MobileNo=@contact)", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                da.SelectCommand.Parameters.AddWithValue("@contact", "+64" + u.Mobile);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string uid = ds.Tables[0].Rows[0][0].ToString();
                    string name = ds.Tables[0].Rows[0][1].ToString()+" "+ ds.Tables[0].Rows[0][2].ToString();
                    string password = ds.Tables[0].Rows[0][3].ToString();
                    string photo = ds.Tables[0].Rows[0][4].ToString();
                    if (password == u.Pass)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Uid");
                        dt.Columns.Add("Name");
                        dt.Columns.Add("Photo");
                        dt.Rows.Add(uid, name,photo);

                        //status ok
                        //data - uid,fullname,photo
                        ans = jm.Maker1(dt);
                    }
                    else
                    {
                        //wrong password
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //email/mobile doesnt exists
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string loginwithToken(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select V_Id,V_FirstName,V_LastName,V_Password,V_Profilepicture from Vendor_Register where (V_EmailID=@email OR V_MobileNo=@contact)", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                da.SelectCommand.Parameters.AddWithValue("@contact", "+64" + u.Mobile);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string uid = ds.Tables[0].Rows[0][0].ToString();
                    string name = ds.Tables[0].Rows[0][1].ToString() + " " + ds.Tables[0].Rows[0][2].ToString();
                    string password = ds.Tables[0].Rows[0][3].ToString();
                    string photo = ds.Tables[0].Rows[0][4].ToString();
                    if (password == u.Pass)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Uid");
                        dt.Columns.Add("Name");
                        dt.Columns.Add("Photo");
                        dt.Rows.Add(uid, name, photo);


                        UserD ud = new UserD();
                        ud.Uid = uid;
                        ud.Utype = "Vendor";
                        ud.Token = u.Token;
                        updateToken(ud);

                        //status ok
                        //data - uid,fullname,photo
                        ans = jm.Maker1(dt);
                    }
                    else
                    {
                        //wrong password
                        ans = jm.Singlevalue("false");
                    }
                }
                else
                {
                    //email/mobile doesnt exists
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Logout(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("delete from FirebaseTokens where Userid=@vid AND Token=@token AND Usertype='Vendor'", con);
                cmd.Parameters.AddWithValue("@vid", u.Vid);
                cmd.Parameters.AddWithValue("@token", u.Token);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getProfile(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string img1 = "ISNULL((select Image1 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img2 = "ISNULL((select Image2 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img3 = "ISNULL((select Image3 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img4 = "ISNULL((select Image4 from Vendor_Image where V_Id=r.V_Id),'NA')";
                string img5 = "ISNULL((select Image5 from Vendor_Image where V_Id=r.V_Id),'NA')";

                SqlDataAdapter da = new SqlDataAdapter("select r.V_FirstName,r.V_LastName,r.V_EmailID,r.V_MobileNo,r.V_BusinessName,r.V_GSTINNO,r.V_City,r.V_Country,r.V_Profilepicture,r.V_Aboutus,"+img1+ "," + img2 + "," + img3 + "," + img4 + "," + img5 + " from Vendor_Register r where r.V_Id=@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - fname,lname,email,mobile,businessname,gstno,city,country,photo,aboutus,img1,img2,img3,img4,img5
                    return jm.Maker(ds);
                }
                else
                {
                    //user doesnt exists
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string updateprofile(VendorD u)
        {
            string check = Helper.getResult("V_Id", "Vendor_Image", "V_Id", u.Vid);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select * from Vendor_Register where V_EmailID=@email AND V_Id<>@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //email already exists
                    return jm.Singlevalue("Email");
                }

                da = new SqlDataAdapter("select * from Vendor_Register where V_MobileNo=@mobile AND V_Id<>@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Vid);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //contact already exists
                    return jm.Singlevalue("Mobile");
                }

                da = new SqlDataAdapter("select * from Vendor_Register where V_GSTINNO=@gst AND V_Id<>@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@gst", u.GST);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Vid);
                ds = new DataSet();
                da.Fill(ds);
                count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //GSTno already exists
                    return jm.Singlevalue("GST");
                }

                if (ans == "")
                {
                    string url = Helper.getvendorURL() + u.Photo;

                    cmd = new SqlCommand("update Vendor_Register set V_FirstName=@fname,V_LastName=@lname,V_EmailID=@email,V_MobileNo=@mobile,V_BusinessName=@bname,V_GSTINNO=@gst,V_City=@city,V_Country=@country,V_Profilepicture=@photo,V_Aboutus=@aboutus where V_Id=@uid", con);
                    cmd.Parameters.AddWithValue("@fname", u.Fname);
                    cmd.Parameters.AddWithValue("@lname", u.Lname);
                    cmd.Parameters.AddWithValue("@email", u.Email);
                    cmd.Parameters.AddWithValue("@mobile", "+64" + u.Mobile);
                    cmd.Parameters.AddWithValue("@bname", u.Bname);
                    cmd.Parameters.AddWithValue("@gst", u.GST);
                    cmd.Parameters.AddWithValue("@city", u.City);
                    cmd.Parameters.AddWithValue("@country", u.Country);
                    cmd.Parameters.AddWithValue("@photo", url);
                    cmd.Parameters.AddWithValue("@aboutus", u.Aboutus);
                    cmd.Parameters.AddWithValue("@uid", u.Vid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();


                    if(check=="no")
                    {
                        cmd = new SqlCommand("insert into Vendor_Image(V_Id,Image1,Image2,Image3,Image4,Image5) values(@vid,@img1,@img2,@img3,@img4,@img5)", con);
                    }
                    else
                    {
                        cmd = new SqlCommand("update Vendor_Image set Image1=@img1,Image2=@img2,Image3=@img3,Image4=@img4,Image5=@img5 where V_Id=@vid", con);
                    }

                    string url1 = u.Image1 == "NA" ? u.Image1 : Helper.getvendorimagesURL() + u.Image1;
                    string url2 = u.Image2 == "NA" ? u.Image2 : Helper.getvendorimagesURL() + u.Image2;
                    string url3 = u.Image3 == "NA" ? u.Image3 : Helper.getvendorimagesURL() + u.Image3;
                    string url4 = u.Image4 == "NA" ? u.Image4 : Helper.getvendorimagesURL() + u.Image4;
                    string url5 = u.Image5 == "NA" ? u.Image5 : Helper.getvendorimagesURL() + u.Image5;

                    con.Open();
                    cmd.Parameters.AddWithValue("@vid", u.Vid);
                    cmd.Parameters.AddWithValue("@img1", url1);
                    cmd.Parameters.AddWithValue("@img2", url2);
                    cmd.Parameters.AddWithValue("@img3", url3);
                    cmd.Parameters.AddWithValue("@img4", url4);
                    cmd.Parameters.AddWithValue("@img5", url5);
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //updated
                    ans = jm.Singlevalue("true");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string changepassword(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from Vendor_Register where V_Password=@pass AND V_Id=@uid", con);
                da.SelectCommand.Parameters.AddWithValue("@pass", u.OldPass);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    if (u.OldPass == u.NewPass)
                    {
                        //no
                        //Your new password cannot be same as old
                        ans = jm.Singlevalue("no");
                    }
                    else
                    {
                        cmd = new SqlCommand("update Vendor_Register set V_Password=@pass where V_Id=@uid", con);
                        cmd.Parameters.AddWithValue("@pass", u.NewPass);
                        cmd.Parameters.AddWithValue("@uid", u.Vid);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        //true
                        ans = jm.Singlevalue("true");
                    }
                }
                else
                {
                    //old password is not correct
                    ans = jm.Singlevalue("false");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string forgetpassword(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select V_Id,CONCAT(V_FirstName,' ',V_LastName) from Vendor_Register where V_EmailID=@email", con);
                da.SelectCommand.Parameters.AddWithValue("@email", u.Email);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string name = ds.Tables[0].Rows[0][1].ToString();
                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/forgotpass_otp.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", name);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);
                    content = content.Replace("@otp??", u.OTP);

                    string subject = "Password Reset";

                    Helper.sendEmail(new string[] { u.Email }, subject, content);

                    //Vid
                    ans = jm.Maker(ds);
                }
                else
                {
                    //email doesnt exists
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string resetpassword(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("update Vendor_Register set V_Password=@pass where V_Id=@vid", con);
                cmd.Parameters.AddWithValue("@pass", u.Pass);
                cmd.Parameters.AddWithValue("@vid", u.Vid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //Password changed
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getAllSuburbs()
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string[] names = new string[] { "City"};
                string[] values = new string[names.Length];


                SqlDataAdapter da = new SqlDataAdapter("select DISTINCT City from Suburbs", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    ans = "{ \"status\" : \"ok\",";
                    ans += "\"Data\" :[ ";

                    for (int i = 0; i < count; i++)
                    {

                        values = new string[names.Length];
                        string city = ds.Tables[0].Rows[i][0].ToString();
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = city;
                        }


                        SqlDataAdapter da1 = new SqlDataAdapter("select SName from Suburbs where City=@city order by SName", con);
                        da1.SelectCommand.Parameters.AddWithValue("@city", city);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);

                        ans += jm.getArray(names, values, ds1);
                        ans += ",";

                    }

                    ans = ans.Remove(ans.Length - 1);
                    ans += "] }";

                    //status ok
                    //data - Sname
                }
                else
                {
                    //no suburbs
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string AddSuburbs(Suburbs s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                cmd = new SqlCommand("delete from Vendor_Suburb where V_Id=@vid", con);
                cmd.Parameters.AddWithValue("@vid", s.Vid);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();


                JObject data = s.Data;
                IList<JToken> results = data["Suburbs"].Children().ToList();
                IList<Suburbs> searchResults = new List<Suburbs>();
                foreach (JToken result in results)
                {
                    Suburbs searchResult = result.ToObject<Suburbs>();
                    searchResults.Add(searchResult);
                }

                foreach (Suburbs su in searchResults)
                {
                    string sname = su.SName;
                    cmd = new SqlCommand("insert into Vendor_Suburb(V_Id,city,SName) values(@vid,@city,@sname)", con);
                    cmd.Parameters.AddWithValue("@vid", s.Vid);
                    cmd.Parameters.AddWithValue("@city", s.City);
                    cmd.Parameters.AddWithValue("@sname",sname);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                
                //Suburbs added
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getVendorSuburbs(Suburbs s)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string[] names = new string[] { "City" };
                string[] values = new string[names.Length];

                SqlDataAdapter da = null;

                if (s.SName == "All")
                {
                    da = new SqlDataAdapter("select DISTINCT City from Suburbs", con);
                }
                else
                {
                    da = new SqlDataAdapter("select DISTINCT City from Vendor_Suburb where V_Id=@vid", con);
                    da.SelectCommand.Parameters.AddWithValue("@vid", s.Vid);
                }
                    
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    ans = "{ \"status\" : \"ok\",";
                    ans += "\"Data\" :[ ";

                    for (int i = 0; i < count; i++)
                    {

                        values = new string[names.Length];
                        string city = ds.Tables[0].Rows[i][0].ToString();
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = city;
                        }

                        SqlDataAdapter da1 = null;
                        if (s.SName == "All")
                        {
                            da1 = new SqlDataAdapter("select s.SName,ISNULL((select 'Yes' from Vendor_Suburb where SName=s.SName AND City=s.City AND V_Id=@vid),'No') from Suburbs s where City=@city order by SName", con);
                        }
                        else
                        {
                            da1 = new SqlDataAdapter("select SName from Vendor_Suburb where V_Id=@vid AND City=@city order by SName", con);
                        }

                        da1.SelectCommand.Parameters.AddWithValue("@vid", s.Vid);
                        da1.SelectCommand.Parameters.AddWithValue("@city", city);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);
                    
                        ans += jm.getArray(names, values, ds1);
                        ans += ",";

                    }

                    ans = ans.Remove(ans.Length - 1);
                    ans += "] }";

                    //status ok
                    //data - Sname
                }
                else
                {
                    //no suburbs
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public class UserD
        {
            public string Uid { set; get; }
            public string Token { set; get; }
            public string Utype { set; get; }

        }

        public string updateToken(UserD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            string dt = Helper.getdatetime(3);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select Userid,Usertype from FirebaseTokens where Token=@token", con);
                da.SelectCommand.Parameters.AddWithValue("@token", u.Token);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string userid = ds.Tables[0].Rows[0][0].ToString();
                    string usertype = ds.Tables[0].Rows[0][1].ToString();

                    if (userid == u.Uid && usertype == u.Utype)
                    {
                        ans = jm.Singlevalue("true");
                    }
                    else
                    {
                        cmd = new SqlCommand("update FirebaseTokens set Userid=@uid,Usertype=@type,Lastupdated=@dt where Token=@token", con);
                        cmd.Parameters.AddWithValue("@uid", u.Uid);
                        cmd.Parameters.AddWithValue("@type", u.Utype);
                        cmd.Parameters.AddWithValue("@dt", dt);
                        cmd.Parameters.AddWithValue("@token", u.Token);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        //true
                        ans = jm.Singlevalue("true");
                    }
                }
                else
                {
                    cmd = new SqlCommand("insert into FirebaseTokens(Userid,Usertype,Token,Flag,Lastupdated) values(@uid,@type,@token,@flag,@dt)", con);
                    cmd.Parameters.AddWithValue("@uid", u.Uid);
                    cmd.Parameters.AddWithValue("@type", u.Utype);
                    cmd.Parameters.AddWithValue("@dt", dt);
                    cmd.Parameters.AddWithValue("@token", u.Token);
                    cmd.Parameters.AddWithValue("@flag","True");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //true
                    ans = jm.Singlevalue("true");
                }

            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getVendorratings(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string[] names = new string[] { "Ratings" };
                string[] values = new string[names.Length];

                SqlDataAdapter da = new SqlDataAdapter("select * from Rating where V_Id =@vid AND VN_Id = '0' AND R_Status = 'Yes'", con);
                da.SelectCommand.Parameters.AddWithValue("@vid", u.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    da = new SqlDataAdapter("select CAST(AVG(CAST(R_Rating AS Float)) as decimal(2,1)) from Rating where V_Id =@vid AND VN_Id = '0' AND R_Status = 'Yes'", con);
                    da.SelectCommand.Parameters.AddWithValue("@vid", u.Vid);
                    ds = new DataSet();
                    da.Fill(ds);
                    count = ds.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        ans = "{ \"status\" : \"ok\",";
                        ans += "\"Data\" :[ ";

                        values = new string[names.Length];
                        string avgratings = ds.Tables[0].Rows[0][0].ToString();
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = avgratings;
                        }


                        string uphoto = "ISNULL((select U_Profilepicture from User_Register where U_Id=r.U_Id),'Unknown')";
                        string uname = "ISNULL((select CONCAT(U_FirstName,' ',U_LastName) from User_Register where U_Id=r.U_Id),'Unknown')";

                        SqlDataAdapter da1 = new SqlDataAdapter("select r.U_Id," + uname + "," + uphoto + ",r.R_Rating, r.R_Description,r.R_Datetime from Rating r where r.V_Id=@vid AND VN_Id='0' AND r.R_Status='Yes'", con);
                        da1.SelectCommand.Parameters.AddWithValue("@vid", u.Vid);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);

                        //status ok
                        // "Ratings"
                        //data - uid,uname,uphoto,rating,review,datetime
                        ans += jm.getArray(names, values, ds1);
                        ans += "] }";

                    }
                    else
                    {
                        //No ratings or Reviews
                        ans = jm.Singlevalue("no");
                    }
                }
                else
                {
                    //No ratings or Reviews
                    ans = jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string updateNotifications(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            string dt = Helper.getdatetime(3);

            try
            {
                cmd = new SqlCommand("update FirebaseTokens set Flag=@flag,Lastupdated=@dt where Userid=@uid AND Usertype=@utype AND Token=@token", con);
                cmd.Parameters.AddWithValue("@flag", u.Flag);
                cmd.Parameters.AddWithValue("@dt", dt);
                cmd.Parameters.AddWithValue("@uid", u.Vid);
                cmd.Parameters.AddWithValue("@utype", "Vendor");
                cmd.Parameters.AddWithValue("@token", u.Token);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getNotificationStatus(VendorD u)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("select Flag from FirebaseTokens where Userid=@uid AND Usertype=@utype AND Token=@token", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", u.Vid);
                da.SelectCommand.Parameters.AddWithValue("@utype", "Vendor");
                da.SelectCommand.Parameters.AddWithValue("@token", u.Token);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - flag
                    return jm.Maker(ds);
                }
                else
                {
                    //no data found
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string getAllcities()
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select DISTINCT City from Suburbs order by City", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //data - city
                    return jm.Maker(ds);
                }
                else
                {
                    //no city found
                    return jm.Singlevalue("no");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string sendEmail()
        {
            string ans = "";
            try
            {
                ans = Helper.Email("nevon.soft@gmail.com","Rovel Pinto","rp","rp");
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }
    }

}