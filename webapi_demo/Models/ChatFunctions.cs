﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using static webapi_demo.Models.JSONMaker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;

namespace webapi_demo.Models
{
    public class ChatFunctions
    {
        SqlCommand cmd;
        JSONMaker jm = new JSONMaker();


        public string InvalidRequest()
        {
            return jm.Singlevalue("Invalid Request");
        }

        public class QuoteRequest
        {
            public string RequestType { get; set; }
            public string QRid { get; set; }
            public string Uid { get; set; }
            public string Vid { get; set; }
            public string VNCat { get; set; }
            public string VNSubcat { get; set; }
            public string Address { get; set; }
            public string QTitle { get; set; }
            public string QDesc { get; set; }
            public string QDatetime { get; set; }
            public string QJobtype { get; set; }
            public string QJobDate { get; set; }
            public string QJobTime { get; set; }
            public string QJobPreference { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string Img5 { get; set; }
            public JObject Data { get; set; }
            public string Datetime { get; set; }
            public string Status { get; set; }
            public string Message { get; set; }
        }

        public class Vendors
        {
            public string Vid { get; set; }
            public string VNId { get; set; }
            public string VNName { get; set; }
            public string VNPrice { get; set; }
        }

        public class Offer
        {
            public string Oid { get; set; }
            public string QRid { get; set; }
            public string Vid { get; set; }
            public string Amount { get; set; }
            public string Desc { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string Status { get; set; }
            public string Datetime { get; set; }
            public string StripeId { get; set; }
            public string StripeResponse { get; set; }
            public string StripeReceipt { get; set; }
        }

        public class Push
        {
            public string Title { get; set; }
            public string Mesg { get; set; }
            public string Token { get; set; }
            public string SentTo { get; set; }
        }

        //-----------------------------------------USER APP---------------------------------------------

        public string User_Chatlist(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string query = "WITH DEDUPE AS ( SELECT c.Bid, q.VN_ServiceName, q.V_Id, (select CONCAT(V_FirstName, ' ', V_LastName) from Vendor_Register where V_Id = q.V_id) AS vname, (select V_BusinessName from Vendor_Register where V_Id = q.V_id) AS bname, (select V_Profilepicture from Vendor_Register where V_Id = q.V_id) AS photo,(CASE WHEN q.QR_Bookingtype = 'Direct' THEN (select DSB_Status from Direct_ServiceBooking where QR_Id = q.QR_Id) ELSE (select QSB_Status from Quote_ServiceBooking where QR_Id = q.QR_Id) END) AS status, ISNULL((CASE WHEN q.QR_Bookingtype = 'Direct' THEN (select DSB_PaymentType from Direct_ServiceBooking where QR_Id = q.QR_Id) ELSE (select QSB_PaymentType from Quote_ServiceBooking where QR_Id = q.QR_Id) END),'Not Paid') AS paymentype,c.M_Id,ROW_NUMBER() OVER(PARTITION BY c.Bid ORDER BY c.Bid DESC) AS OCCURENCE FROM Chat c, Quote_Request q where c.BId = q.QR_Id  AND((c.Sen_Id = @uid AND c.Sendby = 'User') OR(Rec_Id = @uid AND c.Sendby = 'Vendor'))) SELECT * FROM DEDUPE WHERE OCCURENCE = 1 AND status='Accepted' order by M_Id DESC";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@uid", q.Uid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - bid(requestid),servicename,vid,vname,bname,photo,bookingstatus,paymentstatus
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no recent chats
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_Addchat(QuoteRequest q)
        {
            string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", q.QRid);
            string btype = bookingtype == "Direct" ? "Direct" : "Quote";
            string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "User_Register", "U_Id", q.Uid);
            string sname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", q.QRid);
            string dt = Helper.getdatetime(3);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string res = Helper.AddtoChat(btype, q.QRid, q.Uid, q.Vid, "User", "Message", "NA", q.Message, dt, uname, sname);
                if (res == "true")
                {
                    //true
                    ans = jm.Singlevalue("true");
                }
                else
                {
                    ans = jm.Error(res);
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_viewchat(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string[] names = new string[] {"Id","Name","Photo","Service","Status", "Bookingtype","Date","Time","Paymentstatus","Canpay"};
            string[] values = new string[names.Length];
            string ans = "";
            try
            {
                string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", q.QRid);
                if(bookingtype=="Multiple")
                {
                    string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", q.QRid);
                    string res = checkOffer(qsbid);
                }

                con = Database.getSADB();

                string status = "(CASE WHEN q.QR_Bookingtype = 'Direct' THEN (select DSB_Status from Direct_ServiceBooking where QR_Id = q.QR_Id) ELSE (select QSB_Status from Quote_ServiceBooking where QR_Id = q.QR_Id) END) AS status";
                string paymentstatus = "(select SB_PaymentStatus from vw_quotedetails where QR_Id=q.QR_Id)";
                string canpay = "(select ( CASE WHEN SB_PaymentStatus='Unpaid' AND SB_Status='Accepted' AND QR_Bookingtype='Direct' AND QR_JobDate<>'NA' AND QR_JobTime<>'NA' THEN 'Yes' ELSE 'No' END) from vw_quotedetails where QR_Id=q.QR_Id)";

                SqlDataAdapter da = new SqlDataAdapter("select q.V_Id,CONCAT(u.V_FirstName,' ',u.V_LastName),u.V_Profilepicture,q.VN_ServiceName,"+status+ ",q.QR_Bookingtype,q.QR_JobDate,q.QR_JobTime,"+ paymentstatus + ","+canpay+" from Quote_Request q,Vendor_Register u where q.V_Id=u.V_Id AND q.QR_Id=@qrid", con);
                da.SelectCommand.Parameters.AddWithValue("@qrid", q.QRid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    ans = "{ \"status\" : \"ok\",";
                    ans += "\"Data\" :[ ";

                    for (int i = 0; i < count; i++)
                    {

                        values = new string[names.Length];
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = ds.Tables[0].Rows[0][j].ToString();
                        }

                        //check offers if its needs to expire

                        //string oid = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN CAST(ISNULL((select O_Id from Offer where O_Id=c.M_TypeId),'NA') AS VARCHAR(10)) ELSE 'NA' END) AS OfferID";
                        string oid = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Id from Offer where O_Id=c.M_TypeId),'-1') ELSE '-1' END) AS OfferID";
                        string oamount = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Amount from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS OAmount";
                        string odesc = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Description from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODesc";
                        string oddate = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Deliverydate from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODeliverydate";
                        string odtime = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_DeliveryTime from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODeliverytime";
                        string ostatus = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Status from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS OStatus";
                        string odt = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_DateTime from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODatetime";

                        string query = "select c.Sen_Id,c.Rec_Id,c.Sendby,c.M_Type,c.M_TypeId,c.M_Message,c.M_Datetime," + oid+","+oamount+","+odesc+","+oddate+","+odtime+","+ostatus+","+odt+ " from Chat c where c.Bid=@qrid";

                        SqlDataAdapter da1 = new SqlDataAdapter(query, con);
                        da1.SelectCommand.Parameters.AddWithValue("@qrid", q.QRid);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);
                        int count1 = ds1.Tables[0].Rows.Count;

                        DataTable dt = new DataTable();
                        dt.Columns.Add("sendid");
                        dt.Columns.Add("recid");
                        dt.Columns.Add("sendby");
                        dt.Columns.Add("mtype");
                        dt.Columns.Add("mtypeid");
                        dt.Columns.Add("message");
                        dt.Columns.Add("datetime");
                        dt.Columns.Add("oid");
                        dt.Columns.Add("oamount");
                        dt.Columns.Add("odesc");
                        dt.Columns.Add("odeliverydate");
                        dt.Columns.Add("odeliverytime");
                        dt.Columns.Add("ostatus");
                        dt.Columns.Add("odt");

                        for (int j = 0; j < count1; j++)
                        {
                            string sendid = ds1.Tables[0].Rows[j][0].ToString();
                            string recid = ds1.Tables[0].Rows[j][1].ToString();
                            string sendby = ds1.Tables[0].Rows[j][2].ToString();
                            string mtype = ds1.Tables[0].Rows[j][3].ToString();
                            string mtypeid = ds1.Tables[0].Rows[j][4].ToString();
                            string message = ds1.Tables[0].Rows[j][5].ToString();
                            string datetime = ds1.Tables[0].Rows[j][6].ToString();
                            string ofid = ds1.Tables[0].Rows[j][7].ToString();
                            string ofamount = ds1.Tables[0].Rows[j][8].ToString();
                            string ofdesc = ds1.Tables[0].Rows[j][9].ToString();
                            string ofdeliverydate = ds1.Tables[0].Rows[j][10].ToString();
                            string ofdeliverytime = ds1.Tables[0].Rows[j][11].ToString();
                            string ofstatus = ds1.Tables[0].Rows[j][12].ToString();
                            string ofdt = ds1.Tables[0].Rows[j][13].ToString();

                            if (mtype == "Offer")
                            {
                                if (ofid != "NA" && ofid != "-1")
                                {
                                    dt.Rows.Add(sendid, recid, sendby, mtype, mtypeid, message, datetime, ofid, ofamount, ofdesc, ofdeliverydate, ofdeliverytime, ofstatus, ofdt);
                                }
                            }
                            else
                            {
                                ofid = "NA";
                                dt.Rows.Add(sendid, recid, sendby, mtype, mtypeid, message, datetime, ofid, ofamount, ofdesc, ofdeliverydate, ofdeliverytime, ofstatus, ofdt);
                            }
                        }

                        ans += jm.getArray1(names, values, dt);
                        ans += ",";

                    }

                    //status ok
                    //"Id", "Name", "Photo", "Service", "Status","Bookingtype","Date","Time","Paymentstatus","Canpay"
                    // data - sendid,recid,sendby,mtype,mtypeid,message,datetime,oid,oamount,odesc,odeliverydate,odeliverytime,ostatus,odt
                    //all offer related data will be NA if the mtype is not offer
                    //sendby - User/Vendor
                    //mtype - Message/Offer
                    ans = ans.Remove(ans.Length - 1);
                    ans += "] }";
                }
                else
                {
                    //no recent chats
                    ans = jm.Singlevalue("no");
                }                

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_CheckUpdateOffer(Offer o)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", o.QRid);
                string sname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", o.QRid);
                string qrtitle = Helper.getResult("QR_Title", "Quote_Request", "QR_Id", o.QRid);
                string uid = Helper.getResult("U_Id", "Quote_Request", "QR_Id", o.QRid);
                string vid = Helper.getResult("V_Id", "Quote_Request", "QR_Id", o.QRid);
                string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "User_Register", "U_Id", uid);
                string vendorname = Helper.getResult("V_BusinessName", "Vendor_Register", "V_Id", vid);
                string vendoremail = Helper.getResult("V_EmailID", "Vendor_Register", "V_Id", vid);
                string dt = Helper.getdatetime(3);

                if (bookingtype == "Multiple")
                {
                    string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", o.QRid);
                    string qsb_status = Helper.getResult("QSB_Status", "Quote_ServiceBooking", "QR_Id", o.QRid);
                    string res = checkOffer(qsbid);

                    if (qsb_status != "Accepted")
                    {
                        //false
                        //You cannot update the Offer, the Booking status has been changed.
                        ans = jm.Singlevalue("false");
                        return ans;
                    }
                }

                string status = Helper.getResult("O_Status", "Offer", "O_Id", o.Oid);
                string date = Helper.getResult("O_Deliverydate", "Offer", "O_Id", o.Oid);
                string time = Helper.getResult("O_DeliveryTime", "Offer", "O_Id", o.Oid);
                string amount = Helper.getResult("O_Amount", "Offer", "O_Id", o.Oid);

                if (status == "Pending")
                {

                    if (o.Status == "Accepted")
                    {

                        DataTable dtable = new DataTable();
                        dtable.Columns.Add("link");

                        DataRow drow = dtable.NewRow();

                        string paylink = string.Format(Helper.FULL_PAYMENT_LINK_QUOTE, new string[] { o.QRid, amount, sname, qrtitle, "Quote",o.Oid });

                        dtable.Rows.Add(paylink);

                        //ok
                        //data0 - link
                        ans = jm.Maker1(dtable);
                    }
                    else
                    {
                        con = Database.getSADB();

                        cmd = new SqlCommand("Update Offer set O_Status=@status where O_Id=@oid", con);
                        cmd.Parameters.AddWithValue("@status", o.Status);
                        cmd.Parameters.AddWithValue("@oid", o.Oid);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        //true
                        ans = jm.Singlevalue("true");

                        string title = "Offer is " + o.Status + " for " + sname;
                        string mesg = "Offer is " + o.Status + " by " + uname;

                        Helper.Addnotification(title, mesg, dt, "Offer", vid, "Vendor", o.QRid);
                    }

                }
                else if (status == "Accepted")
                {
                    //already
                    //You have already Accepted the offer
                    ans = jm.Singlevalue("already");
                }
                else if (status == "Expired")
                {
                    //Expired
                    //The Offer has been Expired, You cannot update the status
                    ans = jm.Singlevalue("Expired");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_UpdateOffer(Offer o)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string Stripestatus = Helper.getResult("Status", "[Transactions]", "Stripe_TransId", o.StripeId);
                string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", o.QRid);
                string sname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", o.QRid);
                string uid = Helper.getResult("U_Id", "Quote_Request", "QR_Id", o.QRid);
                string vid = Helper.getResult("V_Id", "Quote_Request", "QR_Id", o.QRid);
                string uname = Helper.getResult("CONCAT(U_FirstName,' ',U_LastName)", "User_Register", "U_Id", uid);
                string uemail = Helper.getResult("U_EmailID", "User_Register", "U_Id", uid);
                string vendorname = Helper.getResult("V_BusinessName", "Vendor_Register", "V_Id", vid);
                string vendoremail = Helper.getResult("V_EmailID", "Vendor_Register", "V_Id", vid);
                string dt = Helper.getdatetime(3);

                string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", o.QRid);
                string qsb_status = Helper.getResult("QSB_Status", "Quote_ServiceBooking", "QR_Id", o.QRid);

                string status = Helper.getResult("O_Status", "Offer", "O_Id", o.Oid);
                string date = Helper.getResult("O_Deliverydate", "Offer", "O_Id", o.Oid);
                string time = Helper.getResult("O_DeliveryTime", "Offer", "O_Id", o.Oid);
                string amount = Helper.getResult("O_Amount", "Offer", "O_Id", o.Oid);

                
                if (o.Status.ToLower() == "succeeded")
                //if (Stripestatus.ToLower() == "paid")
                {
                    con = Database.getSADB();

                    cmd = new SqlCommand("Update Offer set O_Status=@status where O_Id=@oid", con);
                    cmd.Parameters.AddWithValue("@status", "Accepted");
                    cmd.Parameters.AddWithValue("@oid", o.Oid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();


                    cmd = new SqlCommand("Update Quote_Request set QR_JobDate=@date,QR_JobTime=@time where QR_Id=@qid", con);
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@time", time);
                    cmd.Parameters.AddWithValue("@qid", o.QRid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    cmd = new SqlCommand("Update Quote_ServiceBooking set QSB_PaymentStatus=@paystatus where QR_Id=@qid", con);
                    cmd.Parameters.AddWithValue("@paystatus", "Paid");
                    cmd.Parameters.AddWithValue("@qid", o.QRid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //useremail
                    StringBuilder useremailbody = new StringBuilder();

                    string ustatustitle = string.Format(Helper.EMAIL_PAYMENT_DONE_USER, new string[] { amount, "Successfull", o.QRid });

                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", o.QRid }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", sname }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Vendor", vendorname }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Offer Amount", amount }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Date", date }));
                    useremailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Time", time }));
                    //useremailbody.Append(string.Format(Helper.EMAIL_PAYMENT_RECIEPT, new string[] { o.StripeReceipt }));

                    string u_status = string.Format(Helper.EMAIL_STATUS, new string[] { Helper.getcolor("Accepted"), "Offer is Accepted / Payment Successfull" });

                    string upath = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/offeraccepted.html");
                    string ucontent = System.IO.File.ReadAllText(upath);

                    ucontent = ucontent.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    ucontent = ucontent.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    ucontent = ucontent.Replace("@address??", Helper.EMAIL_ADDRESS);
                    ucontent = ucontent.Replace("@name??", uname);
                    ucontent = ucontent.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                    ucontent = ucontent.Replace("@intro??", ustatustitle);
                    ucontent = ucontent.Replace("@bookingdetails??", useremailbody.ToString());
                    ucontent = ucontent.Replace("@status??", u_status);

                    string usubject = "Payment Successfull - " + sname;

                    Helper.sendEmail(new string[] { uemail }, usubject, ucontent);


                    //vendoremail
                    StringBuilder emailbody = new StringBuilder();

                    string statustitle = string.Format(Helper.EMAIL_OFFER_TITLE, new string[] { "Accepted", o.QRid, uname });

                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Request No", o.QRid }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Service Name", sname }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Customer", uname }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Offer Amount", amount }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Date", date }));
                    emailbody.Append(string.Format(Helper.EMAIL_TEXT, new string[] { "Job Time", time }));
                    //emailbody.Append(string.Format(Helper.EMAIL_PAYMENT_RECIEPT, new string[] { o.StripeReceipt }));

                    string e_status = string.Format(Helper.EMAIL_STATUS, new string[] { Helper.getcolor("Accepted"), "Offer is Accepted / Payment Successfull" });

                    string path = HttpContext.Current.Server.MapPath("~/Models/EmailTemplate/offeraccepted.html");
                    string content = System.IO.File.ReadAllText(path);

                    content = content.Replace("@photoclick??", Helper.EMAIL_PHOTOCLICK);
                    content = content.Replace("@photolink??", Helper.EMAIL_PHOTOLINK);
                    content = content.Replace("@address??", Helper.EMAIL_ADDRESS);
                    content = content.Replace("@name??", vendorname);
                    content = content.Replace("@signature??", Helper.EMAIL_SIGNATURE);

                    content = content.Replace("@intro??", statustitle);
                    content = content.Replace("@bookingdetails??", emailbody.ToString());
                    content = content.Replace("@status??", e_status);

                    string subject = "Offer is Accepted / Payment Successfull - " + sname;

                    Helper.sendEmail(new string[] { vendoremail }, subject, content);

                    //true
                    ans = jm.Singlevalue("true");

                    string title = "Offer is Accepted for " + sname;
                    string mesg = "Offer is Accepted by " + uname;

                    Helper.Addnotification(title, mesg, dt, "Offer", vid, "Vendor", o.QRid);
                    Helper.Addnotification("Payment Complete - " + sname, "Your Payment is Recieved", dt, "Offer", uid, "User", o.QRid);

                    Helper.Addtransaction(o.QRid, uid, vid, o.Amount, dt, o.Status, o.StripeId, o.StripeResponse, o.StripeReceipt);
                }
                else
                {
                    Helper.Addtransaction(o.QRid, uid, vid, o.Amount, dt, o.Status, o.StripeId, o.StripeResponse, o.StripeReceipt);

                    //false
                    ans = jm.Singlevalue("false");
                }
            }
            catch (Exception e)
            {
                con.Close();
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_Notifications(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select N_Title,N_Message,N_Type,N_Datetime from [Notification] where N_Utype=@utype AND N_Uid=@uid AND N_Visible=@visible order by N_Id DESC", con);
                da.SelectCommand.Parameters.AddWithValue("@uid",q.Uid);
                da.SelectCommand.Parameters.AddWithValue("@utype","User");
                da.SelectCommand.Parameters.AddWithValue("@visible","Yes");
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - title,message,type,datetime
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no notifications
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string User_ClearNotifications(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("update [Notification] set N_Visible=@visible where N_Uid=@uid AND N_Utype=@utype", con);
                cmd.Parameters.AddWithValue("@uid", q.Uid);
                cmd.Parameters.AddWithValue("@utype", "User");
                cmd.Parameters.AddWithValue("@visible", "No");
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        //-------------------------------VENDOR--------------------------------------------------

        public string Vendor_Chatlist(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                string query = "WITH DEDUPE AS ( SELECT c.Bid, q.VN_ServiceName, q.U_Id, (select CONCAT(U_FirstName, ' ', U_LastName) from User_Register where U_Id = q.U_id) AS uname, (select U_Profilepicture from User_Register where U_Id = q.U_Id) AS photo,(CASE WHEN q.QR_Bookingtype = 'Direct' THEN(select DSB_Status from Direct_ServiceBooking where QR_Id = q.QR_Id) ELSE(select QSB_Status from Quote_ServiceBooking where QR_Id = q.QR_Id) END) AS status, ISNULL((CASE WHEN q.QR_Bookingtype = 'Direct' THEN(select DSB_PaymentType from Direct_ServiceBooking where QR_Id = q.QR_Id) ELSE(select QSB_PaymentType from Quote_ServiceBooking where QR_Id = q.QR_Id) END),'Not Paid') AS paymentype, c.M_Id, ROW_NUMBER() OVER(PARTITION BY c.Bid ORDER BY c.Bid DESC) AS OCCURENCE FROM Chat c, Quote_Request q where c.BId = q.QR_Id AND((c.Sen_Id = @vid AND c.Sendby = 'Vendor') OR(Rec_Id = @vid AND c.Sendby = 'User'))) SELECT * FROM DEDUPE WHERE OCCURENCE = 1 AND status='Accepted' order by M_Id DESC";
                
                SqlDataAdapter da = new SqlDataAdapter(query, con);
                da.SelectCommand.Parameters.AddWithValue("@vid", q.Vid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - bid(requestid),servicename,uid,uname,photo,bookingstatus,paymentstatus
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no recent chats
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_Addchat(QuoteRequest q)
        {
            string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", q.QRid);
            string btype = bookingtype == "Direct" ? "Direct" : "Quote";
            string vname = Helper.getResult("CONCAT(V_FirstName,' ',V_LastName)", "Vendor_Register", "V_Id", q.Vid);
            string sname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", q.QRid);
            string dt = Helper.getdatetime(3);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                string res=Helper.AddtoChat(btype,q.QRid,q.Vid,q.Uid,"Vendor", "Message", "NA",q.Message, dt, vname,sname);
                if(res=="true")
                {
                    //true
                    ans = jm.Singlevalue("true");
                }
                else
                {
                    ans = jm.Error(res);
                }
            }
            catch(Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_viewchat(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string[] names = new string[] { "Id", "Name", "Photo", "Service", "Status","Bookingtype", "Date", "Time", "Paymentstatus", "Canpay" };
            string[] values = new string[names.Length];
            string ans = "";
            try
            {

                string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", q.QRid);
                if (bookingtype == "Multiple")
                {
                    string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", q.QRid);
                    string res = checkOffer(qsbid);
                }

                con = Database.getSADB();

                string status = "(CASE WHEN q.QR_Bookingtype = 'Direct' THEN (select DSB_Status from Direct_ServiceBooking where QR_Id = q.QR_Id) ELSE (select QSB_Status from Quote_ServiceBooking where QR_Id = q.QR_Id) END) AS status";
                string paymentstatus = "(select SB_PaymentStatus from vw_quotedetails where QR_Id=q.QR_Id)";

                SqlDataAdapter da = new SqlDataAdapter("select q.U_Id,CONCAT(u.U_FirstName,' ',u.U_LastName),u.U_Profilepicture,q.VN_ServiceName," + status + ",q.QR_Bookingtype,q.QR_JobDate,q.QR_JobTime,"+ paymentstatus + ",'No' from Quote_Request q,User_Register u where q.U_Id=u.U_Id AND q.QR_Id=@qrid", con);
                da.SelectCommand.Parameters.AddWithValue("@qrid", q.QRid);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    ans = "{ \"status\" : \"ok\",";
                    ans += "\"Data\" :[ ";

                    for (int i = 0; i < count; i++)
                    {

                        values = new string[names.Length];
                        for (int j = 0; j < names.Length; j++)
                        {
                            values[j] = ds.Tables[0].Rows[0][j].ToString();
                        }

                        string oid = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Id from Offer where O_Id=c.M_TypeId),'-1') ELSE '-1' END) AS OfferID";
                        string oamount = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Amount from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS OAmount";
                        string odesc = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Description from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODesc";
                        string oddate = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Deliverydate from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODeliverydate";
                        string odtime = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_DeliveryTime from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODeliverytime";
                        string ostatus = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_Status from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS OStatus";
                        string odt = "(CASE WHEN c.M_Type='Offer' AND c.M_TypeId<>'NA' THEN ISNULL((select O_DateTime from Offer where O_Id=c.M_TypeId),'NA') ELSE 'NA' END) AS ODatetime";

                        string query = "select c.Sen_Id,c.Rec_Id,c.Sendby,c.M_Type,c.M_TypeId,c.M_Message,c.M_Datetime," + oid + "," + oamount + "," + odesc + "," + oddate + "," + odtime + "," + ostatus + "," + odt + " from Chat c where c.Bid=@qrid ";

                        SqlDataAdapter da1 = new SqlDataAdapter(query, con);
                        da1.SelectCommand.Parameters.AddWithValue("@qrid", q.QRid);
                        DataSet ds1 = new DataSet();
                        da1.Fill(ds1);
                        int count1 = ds1.Tables[0].Rows.Count;

                        DataTable dt = new DataTable();
                        dt.Columns.Add("sendid");
                        dt.Columns.Add("recid");
                        dt.Columns.Add("sendby");
                        dt.Columns.Add("mtype");
                        dt.Columns.Add("mtypeid");
                        dt.Columns.Add("message");
                        dt.Columns.Add("datetime");
                        dt.Columns.Add("oid");
                        dt.Columns.Add("oamount");
                        dt.Columns.Add("odesc");
                        dt.Columns.Add("odeliverydate");
                        dt.Columns.Add("odeliverytime");
                        dt.Columns.Add("ostatus");
                        dt.Columns.Add("odt");

                        for(int j=0;j< count1;j++)
                        {
                            string sendid = ds1.Tables[0].Rows[j][0].ToString();
                            string recid = ds1.Tables[0].Rows[j][1].ToString();
                            string sendby = ds1.Tables[0].Rows[j][2].ToString();
                            string mtype = ds1.Tables[0].Rows[j][3].ToString();
                            string mtypeid = ds1.Tables[0].Rows[j][4].ToString();
                            string message = ds1.Tables[0].Rows[j][5].ToString();
                            string datetime = ds1.Tables[0].Rows[j][6].ToString();
                            string ofid = ds1.Tables[0].Rows[j][7].ToString();
                            string ofamount = ds1.Tables[0].Rows[j][8].ToString();
                            string ofdesc = ds1.Tables[0].Rows[j][9].ToString();
                            string ofdeliverydate = ds1.Tables[0].Rows[j][10].ToString();
                            string ofdeliverytime = ds1.Tables[0].Rows[j][11].ToString();
                            string ofstatus = ds1.Tables[0].Rows[j][12].ToString();
                            string ofdt = ds1.Tables[0].Rows[j][13].ToString();

                            if (mtype == "Offer")
                            {
                                if (ofid != "NA" && ofid!="-1")
                                {
                                    dt.Rows.Add(sendid, recid, sendby, mtype, mtypeid, message, datetime, ofid, ofamount, ofdesc, ofdeliverydate, ofdeliverytime, ofstatus, ofdt);
                                }
                            }
                            else
                            {
                                ofid = "NA";
                                dt.Rows.Add(sendid, recid, sendby, mtype, mtypeid, message, datetime, ofid, ofamount, ofdesc, ofdeliverydate, ofdeliverytime, ofstatus, ofdt);
                            }
                        }

                        ans += jm.getArray1(names, values, dt);
                        ans += ",";

                    }

                    //status ok
                    //"Id", "Name", "Photo", "Service", "Status","Bookingtype""Date", "Time", "Paymentstatus", "Canpay"
                    // data - sendid,recid,sendby,mtype,mtypeid,message,datetime,oid,oamount,odesc,odeliverydate,odeliverytime,ostatus,odt
                    //all offer related data will be NA if the mtype is not offer
                    //sendby - User/Vendor
                    //mtype - Message/Offer
                    //vendor cannot pay so Canpay will always be No
                    ans = ans.Remove(ans.Length - 1);
                    ans += "] }";
                }
                else
                {
                    //no recent chats
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_Addoffer(Offer o)
        {
            string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", o.QRid);
            string qsbid = Helper.getResult("QSB_Id", "Quote_ServiceBooking", "QR_Id", o.QRid);
            string QRstatus = "";
            if (qsbid!="no")
            {
                QRstatus = Helper.getResult("QSB_Status", "Quote_ServiceBooking", "QR_Id", o.QRid);
            }
            string btype = bookingtype == "Direct" ? "Direct" : "Quote";
            string vname = Helper.getResult("CONCAT(V_FirstName,' ',V_LastName)", "Vendor_Register", "V_Id", o.Vid);
            string sname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", o.QRid);
            string uid = Helper.getResult("U_Id", "Quote_Request", "QR_Id", o.QRid);
            string dt = Helper.getdatetime(3);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                if (bookingtype == "Multiple" && qsbid != "no" && QRstatus=="Accepted")
                {
                    
                    //check if any accepted
                    List<Offers> accepted = getOfferslist(qsbid, "Accepted");
                    if(accepted.Count>0)
                    {
                        //Accepted
                        //Your Previous Offer has been Accepted by the User, you cannot add a New Offer
                        ans = jm.Singlevalue("Accepted");
                    }
                    
                    if(ans.Length==0)
                    {
                        //check if pending

                        string res = checkOffer(qsbid);

                        List<Offers> pending = getOfferslist(qsbid, "Pending");
                        if (pending.Count > 0)
                        {
                            //Pending
                            //Your Previous Offer has yet to be replied by the User, you cannot add a New Offer
                            ans = jm.Singlevalue("Pending");
                        }
                    }

                    //add new offer
                    if (ans.Length == 0)
                    {

                        //check if offer datetime is greater then now
                        DateTime odt = DateTime.ParseExact(o.Date+" "+o.Time, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                        DateTime nzdt = DateTime.ParseExact(dt, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                        double diff = Helper.getmin(odt, nzdt);
                        if(diff<=0)
                        {
                            //Datetime
                            //The Date & time slot for the Service offered should be greater then current time
                            ans = jm.Singlevalue("Datetime");
                        }
                        else
                        {

                            DateTime DT_os = DateTime.ParseExact(o.Date+" "+o.Time, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                            DateTime DT_o = DateTime.ParseExact(dt, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                            DateTime DT_48 = DT_o.AddHours(48);

                            string Expirydt = DT_48.ToString("ddd dd MMM yy, HH:mm");
                            if (DT_os < DT_48)
                            {
                                Expirydt = DT_os.ToString("ddd dd MMM yy, HH:mm");
                            }

                            string finaldescription = o.Desc + "\n( Valid till " + Expirydt+" )";

                            cmd = new SqlCommand("insert into Offer(QSB_ID,V_ID,O_Amount,O_Description,O_Deliverydate,O_DeliveryTime,O_Status,O_Datetime) values(@qsbid,@vid,@amt,@desc,@date,@time,@status,@dt)", con);
                            cmd.Parameters.AddWithValue("@qsbid", qsbid);
                            cmd.Parameters.AddWithValue("@vid", o.Vid);
                            cmd.Parameters.AddWithValue("@amt", o.Amount);
                            cmd.Parameters.AddWithValue("@desc", finaldescription);
                            cmd.Parameters.AddWithValue("@date", o.Date);
                            cmd.Parameters.AddWithValue("@time", o.Time);
                            cmd.Parameters.AddWithValue("@status", "Pending");
                            cmd.Parameters.AddWithValue("@dt", dt);
                            con.Open();
                            cmd.ExecuteNonQuery();

                            SqlDataAdapter da2 = new SqlDataAdapter("select Top 1 @@IDENTITY from Offer order by O_Id DESC", con);
                            DataSet ds2 = new DataSet();
                            da2.Fill(ds2);
                            string oid = ds2.Tables[0].Rows[0][0].ToString();
                            con.Close();

                            string res = Helper.AddtoChat(btype, o.QRid, o.Vid, uid, "Vendor", "Offer", oid, "NA", dt, vname, sname);

                            //true
                            ans = jm.Singlevalue("true");
                        }
                    }

                }
                else
                {
                    //false
                    //This Booking doesnt support Add Offers
                    ans = jm.Singlevalue("false");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_ChangeDateTime(Offer o)
        {
            string bookingtype = Helper.getResult("QR_Bookingtype", "Quote_Request", "QR_Id", o.QRid);
            string uid = Helper.getResult("U_Id", "Quote_Request", "QR_Id", o.QRid);
            string btype = bookingtype == "Direct" ? "Direct" : "Quote";
            string vname = Helper.getResult("CONCAT(V_FirstName,' ',V_LastName)", "Vendor_Register", "V_Id", o.Vid);
            string sname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", o.QRid);
            string dsbid = Helper.getResult("DSB_Id", "Direct_ServiceBooking", "QR_Id", o.QRid);
            string QRstatus = "";
            if (dsbid != "no")
            {
                QRstatus = Helper.getResult("DSB_Status", "Direct_ServiceBooking", "QR_Id", o.QRid);
            }

            string dt = Helper.getdatetime(3);
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {

                if(btype== "Direct" && QRstatus=="Accepted")
                {

                    DateTime odt = DateTime.ParseExact(o.Date + " " + o.Time, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                    DateTime nzdt = DateTime.ParseExact(dt, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                    double diff = Helper.getmin(odt, nzdt);
                    if (diff <= 0)
                    {
                        //Datetime
                        //The Date & time slot for the Service offered should be greater then current time
                        ans = jm.Singlevalue("Datetime");
                    }
                    else
                    {
                        cmd = new SqlCommand("Update Quote_Request set QR_JobDate=@date,QR_JobTime=@time where QR_Id=@qid", con);
                        cmd.Parameters.AddWithValue("@date", o.Date);
                        cmd.Parameters.AddWithValue("@time", o.Time);
                        cmd.Parameters.AddWithValue("@qid", o.QRid);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        Helper.Addnotification(sname + " scheduled at " + o.Date + " " + o.Time, "Service is scheduled by " + vname, dt, "DirectRequest", uid, "User", o.QRid);

                        //true
                        ans = jm.Singlevalue("true");
                    }
                }
                else
                {
                    //false
                    //Changing Date & Time is not allowed for this order
                    ans = jm.Singlevalue("false");
                }
            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_Notifications(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select N_Title,N_Message,N_Type,N_Datetime from [Notification] where N_Utype=@utype AND N_Uid=@uid AND N_Visible=@visible order by N_Id DESC", con);
                da.SelectCommand.Parameters.AddWithValue("@uid", q.Vid);
                da.SelectCommand.Parameters.AddWithValue("@utype", "Vendor");
                da.SelectCommand.Parameters.AddWithValue("@visible", "Yes");
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    //status ok
                    //Data - title,message,type,datetime
                    ans = jm.Maker(ds);
                }
                else
                {
                    //no notifications
                    ans = jm.Singlevalue("no");
                }

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public string Vendor_ClearNotifications(QuoteRequest q)
        {
            SqlConnection con = Database.getSADB();
            string ans = "";
            try
            {
                cmd = new SqlCommand("update [Notification] set N_Visible=@visible where N_Uid=@uid AND N_Utype=@utype", con);
                cmd.Parameters.AddWithValue("@uid", q.Vid);
                cmd.Parameters.AddWithValue("@utype", "Vendor");
                cmd.Parameters.AddWithValue("@visible", "No");
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                //true
                ans = jm.Singlevalue("true");

            }
            catch (Exception e)
            {
                ans = jm.Error(e.Message);
            }
            return ans;
        }

        public List<Offers> getOfferslist(string qsbid,string status)
        {
            List<Offers> o = new List<Offers>();
            SqlConnection con = Database.getSADB();
            SqlDataAdapter da = new SqlDataAdapter("select * from Offer where QSB_ID=@qsbid AND O_Status=@status order by O_Id", con);
            da.SelectCommand.Parameters.AddWithValue("@qsbid", qsbid);
            da.SelectCommand.Parameters.AddWithValue("@status", status);
            DataSet ds = new DataSet();
            da.Fill(ds);
            int count = ds.Tables[0].Rows.Count;
            if(count>0)
            {
                for(int i=0;i<count;i++)
                {
                    string id = ds.Tables[0].Rows[i][0].ToString();
                    string qsid = ds.Tables[0].Rows[i][1].ToString();
                    string vid = ds.Tables[0].Rows[i][2].ToString();
                    string amount = ds.Tables[0].Rows[i][3].ToString();
                    string desc = ds.Tables[0].Rows[i][4].ToString();
                    string date = ds.Tables[0].Rows[i][5].ToString();
                    string time = ds.Tables[0].Rows[i][6].ToString();
                    string sta = ds.Tables[0].Rows[i][7].ToString();
                    string dt = ds.Tables[0].Rows[i][8].ToString();

                    Offers of = new Offers();
                    of.Oid = id;
                    of.Qsbid = qsid;
                    of.Vid = vid;
                    of.Amount = amount;
                    of.Desc = desc;
                    of.Date = date;
                    of.Time = time;
                    of.Status = sta;
                    of.Datetime = dt;
                    o.Add(of);
                }
            }

            return o;
        }

        public string checkOffer(string qsbid)
        {
            string ans = "";
            try
            {
                List<Offers> pending = getOfferslist(qsbid, "Pending");
                if (pending.Count > 0)
                {
                    for (int i = 0; i < pending.Count; i++)
                    {
                        SqlConnection con = Database.getSADB();

                        Offers of = pending[i];
                        string dt = of.Datetime;
                        string d = of.Date;
                        string t = of.Time;


                        DateTime offer_addedDT = DateTime.ParseExact(dt, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                        DateTime offer_deliveryDT = DateTime.ParseExact(d + " " + t, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                        DateTime NZdate = DateTime.ParseExact(Helper.getdatetime(3), "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                        DateTime Finaldate = offer_addedDT;


                        double diff = Helper.getmin(NZdate, offer_addedDT);
                        bool Toupdate = false;

                        if (diff >= 48)
                        {
                            Toupdate = true;
                        }
                        else
                        {
                            double diff1 = Helper.getmin(offer_deliveryDT, NZdate);
                            if (diff1 < 0)
                            {
                                Toupdate = true;
                            }
                        }

                        if(Toupdate)
                        {
                            cmd = new SqlCommand("update Offer set O_Status=@status where O_Id=@oid", con);
                            cmd.Parameters.AddWithValue("@status", "Expired");
                            cmd.Parameters.AddWithValue("@oid", of.Oid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();

                            string qrid = Helper.getResult("QR_Id", "Quote_ServiceBooking", "QSB_Id", qsbid);
                            string taskname = Helper.getResult("VN_ServiceName", "Quote_Request", "QR_Id", qrid);
                            string vid = Helper.getResult("V_Id", "Quote_Request", "QR_Id", qrid);
                            string vname = Helper.getResult("CONCAT(V_FirstName,' ',V_LastName)", "Vendor_Register", "V_Id", vid);
                            string uid = Helper.getResult("U_Id", "Quote_Request", "QR_Id", qrid);
                            string curdt = Helper.getdatetime(3);

                            string title = "Your Offer is Expired!";
                            string message = "Offer for " + taskname + " offered by " + vname;

                            Helper.Addnotification(title, message, curdt, "Offer", uid, "User", qrid);
                        }
                    }
                }

                ans = "true";
            }
            catch(Exception e)
            {
                ans = e.Message;
            }

            return ans;
        }

        public class Offers
        {
            public string Oid { get; set;}
            public string Qsbid { get; set; }
            public string Vid { get; set; }
            public string Amount { get; set; }
            public string Desc { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string Status { get; set; }
            public string Datetime { get; set; }
        }

        public string PushNotification(Push p)
        {
            string ans = "";
            try
            {
                string applicationID = "";
                string senderId = "";

                if (p.SentTo == "User")
                {
                    applicationID = "AAAA_dUmZBk:APA91bEtlRIDDVvTdGh5TusliUhxl-TXaUKQxfGLOXunjD5aZ0G_9zvzszKWUm5TmvTQrv9x3xWjEEq1-yxPmLcmVqBMFky3-FQVcYkGT5C_xLCnglp7xGk72ffM9Y2_glTJLzrenfCF";
                    senderId = "1090202788889";
                }
                else
                {
                    applicationID = "AAAA61zxF4E:APA91bHDKYaI3akf1kQXtmayVno9uEmL2U3EZXk-5V3AoYeCBJ3dwcVbJRUXP8JALeTdbtqZMkOtPrkEv9XhbA3bx-UOte5d8QA4e2gGAHAATEAeX4aWRDc00Rh_93DylowKtujB8oPu";
                    senderId = "1010876618625";
                }

                string deviceId = p.Token;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";

                var data = new

                {
                    collapseKey = Helper.getRandomAlpha(),

                    to = p.Token,

                    notification = new

                    {

                        body = p.Mesg,

                        title = p.Title,

                        icon = "myicon"

                    },
                    data = new
                    {
                        key1 = "value1",
                        key2 = "value2"
                    }
                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                ans = sResponseFromServer;
                                //{"status": "{"multicast_id":5015833947968800616,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1605869368217740%bb933c01bb933c01"}]}"}

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                ans = ex.Message;
            }

            return ans;

        }

    }
}