﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.ChatFunctions;

namespace webapi_demo.Controllers
{
    public class ChatController : ApiController
    {
        Models.ChatFunctions f = new Models.ChatFunctions();

        //URL LINK http://servicenz.azurewebsites.net/

        //status Offers
        // Pending, Accepted, Rejected


        //{
        //    "Uid":"1002"
        //}
        [HttpPost]
        [Route("api/Chat/User_Chatlist")]
        public HttpResponseMessage User_Chatlist([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_Chatlist(q));
        }

        //{
        //    "QRid":"15",
        //    "Uid":"1002",
        //    "Vid":"1001",
        //    "Message":"Hi i need my service by today itself, i you can make it possible",
        //    "Datetime":"2021-06-08 19:29" (taken from backend)
        //}
        [HttpPost]
        [Route("api/Chat/User_Addchat")]
        public HttpResponseMessage User_Addchat([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_Addchat(q));
        }


        //{
        //    "QRid":"10"
        //}

        [HttpPost]
        [Route("api/Chat/User_viewchat")]
        public HttpResponseMessage User_viewchat([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_viewchat(q));
        }


        //{
        //    "QRid":"10",
        //    "Oid":"10",
        //    "Status":"Accepted/Rejected"
        //    "Datetime":"2021-06-08 19:29", (taken from backend)
        //}

        [HttpPost]
        [Route("api/Chat/User_CheckUpdateOffer")]
        public HttpResponseMessage User_CheckUpdateOffer([FromBody] Offer o)
        {
            return Models.Helper.getResponse(f.User_CheckUpdateOffer(o));
        }


        //{
        //    "QRid":"10",
        //    "Oid":"10",
        //    "Amount":"2000",
        //    "Status":"Succeeded",
        //    "StripeId":"kjskakbjds89900990",
        //    "StripeResponse":"jsonresponse",
        //    "StripeReceipt":"receipturl"
        //}

        [HttpPost]
        [Route("api/Chat/User_UpdateOffer")]
        public HttpResponseMessage User_UpdateOffer([FromBody] Offer o)
        {
            return Models.Helper.getResponse(f.User_UpdateOffer(o));
        }

        //{
        //    "Uid":"1002"
        //}
        [HttpPost]
        [Route("api/Chat/User_Notifications")]
        public HttpResponseMessage User_Notifications([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_Notifications(q));
        }


        //{
        //    "Uid":"1002"
        //}
        [HttpPost]
        [Route("api/Chat/User_ClearNotifications")]
        public HttpResponseMessage User_ClearNotifications([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_ClearNotifications(q));
        }

        //--------------------------------VENDOR----------------------------------------------


        //{
        //    "Vid":"1001"
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_Chatlist")]
        public HttpResponseMessage Vendor_Chatlist([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_Chatlist(q));
        }

        //{
        //    "QRid":"15",
        //    "Uid":"1002",
        //    "Vid":"1001",
        //    "Message":"Hi i need my service by today itself, i you can make it possible",
        //    "Datetime":"2021-06-08 19:29", (taken from backend)
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_Addchat")]
        public HttpResponseMessage Vendor_Addchat([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_Addchat(q));
        }


        //{
        //    "QRid":"10"
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_viewchat")]
        public HttpResponseMessage Vendor_viewchat([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_viewchat(q));
        }


        //{
        //    "QRid":"10",
        //    "Vid":"1001",
        //    "Amount":"500",
        //    "Desc":"NA",
        //    "Date":"2021-06-10",
        //    "Time":"12:51"
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_Addoffer")]
        public HttpResponseMessage Vendor_Addoffer([FromBody] Offer o)
        {
            return Models.Helper.getResponse(f.Vendor_Addoffer(o));
        }


        //{
        //    "QRid":"10",
        //    "Vid":"1001",
        //    "Date":"2021-06-10",
        //    "Time":"12:51"
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_ChangeDateTime")]
        public HttpResponseMessage Vendor_ChangeDateTime([FromBody] Offer o)
        {
            return Models.Helper.getResponse(f.Vendor_ChangeDateTime(o));
        }

        //{
        //    "Vid":"1001"
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_Notifications")]
        public HttpResponseMessage Vendor_Notifications([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_Notifications(q));
        }


        //{
        //    "Vid":"1001"
        //}
        [HttpPost]
        [Route("api/Chat/Vendor_ClearNotifications")]
        public HttpResponseMessage Vendor_ClearNotifications([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_ClearNotifications(q));
        }

        //{
        //    "Title":"1001",
        //    "Mesg":"1001",
        //    "Token":"1001",
        //    "SentTo":"User/vendor",
        //}
        [HttpPost]
        [Route("api/Chat/PushNotification")]
        public HttpResponseMessage PushNotification([FromBody] Push q)
        {
            return Models.Helper.getResponse(f.PushNotification(q));
        }
    }
}
