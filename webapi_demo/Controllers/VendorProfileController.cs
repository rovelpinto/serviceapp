﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.VendorProfileFunctions;

namespace webapi_demo.Controllers
{
    public class VendorProfileController : ApiController
    {
        Models.VendorProfileFunctions f = new Models.VendorProfileFunctions();

        //URL LINK http://servicenz.azurewebsites.net/

        //{
        //    "Fname": "Rovel",
        //    "Lname": "Pinto",
        //    "Email": "rp@rp.com",
        //    "Pass": "rp12345",
        //    "Mobile": "9876543210",
        //    "Bname": "Nevon Enterprises Pvt Ltd",
        //    "GST": "12345ASFGGG44",
        //    "City": "Mumbai",
        //    "Country": "India",
        //    "Photo": "name.jpg",
        //    "Aboutus": "We are dealing in ACs and Specialized and Expereinced since 2000"
        //}

        [HttpPost]
        [Route("api/VendorProfile/registration")]
        public HttpResponseMessage registration([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.registration(u));
        }


        //{
        //    "Fname": "Rovel",
        //    "Lname": "Pinto",
        //    "Email": "rp@rp.com",
        //    "Pass": "rp12345",
        //    "Mobile": "9876543210",
        //    "Bname": "Nevon Enterprises Pvt Ltd",
        //    "GST": "12345ASFGGG44",
        //    "City": "Mumbai",
        //    "Country": "India",
        //    "Photo": "name.jpg",
        //    "Aboutus": "We are dealing in ACs and Specialized and Expereinced since 2000"
        //    "OTP": "1234"
        //}

        [HttpPost]
        [Route("api/VendorProfile/Checkregistration")]
        public HttpResponseMessage Checkregistration([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.Checkregistration(u));
        }


        //{
        //  "Email": "rp@rp.com",
        //  "Mobile": "NA",
        //  "Pass": "rp12345"
        //}
        //or visa versa
        [HttpPost]
        [Route("api/VendorProfile/login")]
        public HttpResponseMessage login([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.login(u));
        }

        //{
        //  "Email": "rp@rp.com",
        //  "Mobile": "NA",
        //  "Pass": "rp12345",
        //  "Token": "dhtckl5467890"
        //}
        [HttpPost]
        [Route("api/VendorProfile/loginwithToken")]
        public HttpResponseMessage loginwithToken([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.loginwithToken(u));
        }

        //{
        //  "Vid": "1001"
        //  "Token": "dhtckl5467890"
        //}
        [HttpPost]
        [Route("api/VendorProfile/Logout")]
        public HttpResponseMessage Logout([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.Logout(u));
        }


        //{
        //  "Vid": "1001",
        //}
        [HttpPost]
        [Route("api/VendorProfile/getProfile")]
        public HttpResponseMessage getProfile([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.getProfile(u));
        }

        //{
        //    "Vid": "1001",
        //    "Fname": "Rovel",
        //    "Lname": "Pinto",
        //    "Email": "rp@rp.com",
        //    "Mobile": "9876543210",
        //    "Bname": "Nevon Enterprises Pvt Ltd",
        //    "GST": "12345ASFGGG44",
        //    "City": "Mumbai",
        //    "Country": "India",
        //    "Photo": "name.jpg",
        //    "Aboutus": "We are dealing in ACs and Specialized and Expereinced since 2000",
        //    "Image1":"Vid_timestamp_image1.jpg",
        //    "Image2":"Vid_timestamp_image2.jpg",
        //    "Image3":"NA",
        //    "Image4":"NA",
        //    "Image5":"NA"
        //}

        [HttpPost]
        [Route("api/VendorProfile/updateprofile")]
        public HttpResponseMessage updateprofile([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.updateprofile(u));
        }

        //{
        //    "Vid": "1001",
        //    "OldPass": "rp12345",
        //    "NewPass": "rp00000"
        //}

        [HttpPost]
        [Route("api/VendorProfile/changepassword")]
        public HttpResponseMessage changepassword([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.changepassword(u));
        }

        //{
        //    "Email": "rp@rp.com",
        //     "OTP":"1234"
        //}

        [HttpPost]
        [Route("api/VendorProfile/forgetpassword")]
        public HttpResponseMessage forgetpassword([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.forgetpassword(u));
        }

        //{
        //    "Vid": "1001",
        //    "Pass": "rp12345",
        //}

        [HttpPost]
        [Route("api/VendorProfile/resetpassword")]
        public HttpResponseMessage resetpassword([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.resetpassword(u));
        }

        //no request parameters
        [HttpPost]
        [Route("api/VendorProfile/getAllSuburbs")]
        public HttpResponseMessage getAllSuburbs()
        {
            return Models.Helper.getResponse(f.getAllSuburbs());
        }


        //{
        //  "Vid": "1001",
        //  "City":"Mumbai",
        //  "Data": {
        //    "Suburbs": [
        //      {
        //        "SName": "Andheri"
        //      },
        //      {
        //        "SName": "Borivali"
        //      }
        //    ]
        //  }
        //}

        [HttpPost]
        [Route("api/VendorProfile/AddSuburbs")]
        public HttpResponseMessage AddSuburbs([FromBody] Suburbs s)
        {
            return Models.Helper.getResponse(f.AddSuburbs(s));
        }


        //{
        //    "Vid": "1001",
        //    "SName": "All/Selected",
        //}
        [HttpPost]
        [Route("api/VendorProfile/getVendorSuburbs")]
        public HttpResponseMessage getVendorSuburbs([FromBody] Suburbs s)
        {
            return Models.Helper.getResponse(f.getVendorSuburbs(s));
        }

        //{
        //    "Vid": "1001"
        //}
        [HttpPost]
        [Route("api/VendorProfile/getVendorratings")]
        public HttpResponseMessage getVendorratings([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.getVendorratings(u));
        }

        //{
        //    "Vid": "userid/vendorid",
        //    "Token": "234567890-erghjkteih3r3yt73t73g3v3g33i3yi3"
        //    "Flag": "True/False"
        //}

        [HttpPost]
        [Route("api/VendorProfile/updateNotifications")]
        public HttpResponseMessage updateNotifications([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.updateNotifications(u));
        }


        //{
        //    "Vid": "userid/vendorid",
        //    "Token": "234567890-erghjkteih3r3yt73t73g3v3g33i3yi3"
        //}

        [HttpPost]
        [Route("api/VendorProfile/getNotificationStatus")]
        public HttpResponseMessage getNotificationStatus([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.getNotificationStatus(u));
        }


        [HttpGet]
        [Route("api/VendorProfile/getAllcities")]
        public HttpResponseMessage getAllcities()
        {
            return Models.Helper.getResponse(f.getAllcities());
        }


        [HttpPost]
        [Route("api/VendorProfile/sendEmail")]
        public HttpResponseMessage sendEmail()
        {
            return Models.Helper.getResponse(f.sendEmail());
        }

    }
}
