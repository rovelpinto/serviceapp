﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.BookingsFunctions;
using static webapi_demo.Models.CategoryFunctions;
using static webapi_demo.Models.ServiceFunctions;

namespace webapi_demo.Controllers
{
    public class BookingsController : ApiController
    {
        Models.BookingsFunctions f = new Models.BookingsFunctions();

        //URL LINK http://servicenz.azurewebsites.net/


        //{
        //    "RequestType":"Direct/Multiple",
        //    "Uid":"1002",
        //    "VNCat":"8",
        //    "VNSubcat":"3",
        //    "Address":"Flat no : 2, Test Society, Adarsh Nagar, Sanatacruz, Mumbai - 400001",
        //    "QTitle":"Spa - Foot Massage Spa",
        //    "QDesc":"I want to have a soft tissue massage",
        //    "QDatetime":"2021/05/14 13:16", (taken directly from backend)
        //    "QJobtype":"One Off/Repeat",
        //    "QJobDate":"2021/05/14/NA",
        //    "QJobTime":"13:17/NA",
        //    "QJobPreference":"ASAP/Flexible/Specific Date Time",
        //    "Img1":"Uid_Qtitle_SystemTimeinMilis_Image1.jpg",
        //    "Img2":"Uid_Qtitle_SystemTimeinMilis_Image2.jpg",
        //    "Img3":"NA",
        //    "Img4":"NA",
        //    "Img5":"NA",
        //    "Data": {
        //          "Vendors": [
        //          {
        //              "Vid": "1001",
        //              "VNId": "100",
        //              "VNName": "Multani Foot Massage",
        //              "VNPrice": "4200",
        //          },
        //          {
        //              "Vid": "",
        //              "VNId": "",
        //              "VNName": "",
        //              "VNPrice": "",
        //          }
        //      ]
        //    }
        //}

        //RequestType=Direct(Book service) / Multiple (Multiple qoute request)
        //QTitle=[Categoryname - Subcategory Name]
        //QJobtype=One Off/Repeat
        //QJobPreference=ASAP/Flexible/Specific Date Time
        //QJobDate= Enter date only if Jobpreference is "Specific Date Time" or else NA
        //QJobTime= Enter time only if Jobpreference is "Specific Date Time" or else NA
        //Under Data - Vendors (Add only one vendor to the array if RequestType=Direct else Multiple selection)

        [HttpPost]
        [Route("api/Bookings/RequestBooking")]
        public HttpResponseMessage RequestBooking([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.RequestBooking(q));
        }

        //{
        //    "Uid":"1002",
        //    "Atype":"Home",
        //    "Address":"Flat no : 2, Test Society, Adarsh Nagar, Sanatacruz, Mumbai - 400001"
        //}

        [HttpPost]
        [Route("api/Bookings/AddAddress")]
        public HttpResponseMessage AddAddress([FromBody] BillingAddress a)
        {
            return Models.Helper.getResponse(f.AddAddress(a));
        }


        //{
        //    "Aid":"1",
        //    "Atype":"Office",
        //    "Address":"Flat no : 2, Test Society, Adarsh Nagar, Sanatacruz, Mumbai - 400002"
        //}

        [HttpPost]
        [Route("api/Bookings/UpdateAddress")]
        public HttpResponseMessage UpdateAddress([FromBody] BillingAddress a)
        {
            return Models.Helper.getResponse(f.UpdateAddress(a));
        }

        //{
        //    "Aid":"1"
        //}

        [HttpPost]
        [Route("api/Bookings/DeleteAddress")]
        public HttpResponseMessage DeleteAddress([FromBody] BillingAddress a)
        {
            return Models.Helper.getResponse(f.DeleteAddress(a));
        }

        //{
        //    "Uid":"1002"
        //}

        [HttpPost]
        [Route("api/Bookings/getAddress")]
        public HttpResponseMessage getAddress([FromBody] BillingAddress a)
        {
            return Models.Helper.getResponse(f.getAddress(a));
        }


        //{
        //    "Uid":"1002"
        //}
        [HttpPost]
        [Route("api/Bookings/User_Bookings")]
        public HttpResponseMessage User_Bookings([FromBody] BillingAddress a)
        {
            return Models.Helper.getResponse(f.User_Bookings(a));
        }


        //{
        //    "QRid":"10"
        //}
        [HttpPost]
        [Route("api/Bookings/User_BookingDetails")]
        public HttpResponseMessage User_BookingDetails([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_BookingDetails(q));
        }


        //{
        //    "QRid":"10"
        //    "Uid":"10"
        //    "Datetime":"2021/05/24 13:35" (taken from backend)
        //}
        [HttpPost]
        [Route("api/Bookings/User_CancelBooking")]
        public HttpResponseMessage User_CancelBooking([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_CancelBooking(q));
        }


        //{
        //    "QRid":"10"
        //    "Uid":"10",
        //    "Rating":"0.1-5.0",
        //    "Review":"I am happy with the service"
        //}
        [HttpPost]
        [Route("api/Bookings/User_SubmitServiceRatings")]
        public HttpResponseMessage User_SubmitServiceRatings([FromBody] Ratings r)
        {
            return Models.Helper.getResponse(f.User_SubmitServiceRatings(r));
        }


        //{
        //    "QRid":"10"
        //    "Uid":"10",
        //    "Rating":"0.1-5.0",
        //    "Review":"The vendor is very friendly"
        //}
        [HttpPost]
        [Route("api/Bookings/User_SubmitVendorRatings")]
        public HttpResponseMessage User_SubmitVendorRatings([FromBody] Ratings r)
        {
            return Models.Helper.getResponse(f.User_SubmitVendorRatings(r));
        }


        //{
        //    "QRid":"10"
        //}
        [HttpPost]
        [Route("api/Bookings/User_CheckBookingPaid")]
        public HttpResponseMessage User_CheckBookingPaid([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_CheckBookingPaid(q));
        }


        //{
        //    "QRid":"10",
        //    "Amount":"2000",
        //    "Status":"Succeeded",
        //    "StripeId":"kjskakbjds89900990",
        //    "StripeResponse":"jsonresponse",
        //    "StripeReceipt":"receipturl"
        //}
        [HttpPost]
        [Route("api/Bookings/User_BookingPaid")]
        public HttpResponseMessage User_BookingPaid([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.User_BookingPaid(q));
        }

        //--------------------------------VENDOR----------------------------------------------

        //{
        //    "Vid":"1002"
        //}

        [HttpPost]
        [Route("api/Bookings/Vendor_Bookings")]
        public HttpResponseMessage Vendor_Bookings([FromBody] Vendors a)
        {
            return Models.Helper.getResponse(f.Vendor_Bookings(a));
        }

        //{
        //    "QRid":"10"
        //}

        [HttpPost]
        [Route("api/Bookings/Vendor_BookingDetails")]
        public HttpResponseMessage Vendor_BookingDetails([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_BookingDetails(q));
        }

        //{
        //    "QRid":"10"
        //    "Vid":"1001"
        //    "Status":"Accepted/Rejected/Completed/Cancelled"
        //    "Datetime":"2021/05/24 13:35" (taken from backend)
        //}

    [HttpPost]
        [Route("api/Bookings/Vendor_UpdateBooking")]
        public HttpResponseMessage Vendor_UpdateBooking([FromBody] QuoteRequest q)
        {
            return Models.Helper.getResponse(f.Vendor_UpdateBooking(q));
        }

    }
}
