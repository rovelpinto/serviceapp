﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.CategoryFunctions;

namespace webapi_demo.Controllers
{
    public class CategoryController : ApiController
    {
        Models.CategoryFunctions f = new Models.CategoryFunctions();

        //URL LINK http://servicenz.azurewebsites.net/


        //{
        //    "Name": "Spa",
        //    "Image": "spa.jpg",
        //    "Desciption": "All kinds of Massages & Body Pampering & relaxations comes under this category"
        //}

        [HttpPost]
        [Route("api/Category/AddCategory")]
        public HttpResponseMessage AddCategory([FromBody] Category c)
        {
            return Models.Helper.getResponse(f.AddCategory(c));
        }


        //{
        //    "Cid": "1",
        //    "Name": "Spa",
        //    "Image": "spa.jpg",
        //    "Desciption": "All kinds of Massages & Body Pampering & relaxations comes under this category"
        //}

        [HttpPost]
        [Route("api/Category/UpdateCategory")]
        public HttpResponseMessage UpdateCategory([FromBody] Category c)
        {
            return Models.Helper.getResponse(f.UpdateCategory(c));
        }


        //{
        //    "Cid": "1"
        //}

        [HttpPost]
        [Route("api/Category/DeleteCategory")]
        public HttpResponseMessage DeleteCategory([FromBody] Category c)
        {
            return Models.Helper.getResponse(f.DeleteCategory(c));
        }


        //{
        //    "Name": "All/Spa"
        //}

        // All will give you all the categories & rest will be used with Like query

        [HttpPost]
        [Route("api/Category/getCategories")]
        public HttpResponseMessage getCategories([FromBody] Category c)
        {
            return Models.Helper.getResponse(f.getCategories(c));
        }



        //---------------------------------SUB CATEGORY-----------------------------------------------

        //{
        //    "Cid": "1",
        //    "Name": "Ayurvedic Foot Spa",
        //    "Image": "footspa.jpg",
        //    "Desciption": "you will get the best Ayurvedic Foot Spa, which is a 1000 years old Mantra of technique"
        //}

        [HttpPost]
        [Route("api/Category/AddSubcategory")]
        public HttpResponseMessage AddSubcategory([FromBody] Subcategory c)
        {
            return Models.Helper.getResponse(f.AddSubcategory(c));
        }


        //{
        //    "SCid": "1",
        //    "Cid": "1",
        //    "Name": "Ayurvedic Foot Spa",
        //    "Image": "footspa.jpg",
        //    "Desciption": "you will get the best Ayurvedic Foot Spa, which is a 1000 years old Mantra of technique"
        //}

        [HttpPost]
        [Route("api/Category/UpdateSubcategory")]
        public HttpResponseMessage UpdateSubcategory([FromBody] Subcategory c)
        {
            return Models.Helper.getResponse(f.UpdateSubcategory(c));
        }


        //{
        //    "SCid": "1"
        //}

        [HttpPost]
        [Route("api/Category/DeleteSubcategory")]
        public HttpResponseMessage DeleteSubcategory([FromBody] Subcategory c)
        {
            return Models.Helper.getResponse(f.DeleteSubcategory(c));
        }


        //{
        //    "Cid": "1",
        //    "Name": "All/footspa"
        //}
        // All will give you all the categories & rest will be used with Like query

        [HttpPost]
        [Route("api/Category/getSubcategories")]
        public HttpResponseMessage getSubcategories([FromBody] Subcategory c)
        {
            return Models.Helper.getResponse(f.getSubcategories(c));
        }

    }
}
