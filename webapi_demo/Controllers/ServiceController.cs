﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.CategoryFunctions;
using static webapi_demo.Models.ServiceFunctions;

namespace webapi_demo.Controllers
{
    public class ServiceController : ApiController
    {
        Models.ServiceFunctions f = new Models.ServiceFunctions();

        //URL LINK http://servicenz.azurewebsites.net/


        //{
        //    "Vid":"1001",
        //    "VNType":"Fixed/Negotiable",
        //    "VNName":"Fruits Exotic Foot Massage",
        //    "VNDesc":"This the best kind of massage with all Fresh Exotic Fruits which are rich in Iron",
        //    "VNPrice":"2500",
        //    "VNTime":"30",
        //    "VNCat":"8",
        //    "VNSubcat":"3",
        //    "VNStatus":"Enable/Disable"
        //}

        [HttpPost]
        [Route("api/Service/AddServices")]
        public HttpResponseMessage AddServices([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.AddServices(s));
        }


        //{
        //    "VNId":"103",
        //    "Image1":"103_FootMessage_Image1",
        //    "Image2":"103_FootMessage_Image2",
        //    "Image3":"103_FootMessage_Image3",
        //    "Image4":"NA",
        //    "Image5":"NA"
        //}

        [HttpPost]
        [Route("api/Service/AddServicesImages")]
        public HttpResponseMessage AddServicesImages([FromBody] VendorImages i)
        {
            return Models.Helper.getResponse(f.AddServicesImages(i));
        }


        //{
        //    "VNId":"103"
        //    "Vid":"1001",
        //    "VNType":"Fixed/Negotiable",
        //    "VNName":"Fruits Exotic Foot Massage",
        //    "VNDesc":"This the best kind of massage with all Fresh Exotic Fruits which are rich in Iron",
        //    "VNPrice":"2500",
        //    "VNTime":"30",
        //    "VNCat":"8",
        //    "VNSubcat":"3",
        //    "VNStatus":"Enable/Disable"
        //}

        [HttpPost]
        [Route("api/Service/UpdateServices")]
        public HttpResponseMessage UpdateServices([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.UpdateServices(s));
        }

        //{
        //    "VNId":"103",
        //    "Image1":"103_FootMessage_Image1",
        //    "Image2":"103_FootMessage_Image2",
        //    "Image3":"103_FootMessage_Image3",
        //    "Image4":"103_FootMessage_Image4",
        //    "Image5":"103_FootMessage_Image5"
        //}

        [HttpPost]
        [Route("api/Service/UpdateServicesImages")]
        public HttpResponseMessage UpdateServicesImages([FromBody] VendorImages i)
        {
            return Models.Helper.getResponse(f.UpdateServicesImages(i));
        }

        //{
        //    "VNId":"103"
        //    "VNStatus":"Removed"
        //}

        //to enable it again change the status to "Enable"
        [HttpPost]
        [Route("api/Service/DeleteService")]
        public HttpResponseMessage DeleteService([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.DeleteService(s));
        }


        //{
        //  "VNId":"100",
        //  "VS1Name": "Olive Oil",
        //  "VS1Desc": "Foot massage with Olive Oil",
        //  "VS1Amount": "500",
        //  "VS1Image": "oliveoil.jpg",
        //  "VS1Time": "10",
        //  "VS2Name": "Coconut Oil",
        //  "VS2Desc": "Foot massage with Coconut Oil",
        //  "VS2Amount": "300",
        //  "VS2Image": "Coconutoil.jpg",
        //  "VS2Time": "10",
        //  "VS3Name": "NA",
        //  "VS3Desc": "NA",
        //  "VS3Amount": "NA",
        //  "VS3Image": "NA",
        //  "VS3Time": "NA",
        //  "VS4Name": "NA",
        //  "VS4Desc": "NA",
        //  "VS4Amount": "NA",
        //  "VS4Image": "NA",
        //  "VS4Time": "NA",
        //  "VS5Name": "NA",
        //  "VS5Desc": "NA",
        //  "VS5Amount": "NA",
        //  "VS5Image": "NA",
        //  "VS5Time": "NA",
        //}

        [HttpPost]
        [Route("api/Service/AddServiceVariant")]
        public HttpResponseMessage AddServiceVariant([FromBody] Variant v)
        {
            return Models.Helper.getResponse(f.AddServiceVariant(v));
        }

        //{
        //  "VSId":"1",
        //  "VS1Name": "Olive Oil",
        //  "VS1Desc": "Foot massage with Olive Oil",
        //  "VS1Amount": "300",
        //  "VS1Image": "oliveoil.jpg",
        //  "VS1Time": "10",
        //  "VS2Name": "Coconut Oil",
        //  "VS2Desc": "Foot massage with Coconut Oil",
        //  "VS2Amount": "300",
        //  "VS2Image": "Coconutoil.jpg",
        //  "VS2Time": "10",
        //  "VS3Name": "Tea Tree Oil",
        //  "VS3Desc": "Foot massage with tea Tree Oil",
        //  "VS3Amount":"500",
        //  "VS3Image": "teatreeoil.jpg",
        //  "VS3Time": "10",
        //  "VS4Name": "NA",
        //  "VS4Desc": "NA",
        //  "VS4Amount":"NA",
        //  "VS4Image": "NA",
        //  "VS4Time": "NA",
        //  "VS5Name": "NA",
        //  "VS5Desc": "NA",
        //  "VS5Amount": "NA",
        //  "VS5Image": "NA",
        //  "VS5Time": "NA",
        //}

        [HttpPost]
        [Route("api/Service/UpdateServiceVariant")]
        public HttpResponseMessage UpdateServiceVariant([FromBody] Variant v)
        {
            return Models.Helper.getResponse(f.UpdateServiceVariant(v));
        }

        //{
        //  "VSId":"1",
        //}
        [HttpPost]
        [Route("api/Service/DeleteServiceVariant")]
        public HttpResponseMessage DeleteServiceVariant([FromBody] Variant v)
        {
            return Models.Helper.getResponse(f.DeleteServiceVariant(v));
        }

        //{
        //   "Vid":"1001",
        //   "VNType":"Fixed",
        //   "VNName":"",
        //   "VNStatus":""
        //}

        //VNType=Any/Fixed/Negotiable
        //VNStatus=Enable/Disable/Removed
        //leave "" blank incase filter is not used

        [HttpPost]
        [Route("api/Service/getServicesforVendor")]
        public HttpResponseMessage getServicesforVendor([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getServicesforVendor(s));
        }

        //{
        //   "VSId":"2"
        //}

        [HttpPost]
        [Route("api/Service/getVariant")]
        public HttpResponseMessage getVariant([FromBody] Variant v)
        {
            return Models.Helper.getResponse(f.getVariant(v));
        }

        //{
        // "Uid":"1002"
        //}
        [HttpPost]
        [Route("api/Service/getUserSuburbs")]
        public HttpResponseMessage getUserSuburbs([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getUserSuburbs(s));
        }


        //{
        // "Uid":"1002",
        // "City":"Mumbai",
        // "SName":"Andheri"
        //}

        [HttpPost]
        [Route("api/Service/SubmitSuburb")]
        public HttpResponseMessage SubmitSuburb([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.SubmitSuburb(s));
        }


        //{
        // "City":"Mumbai",
        // "SName": "Andheri"
        // "Name":"All/Spa"
        //}

        [HttpPost]
        [Route("api/Service/getCategories")]
        public HttpResponseMessage getCategories([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getCategories(s));
        }

        //{
        // "Cid":"8"
        // "City":"Mumbai",
        // "SName": "Andheri"
        // "Name":"All/Spa"
        //}

        [HttpPost]
        [Route("api/Service/getSubCategories")]
        public HttpResponseMessage getSubCategories([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getSubCategories(s));
        }

        //{
        // "City":"Mumbai",
        // "SName": "Andheri"
        //  "VNType":"All",
        //    "VNCat":"8",
        //  "VNSubcat":"3",
        //}

        //VNType=All/Fixed/Negotiable

        [HttpPost]
        [Route("api/Service/getServicesforUser")]
        public HttpResponseMessage getServicesforUser([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getServicesforUser(s));
        }


        //{
        //  "Uid": "1002",
        //  "VNId":"100"
        //}

        [HttpPost]
        [Route("api/Service/getServicedetails")]
        public HttpResponseMessage getServicedetails([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getServicedetails(s));
        }


        //{
        //  "VNId":"100"
        //}
        [HttpPost]
        [Route("api/Service/getServiceReviews")]
        public HttpResponseMessage getServiceReviews([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getServiceReviews(s));
        }

        //{
        //  "Vid":"1001",
        //  "VNType":"Fixed/Negotiable"
        //}
        [HttpPost]
        [Route("api/Service/getVendor_Services")]
        public HttpResponseMessage getVendor_Services([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getVendor_Services(s));
        }


        //{
        //  "Vid":"1001",
        //}
        [HttpPost]
        [Route("api/Service/getVendor_Aboutus")]
        public HttpResponseMessage getVendor_Aboutus([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getVendor_Aboutus(s));
        }

        //{
        //  "Vid":"1001",
        //}
        [HttpPost]
        [Route("api/Service/getVendor_Reviews")]
        public HttpResponseMessage getVendor_Reviews([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getVendor_Reviews(s));
        }

        //{
        //  "Vid":"1001"
        //}
        [HttpPost]
        [Route("api/Service/getVendor_Vendordetails")]
        public HttpResponseMessage getVendor_Vendordetails([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getVendor_Vendordetails(s));
        }

        //{
        //  "VNId":"110"
        //}
        [HttpPost]
        [Route("api/Service/getServiceReviewsforVendors")]
        public HttpResponseMessage getServiceReviewsforVendors([FromBody] Vendorservices s)
        {
            return Models.Helper.getResponse(f.getServiceReviewsforVendors(s));
        }
    }
}
