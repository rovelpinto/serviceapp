﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.VendorDashFunctions;

namespace webapi_demo.Controllers
{
    public class VendorDashController : ApiController
    {
        Models.VendorDashFunctions f = new Models.VendorDashFunctions();

        //URL LINK http://servicenz.azurewebsites.net/

        //{
        //    "Vid": "1001",
        //    "Type":"Monthly"
        //}
        //Type=Monthly/Half Yearly/Yearly

        [HttpPost]
        [Route("api/VendorDash/getVendordashboad")]
        public HttpResponseMessage getVendordashboad([FromBody] VendorD u)
        {
            return Models.Helper.getResponse(f.getVendordashboad(u));
        }
        
    }
}
