﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static webapi_demo.Models.UserProfileFunctions;

namespace webapi_demo.Controllers
{
    public class UserProfileController : ApiController
    {
        Models.UserProfileFunctions f = new Models.UserProfileFunctions();

        //URL LINK http://servicenz.azurewebsites.net/

        //{
        //    "Fname": "Rovel",
        //    "Lname": "Pinto",
        //    "Email": "rp@rp.com",
        //    "Pass": "rp12345",
        //    "Mobile": "9876543210",
        //    "City": "Mumbai",
        //    "Country": "India",
        //    "Photo": "name.jpg"
        //}

        [HttpPost]
        [Route("api/UserProfile/registration")]
        public HttpResponseMessage registration([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.registration(u));
        }


        //{
        //    "Fname": "Rovel",
        //    "Lname": "Pinto",
        //    "Email": "rp@rp.com",
        //    "Pass": "rp12345",
        //    "Mobile": "9876543210",
        //    "City": "Mumbai",
        //    "Country": "India",
        //    "Photo": "name.jpg"
        //    "OTP": "1234"
        //}

        [HttpPost]
        [Route("api/UserProfile/Checkregistration")]
        public HttpResponseMessage Checkregistration([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.Checkregistration(u));
        }


        //{
        //  "Email": "rp@rp.com",
        //  "Mobile": "NA",
        //  "Pass": "rp12345"
        //}
        //or visa versa
        [HttpPost]
        [Route("api/UserProfile/login")]
        public HttpResponseMessage login([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.login(u));
        }

        //{
        //  "Email": "rp@rp.com",
        //  "Mobile": "NA",
        //  "Pass": "rp12345",
        //  "Token": "asdfghjkl123456789o"
        //}
        //or visa versa
        [HttpPost]
        [Route("api/UserProfile/loginwithToken")]
        public HttpResponseMessage loginwithToken([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.loginwithToken(u));
        }


        //{
        //  "Uid": "1001"
        //  "Token": "dhtckl5467890"
        //}
        [HttpPost]
        [Route("api/UserProfile/Logout")]
        public HttpResponseMessage Logout([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.Logout(u));
        }


        //{
        //  "Uid": "1001",
        //}
        [HttpPost]
        [Route("api/UserProfile/getProfile")]
        public HttpResponseMessage getProfile([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.getProfile(u));
        }

        //{
        //    "Uid": "1001",
        //    "Fname": "Rovel",
        //    "Lname": "Pinto",
        //    "Email": "rp@rp.com",
        //    "Mobile": "9876543210",
        //    "City": "Mumbai",
        //    "Country": "India",
        //    "Photo": "name.jpg"
        //}

        [HttpPost]
        [Route("api/UserProfile/updateprofile")]
        public HttpResponseMessage updateprofile([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.updateprofile(u));
        }

        //{
        //    "Uid": "1001",
        //    "OldPass": "rp12345",
        //    "NewPass": "rp00000"
        //}

        [HttpPost]
        [Route("api/UserProfile/changepassword")]
        public HttpResponseMessage changepassword([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.changepassword(u));
        }


        //{
        //    "Email": "rp@rp.com",
        //    "OTP":"1234"
        //}

        [HttpPost]
        [Route("api/UserProfile/forgetpassword")]
        public HttpResponseMessage forgetpassword([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.forgetpassword(u));
        }

        //{
        //    "Uid": "1002",
        //    "Pass": "rp12345",
        //}

        [HttpPost]
        [Route("api/UserProfile/resetpassword")]
        public HttpResponseMessage resetpassword([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.resetpassword(u));
        }


        //{
        //    "Uid": "1002",
        //    "Atype": "Home",
        //    "Address": "Santacruz,Mumbai"
        //}
        [HttpPost]
        [Route("api/UserProfile/addAddress")]
        public HttpResponseMessage addAddress([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.addAddress(u));
        }


        //{
        //    "Aid": "1",
        //    "Atype": "Home",
        //    "Address": "Santacruz,kalina,Mumbai",
        //}

        [HttpPost]
        [Route("api/UserProfile/updateAddress")]
        public HttpResponseMessage updateAddress([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.updateAddress(u));
        }


        //{
        //    "Aid": "1"
        //}

        [HttpPost]
        [Route("api/UserProfile/deleteAddress")]
        public HttpResponseMessage deleteAddress([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.deleteAddress(u));
        }


        //{
        //    "Uid": "1001"
        //}

        [HttpPost]
        [Route("api/UserProfile/getAddress")]
        public HttpResponseMessage getAddress([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.getAddress(u));
        }


        //{
        //    "Uid": "1001",
        //    "Aid": "1"
        //}

        [HttpPost]
        [Route("api/UserProfile/getAddressbyId")]
        public HttpResponseMessage getAddressbyId([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.getAddressbyId(u));
        }


        //{
        //    "Uid": "1002",
        //    "Ctype": "Debitcard/Creditcard",
        //    "Cno": "1234567891023456", (without space)
        //    "Expiryyear": "2021", (yyyy)
        //    "Expirymonth": "06", (MM)
        //    "Cvv": "123",
        //    "Cardname": "Rovel Pinto", (Full name/can be left blank)
        //    "Bankname": "ICICI Bank",
        //}
        [HttpPost]
        [Route("api/UserProfile/addCard")]
        public HttpResponseMessage addCard([FromBody] Cards c)
        {
            return Models.Helper.getResponse(f.addCard(c));
        }


        //{
        //    "Cid": "1"
        //}

        [HttpPost]
        [Route("api/UserProfile/deleteCard")]
        public HttpResponseMessage deleteCard([FromBody] Cards c)
        {
            return Models.Helper.getResponse(f.deleteCard(c));
        }


        //{
        //    "Uid": "1001"
        //}

        [HttpPost]
        [Route("api/UserProfile/getCards")]
        public HttpResponseMessage getCards([FromBody] Cards c)
        {
            return Models.Helper.getResponse(f.getCards(c));
        }


        //{
        //    "Uid": "userid/vendorid",
        //    "Utype": "User/Vendor",
        //    "Token": "234567890-erghjkteih3r3yt73t73g3v3g33i3yi3"
        //}
        //call this in both the 

        [HttpPost]
        [Route("api/UserProfile/updateToken")]
        public HttpResponseMessage updateToken([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.updateToken(u));
        }


        //{
        //    "Uid": "userid/vendorid",
        //    "Token": "234567890-erghjkteih3r3yt73t73g3v3g33i3yi3"
        //    "Flag": "True/False"
        //}

        [HttpPost]
        [Route("api/UserProfile/updateNotifications")]
        public HttpResponseMessage updateNotifications([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.updateNotifications(u));
        }


        //{
        //    "Uid": "userid/vendorid",
        //    "Token": "234567890-erghjkteih3r3yt73t73g3v3g33i3yi3"
        //}

        [HttpPost]
        [Route("api/UserProfile/getNotificationStatus")]
        public HttpResponseMessage getNotificationStatus([FromBody] UserD u)
        {
            return Models.Helper.getResponse(f.getNotificationStatus(u));
        }
    }
}
